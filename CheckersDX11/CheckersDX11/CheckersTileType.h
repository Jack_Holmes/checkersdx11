#pragma once

// The CheckersTileType holds all the possible different
// states a checker board tile can have.
enum class CheckersTileType
{
	White,
	Black,
	Player1Piece,
	Player1King,
	Player2Piece,
	Player2King
};