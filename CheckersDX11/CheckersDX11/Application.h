#pragma once

#include "Quanta/QuantaDefines.h"

// Link against depedent libraries
QUANTA_LINK_LIBRARY(FW1FontWrapper) // Fonts
QUANTA_LINK_LIBRARY(Shlwapi)        // Windows open/save file interfacing
QUANTA_LINK_LIBRARY(irrKlang)       // Sound library

#include <memory>

#include "Quanta/QApplication.h"

#include "SceneManager.h"
#include "ResourceMapper.h"

class Application : public Quanta::QApplication
{
public:
	Application();
	~Application();

	// Run the checkers game. This contains the main game loop
	// and only needs to be called once.
	int Run();

private:
	// Process and window message events.
	void ProcessEvents() const;
	// The main update loop.
	void Update(float deltaTime) const;
	// The main render loop.
	void Render() const;

	// Perform application setup that is neccessary before entering the main
	// application loop.
	void SetUp();

	// The scene manager which holds all the application scenes
	// used through an execution.
	std::unique_ptr<SceneManager> m_sceneManager;

	// The Resource Mapper
	std::unique_ptr<ResourceMapper> m_resourceMapper;
};