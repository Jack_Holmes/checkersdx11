#include "CheckersBoardSelectionHandler.h"
#include "Quanta/QEvent.h"
#include "Quanta/QRay.h"

using namespace Quanta;
using namespace DirectX;

CheckersBoardSelectionHandler::CheckersBoardSelectionHandler(Quanta::QWindow* window, Quanta::QCamera* camera)
	: m_window(window)
	, m_camera(camera)
	, m_isPlayer1(true)
	, m_sourceIndex(0)
	, m_destinationIndex(0)
	, m_inMultiJumpState(false)
{
	// Create the 64 tiles used for selection
	for (int index = 0; index < 64; ++index)
	{
		m_checkersBoardTiles.push_back(CheckersBoardTile(index, this));
	}
}

void CheckersBoardSelectionHandler::Update(float deltaTime)
{
	for (auto& checkersSquare : m_checkersBoardTiles)
	{
		checkersSquare.Update(deltaTime);
	}
}

void CheckersBoardSelectionHandler::Render(Quanta::QRenderer& renderer)
{
	for (auto& checkersSquare : m_checkersBoardTiles)
	{
		checkersSquare.Render(renderer);
	}
}

void CheckersBoardSelectionHandler::ProcessEvent(const Quanta::QEvent& e)
{
	if (e.type == QEvent::Type::MouseButtonPressed
		&& e.mouse.button == QMouse::Button::Left)
	{
		float dist;
		QRay ray = GetMousePickingRay(*m_window, *m_camera);

		for (int i = 63; i >= 0; --i)
		{
			auto& checkersSquare = m_checkersBoardTiles[i];
			BoundingBox boundingBox = checkersSquare.GetBoundingBox();
			if (boundingBox.Intersects(ray.GetOrigin(), ray.GetDirection(), dist))
			{
				if (!m_inMultiJumpState)
				{
					if (// If player 1 piece selected
						(m_isPlayer1 
						&& (checkersSquare.GetCheckersTileType() == CheckersTileType::Player1Piece
						|| checkersSquare.GetCheckersTileType() == CheckersTileType::Player1King))
						// Or if player 2 piece selected
						|| (!m_isPlayer1 
						&& (checkersSquare.GetCheckersTileType() == CheckersTileType::Player2Piece
						|| checkersSquare.GetCheckersTileType() == CheckersTileType::Player2King)))
					{
						// Player is selecting their piece (as a candidate to move)
						SetSourceIndex(checkersSquare.GetIndex());
						checkersSquare.SetSelected(SelectionType::Source);
						MakeMove();
						break;
					}
				}

				if (checkersSquare.GetSelectionType() == SelectionType::Destination)
				{
					// Player is selecting a destination, hence requesting to move.
					SetDestinationIndex(checkersSquare.GetIndex());
				}
			}			
		}
	}
}

void CheckersBoardSelectionHandler::UpdateTile(int index, CheckersTileType tileType)
{
	if (index < m_checkersBoardTiles.size())
	{
		// Update the type of tile being represented
		m_checkersBoardTiles[index].SetCheckersTileType(tileType);
	}	
}

void CheckersBoardSelectionHandler::SetIsPlayer1(bool isPlayer1)
{
	m_isPlayer1 = isPlayer1;
}

void CheckersBoardSelectionHandler::SetSourceIndex(int index)
{
	m_sourceIndex = index;
}

void CheckersBoardSelectionHandler::SetDestinationIndex(int index)
{
	m_destinationIndex = index;
	
	ClearSelections();

	if (m_makeMoveCallback)
	{
		m_makeMoveCallback();
	}	
}

int CheckersBoardSelectionHandler::GetSourceIndex() const
{
	return m_sourceIndex;
}

int CheckersBoardSelectionHandler::GetDestinationIndex() const
{
	return m_destinationIndex;
}

void CheckersBoardSelectionHandler::MakeMove()
{
	ClearSelections();

	m_checkersBoardTiles[m_sourceIndex].SetSelected(SelectionType::Source);

	if (m_checkersBoardTiles[m_sourceIndex].GetCheckersTileType() == CheckersTileType::Player1Piece
		|| m_checkersBoardTiles[m_sourceIndex].GetCheckersTileType() == CheckersTileType::Player2Piece)
	{
		// Check simple and capture moves for a regular piece
		CalculateSimpleMovesForRegularPiece(m_sourceIndex);
		CalculateCaptureMovesForRegularPiece(m_sourceIndex);
	}
	else if (m_checkersBoardTiles[m_sourceIndex].GetCheckersTileType() == CheckersTileType::Player1King
		|| m_checkersBoardTiles[m_sourceIndex].GetCheckersTileType() == CheckersTileType::Player2King)
	{
		// Check simple and capture moves for a king
		CalculateSimpleMovesForKingPiece(m_sourceIndex);
		CalculateCaptureMovesForKingPiece(m_sourceIndex);
	}
}

void CheckersBoardSelectionHandler::SetMakeMoveCallback(std::function<void()> callback)
{
	m_makeMoveCallback = callback;
}

bool CheckersBoardSelectionHandler::IsMultiJump()
{
	// Check if this is a multi-jump move.
	if (m_checkersBoardTiles[m_sourceIndex].GetCheckersTileType() == CheckersTileType::Player1Piece
		|| m_checkersBoardTiles[m_sourceIndex].GetCheckersTileType() == CheckersTileType::Player2Piece)
	{
		m_inMultiJumpState = CalculateCaptureMovesForRegularPiece(m_sourceIndex);
	}
	else if (m_checkersBoardTiles[m_sourceIndex].GetCheckersTileType() == CheckersTileType::Player1King
		|| m_checkersBoardTiles[m_sourceIndex].GetCheckersTileType() == CheckersTileType::Player2King)
	{
		m_inMultiJumpState = CalculateCaptureMovesForKingPiece(m_sourceIndex);
	}

	return m_inMultiJumpState;
}

void CheckersBoardSelectionHandler::SetMultiJump(bool multijump)
{
	m_inMultiJumpState = multijump;
}

void CheckersBoardSelectionHandler::ClearSelections()
{
	// Clear any current selections
	for (auto& checkersBoardTile : m_checkersBoardTiles)
	{
		checkersBoardTile.SetSelected(SelectionType::None);
	}
}

void CheckersBoardSelectionHandler::Reset()
{
	SetIsPlayer1(true);
	ClearSelections();
	SetSourceIndex(0);
	SetDestinationIndex(0);
}

void CheckersBoardSelectionHandler::CalculateSimpleMovesForRegularPiece(int index)
{
	assert(index >= 0 && index <= 63);

	int adjacentDiagonal1 = index + 7;
	int adjacentDiagonal2 = index + 9;
	CheckersTileType playerPiece = CheckersTileType::Player1Piece;
	CheckersTileType opponentRegularPiece = CheckersTileType::Player2Piece;
	CheckersTileType opponentKingPiece = CheckersTileType::Player2King;

	if (!m_isPlayer1)
	{
		adjacentDiagonal1 = index - 9;
		adjacentDiagonal2 = index - 7;
		playerPiece = CheckersTileType::Player2Piece;
		opponentRegularPiece = CheckersTileType::Player1Piece;
		opponentKingPiece = CheckersTileType::Player1King;
	}

	// Confirm the selection is the players piece
	if (m_checkersBoardTiles[index].GetCheckersTileType() != playerPiece)
	{
		return;
	}

	int row = index / 8;
	int col = index % 8;

	// Check for cases where the selection is *not* on the left edge
	if (col > 0)
	{
		if ((m_isPlayer1 && row < 7) || (!m_isPlayer1 && row > 0))
		{
			// Check is the adjacent diagonal is available.
			if (m_checkersBoardTiles[adjacentDiagonal1].GetCheckersTileType() == CheckersTileType::Black)
			{
				m_checkersBoardTiles[adjacentDiagonal1].SetSelected(SelectionType::Destination);
			}
		}
	}

	// Check for cases where the selection is *not* on the right edge
	if (col < 7)
	{
		if ((m_isPlayer1 && row < 7) || (!m_isPlayer1 && row > 0))
		{
			// Check is the adjacent diagonal is available.
			if (m_checkersBoardTiles[adjacentDiagonal2].GetCheckersTileType() == CheckersTileType::Black)
			{
				m_checkersBoardTiles[adjacentDiagonal2].SetSelected(SelectionType::Destination);
			}
		}
	}
}

bool CheckersBoardSelectionHandler::CalculateCaptureMovesForRegularPiece(int index)
{
	assert(index >= 0 && index <= 63);

	int adjacentDiagonal1 = index + 7;
	int adjacentDiagonal2 = index + 9;
	CheckersTileType playerPiece = CheckersTileType::Player1Piece;
	CheckersTileType opponentRegularPiece = CheckersTileType::Player2Piece;
	CheckersTileType opponentKingPiece = CheckersTileType::Player2King;

	if (!m_isPlayer1)
	{
		adjacentDiagonal1 = index - 9;
		adjacentDiagonal2 = index - 7;
		playerPiece = CheckersTileType::Player2Piece;
		opponentRegularPiece = CheckersTileType::Player1Piece;
		opponentKingPiece = CheckersTileType::Player1King;
	}

	bool captureAvailable = false;

	// Confirm the selection is the players piece
	if (m_checkersBoardTiles[index].GetCheckersTileType() != playerPiece)
	{
		return captureAvailable;
	}

	int row = index / 8;
	int col = index % 8;

	// Check for cases where the selection is *not* on the left edge
	if (col > 1)
	{
		if ((m_isPlayer1 && row < 7) || (!m_isPlayer1 && row > 0))
		{
			if (m_checkersBoardTiles[adjacentDiagonal1].GetCheckersTileType() == opponentRegularPiece
				|| m_checkersBoardTiles[adjacentDiagonal1].GetCheckersTileType() == opponentKingPiece)
			{
				// The immediate diagonal has an opponent piece, so check if the next diagonal is available
				int nextDiagonal = adjacentDiagonal1 + 7;
				if (!m_isPlayer1)
				{
					nextDiagonal = adjacentDiagonal1 - 9;
				}

				// Out of bounds check
				if ((m_isPlayer1 && row < 6) || (!m_isPlayer1 && row > 1))
				{
					// Check for available space over capture piece
					if (m_checkersBoardTiles[nextDiagonal].GetCheckersTileType() == CheckersTileType::Black)
					{
						m_checkersBoardTiles[nextDiagonal].SetSelected(SelectionType::Destination);
						captureAvailable = true;
					}
				}
			}
		}
	}

	// Check for cases where the selection is *not* on the right edge
	if (col < 6)
	{
		if ((m_isPlayer1 && row < 7) || (!m_isPlayer1 && row > 0))
		{
			if (m_checkersBoardTiles[adjacentDiagonal2].GetCheckersTileType() == opponentRegularPiece
				|| m_checkersBoardTiles[adjacentDiagonal2].GetCheckersTileType() == opponentKingPiece)
			{
				// The immediate diagonal has an opponent piece, so check if the next diagonal is available
				int nextDiagonal = adjacentDiagonal2 + 9;
				if (!m_isPlayer1)
				{
					nextDiagonal = adjacentDiagonal2 - 7;
				}

				// Out of bounds check
				if ((m_isPlayer1 && row < 6) || (!m_isPlayer1 && row > 1))
				{
					// Check for available space over capture piece
					if (m_checkersBoardTiles[nextDiagonal].GetCheckersTileType() == CheckersTileType::Black)
					{
						m_checkersBoardTiles[nextDiagonal].SetSelected(SelectionType::Destination);
						captureAvailable = true;
					}
				}
			}
		}
	}

	return captureAvailable;
}

void CheckersBoardSelectionHandler::CalculateSimpleMovesForKingPiece(int index)
{
	assert(index >= 0 && index <= 63);

	//  -------------
	//  | 1 |   | 2 |  // 1(-9), 2(-7)
	//  -------------  
	//  |   | O |   |
	//  -------------
	//  | 3 |   | 4 |  // 3(+7), 4(+9)
	//  -------------
	int adjacentDiagonal1 = index - 9;
	int adjacentDiagonal2 = index - 7;
	int adjacentDiagonal3 = index + 7;
	int adjacentDiagonal4 = index + 9;
	CheckersTileType playerKingPiece = CheckersTileType::Player1King;
	CheckersTileType opponentRegularPiece = CheckersTileType::Player2Piece;
	CheckersTileType opponentKingPiece = CheckersTileType::Player2King;

	int row = index / 8;
	int col = index % 8;

	if (!m_isPlayer1)
	{
		playerKingPiece = CheckersTileType::Player2King;
		opponentRegularPiece = CheckersTileType::Player1Piece;
		opponentKingPiece = CheckersTileType::Player1King;
	}

	// Confirm the selection is the players king
	if (m_checkersBoardTiles[index].GetCheckersTileType() != playerKingPiece)
	{
		return;
	}

	if (col > 0)
	{
		if ((row > 0) && (m_checkersBoardTiles[adjacentDiagonal1].GetCheckersTileType() == CheckersTileType::Black))
		{
			m_checkersBoardTiles[adjacentDiagonal1].SetSelected(SelectionType::Destination);
		}

		if ((row < 7) && (m_checkersBoardTiles[adjacentDiagonal3].GetCheckersTileType() == CheckersTileType::Black))
		{
			m_checkersBoardTiles[adjacentDiagonal3].SetSelected(SelectionType::Destination);
		}
	}

	if (col < 7)
	{
		if ((row > 0) && (m_checkersBoardTiles[adjacentDiagonal2].GetCheckersTileType() == CheckersTileType::Black))
		{
			m_checkersBoardTiles[adjacentDiagonal2].SetSelected(SelectionType::Destination);
		}

		if ((row < 7) && (m_checkersBoardTiles[adjacentDiagonal4].GetCheckersTileType() == CheckersTileType::Black))
		{
			m_checkersBoardTiles[adjacentDiagonal4].SetSelected(SelectionType::Destination);
		}
	}
}

bool CheckersBoardSelectionHandler::CalculateCaptureMovesForKingPiece(int index)
{
	assert(index >= 0 && index <= 63);
	
	//  -------------
	//  | 1 |   | 2 |  // 1(-9), 2(-7)
	//  -------------  
	//  |   | O |   |
	//  -------------
	//  | 3 |   | 4 |  // 3(+7), 4(+9)
	//  -------------
	int adjacentDiagonal1 = index - 9;
	int adjacentDiagonal2 = index - 7;
	int adjacentDiagonal3 = index + 7;
	int adjacentDiagonal4 = index + 9;
	CheckersTileType playerKingPiece = CheckersTileType::Player1King;
	CheckersTileType opponentRegularPiece = CheckersTileType::Player2Piece;
	CheckersTileType opponentKingPiece = CheckersTileType::Player2King;
	
	int row = index / 8;
	int col = index % 8;
	
	if (!m_isPlayer1)
	{
		playerKingPiece = CheckersTileType::Player2King;
		opponentRegularPiece = CheckersTileType::Player1Piece;
		opponentKingPiece = CheckersTileType::Player1King;
	}
	
	bool captureAvailable = false;

	// Confirm the selection is the players king
	if (m_checkersBoardTiles[index].GetCheckersTileType() != playerKingPiece)
	{
		return captureAvailable;
	}
	
	if (col > 0)
	{
		if (row > 0)
		{
			if (m_checkersBoardTiles[adjacentDiagonal1].GetCheckersTileType() == opponentRegularPiece
				|| m_checkersBoardTiles[adjacentDiagonal1].GetCheckersTileType() == opponentKingPiece)
			{
				int nextDiagonal = adjacentDiagonal1 - 9;
				if ((col > 1 && row > 1)
					&& m_checkersBoardTiles[nextDiagonal].GetCheckersTileType() == CheckersTileType::Black)
				{
					m_checkersBoardTiles[nextDiagonal].SetSelected(SelectionType::Destination);
					captureAvailable = true;
				}
			}
		}
			
		if (row < 7)
		{
			if (m_checkersBoardTiles[adjacentDiagonal3].GetCheckersTileType() == opponentRegularPiece
				|| m_checkersBoardTiles[adjacentDiagonal3].GetCheckersTileType() == opponentKingPiece)
			{
				int nextDiagonal = adjacentDiagonal3 + 7;
				if ((col > 1 && row < 6)
					&& m_checkersBoardTiles[nextDiagonal].GetCheckersTileType() == CheckersTileType::Black)
				{
					m_checkersBoardTiles[nextDiagonal].SetSelected(SelectionType::Destination);
					captureAvailable = true;
				}
			}
		}		
	}
	
	if (col < 7)
	{
		if (row > 0)
		{
			if (m_checkersBoardTiles[adjacentDiagonal2].GetCheckersTileType() == opponentRegularPiece
				|| m_checkersBoardTiles[adjacentDiagonal2].GetCheckersTileType() == opponentKingPiece)
			{
				int nextDiagonal = adjacentDiagonal2 - 7;
				if ((col < 6 && row > 1)
					&& m_checkersBoardTiles[nextDiagonal].GetCheckersTileType() == CheckersTileType::Black)
				{
					m_checkersBoardTiles[nextDiagonal].SetSelected(SelectionType::Destination);
					captureAvailable = true;
				}
			}
		}
			
		if (row < 7)
		{
			if (m_checkersBoardTiles[adjacentDiagonal4].GetCheckersTileType() == opponentRegularPiece
				|| m_checkersBoardTiles[adjacentDiagonal4].GetCheckersTileType() == opponentKingPiece)
			{
				int nextDiagonal = adjacentDiagonal4 + 9;
				if ((col < 6 && row < 6)
					&& m_checkersBoardTiles[nextDiagonal].GetCheckersTileType() == CheckersTileType::Black)
				{
					m_checkersBoardTiles[nextDiagonal].SetSelected(SelectionType::Destination);
					captureAvailable = true;
				}
			}
		}		
	}

	return captureAvailable;
}