#include <fstream>
#include <string>

#include "GameSettings.h"
#include "SceneManager.h"
#include "ResourceMapper.h"

#include "Quanta/QWindow.h"
#include "Quanta/QRenderer.h"
#include "Quanta/QApplication.h"

using namespace Quanta;

void GameSettings::Init(
	SceneManager* sceneManager,
	ResourceMapper* resourceMapper,
	QWindow* window,
	QRenderer* renderer)	
{
	m_sceneManager = sceneManager;
	m_resourceMapper = resourceMapper;
	m_window = window;
	m_renderer = renderer;

	LoadSettingsFromFile();

	m_resourceMapper->Map(m_gameTheme);
	m_sceneManager->OnGameThemeChanged(m_gameTheme);
}

void GameSettings::SetMusicEnabled(bool enabled)
{
	m_musicEnabled = enabled;

	irrklang::ISoundEngine& soundEngine = m_sceneManager->GetSoundEngine();
	irrklang::ISoundSource* bgMusicSource = soundEngine.getSoundSource("Media/Audio/Music/background.wav");
	bool musicPlaying = soundEngine.isCurrentlyPlaying(bgMusicSource);

	if (!m_musicEnabled && musicPlaying)
	{
		soundEngine.removeSoundSource(bgMusicSource);
	}
	else if (m_musicEnabled && !musicPlaying)
	{
		soundEngine.play2D(bgMusicSource, true);
	}	
}

void GameSettings::SetSoundEffectsEnabled(bool enabled)
{
	m_sfxEnabled = enabled;
}

void GameSettings::SetVSyncEnabled(bool enabled)
{
	m_vSyncEnabled = enabled;
	m_renderer->SetVSyncEnabled(m_vSyncEnabled);
}

void GameSettings::SetGameTheme(GameTheme theme)
{
	auto prevTheme = m_gameTheme;
	m_gameTheme = theme;

	if (m_gameTheme == GameTheme::Theme1)
		m_window->SetCursorImage("Media/theme_1.cur");
	else if (m_gameTheme == GameTheme::Theme2)
		m_window->SetCursorImage("Media/theme_2.cur");

	if (m_gameTheme != prevTheme)
	{
		m_gameTheme = theme;
		m_resourceMapper->Map(m_gameTheme);
		m_sceneManager->OnGameThemeChanged(m_gameTheme);
	}	
}

void GameSettings::SetAIDifficulty(AIDifficulty difficulty)
{
	m_aiDifficulty = difficulty;
}

bool GameSettings::IsMusicEnabled() const
{
	return m_musicEnabled;
}

bool GameSettings::IsSoundEffectsEnabled() const
{
	return m_sfxEnabled;
}

bool GameSettings::IsVSyncEnabled() const
{ 
	return m_vSyncEnabled;
}

GameTheme GameSettings::GetGameTheme() const
{
	return m_gameTheme;
}

AIDifficulty GameSettings::GetAIDifficulty() const
{
	return m_aiDifficulty;
}

void GameSettings::Save() const
{
	std::wstring dataDirectory = QApplication::GetWorkingDirectory() + L"data/";

	if (CreateDirectoryW(dataDirectory.c_str(), nullptr)
		|| ERROR_ALREADY_EXISTS == GetLastError())
	{
		// Save the game settings to the configuration file.
		std::wstring configFile = dataDirectory + L"settings.ini";
		std::ofstream iniFile(configFile);
		if (!iniFile.good())
			return;

		iniFile << "music=" << m_musicEnabled << '\n';
		iniFile << "sfx=" << m_sfxEnabled << '\n';
		iniFile << "vsync=" << m_vSyncEnabled << '\n';
		iniFile << "theme=" << static_cast<int>(m_gameTheme) << '\n';
		iniFile << "ai_difficulty=" << static_cast<int>(m_aiDifficulty) << '\n';

		iniFile.close();
	}
}

const std::wstring& GameSettings::GetFontFamily() const
{
	return m_fontFamily;
}

void GameSettings::LoadSettingsFromFile()
{
	std::wstring dataDirectory = QApplication::GetWorkingDirectory() + L"data/";
	std::wstring configFile = dataDirectory + L"settings.ini";

	std::ifstream iniFile(configFile);

	if (!iniFile.good())
		return;

	std::vector<std::string> fileContents;
	std::string line;
	while (std::getline(iniFile, line))
	{
		fileContents.push_back(line);
	}

	for (auto& keyval : fileContents)
	{
		auto sep = keyval.find_last_of('=');
		if (sep != std::string::npos)
		{
			// Get the key-val pair from the file
			std::string key = keyval.substr(0, sep);
			std::string val = keyval.substr(sep+1, keyval.back());

			if (key == "music")
			{
				SetMusicEnabled(val == "1");
			}				
			else if (key == "sfx")
			{
				SetSoundEffectsEnabled(val == "1");
			}			
			else if (key == "vsync")
			{
				SetVSyncEnabled(val == "1");
			}			
			else if (key == "theme")
			{
				if (val == "0")
				{
					SetGameTheme(GameTheme::Theme1);
				}
				else if (val == "1")
				{
					SetGameTheme(GameTheme::Theme2);
				}
			}
			else if (key == "ai_difficulty")
			{
				SetAIDifficulty(atoi(val.c_str()));
			}
		}
	}
}
