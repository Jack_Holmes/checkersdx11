#include "Application.h"
#include <memory>

#include "cuda_runtime_api.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	/*int devCount = 0;
	cudaError_t res;
	res = cudaGetDeviceCount(&devCount);
	if (res != cudaError::cudaSuccess || !(devCount >= 1))
	{
		MessageBox(nullptr, "This application requires a CUDA compute capable device.", "Unable to start application", MB_ICONERROR);
		return -1;
	}*/

	auto app = std::make_unique<Application>();

	return app->Run();
}