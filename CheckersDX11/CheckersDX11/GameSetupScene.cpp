#include "Quanta/QRenderer.h"
#include "Quanta/QWindow.h"

#include "GameSetupScene.h"
#include "GameScene.h"
#include "SceneManager.h"
#include "TexturePool.h"
#include "CheckersBoardFileHandler.h"
#include "GameSettings.h"

using namespace Quanta;
using Microsoft::WRL::ComPtr;

GameSetupScene::GameSetupScene(SceneManager* sceneManager)
	: Scene("game_setup", sceneManager)
	, m_camera({0, 0, 0})
	, m_player1(PlayerType::Human)
	, m_player2(PlayerType::Human)
	, m_checkersBoardLoaded(false)
{
	InitGUI();
}

void GameSetupScene::ProcessEvent(Quanta::QEvent& e)
{
	m_guiCanvas->ProcessEvent(e);

	if (e.type == QEvent::Type::Resized)
	{
		RecalculateSprites();
	}
}

void GameSetupScene::Update(float deltaTime)
{
}

void GameSetupScene::Render()
{
	QRenderer& renderer = m_sceneManager->GetRenderer();
	QWindow& window = m_sceneManager->GetWindow();

	renderer.BeginSpritePass(m_camera);
	renderer.DrawSprite(m_sprBackground);
	renderer.DrawSprite(m_sprPlayerSelectPanel1);
	renderer.DrawSprite(m_sprPlayerSelectPanel2);
	m_guiCanvas->Render();	
	renderer.EndSpritePass();
	
	m_guiCanvas->DrawString(m_titleTextLayout.Get(), float(window.GetWidth() / 2 - 500), 10.0f, m_primaryTextColour);
	m_guiCanvas->DrawString(
		m_playerSelect1TextLayout.Get(),
		float(m_sprPlayerSelectPanel1.GetPosition().x - m_sprPlayerSelectPanel1.GetSize().x / 2),
		float(m_sprPlayerSelectPanel1.GetPosition().y - m_sprPlayerSelectPanel1.GetSize().y / 2) + 10.0f,
		m_primaryTextColour);
	m_guiCanvas->DrawString(
		m_playerSelect2TextLayout.Get(),
		float(m_sprPlayerSelectPanel2.GetPosition().x - m_sprPlayerSelectPanel2.GetSize().x / 2),
		float(m_sprPlayerSelectPanel2.GetPosition().y - m_sprPlayerSelectPanel2.GetSize().y / 2) + 10.0f,
		m_primaryTextColour);
	m_guiCanvas->DrawString(
		m_loadedFileTextLayout.Get(),
		float(m_loadButton->GetPosition().x + m_loadButton->GetSize().x / 2 + 10),
		float(m_loadButton->GetPosition().y),
		m_primaryTextColour);
}

void GameSetupScene::OnEnter()
{
	RecalculateSprites();
	m_checkersBoardLoaded = false;
	
	auto gameScene = dynamic_cast<GameScene*>(m_sceneManager->GetScene("game"));
	if (gameScene)
	{
		gameScene->ResetGame();
	}
}

void GameSetupScene::OnGameThemeChanged(GameTheme theme)
{
	m_primaryTextColour = 0xff000000;

	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		m_primaryTextColour = 0xff27848c;
	}

	m_radio1Human->SetTextColour(m_primaryTextColour);
	m_radio1AI->SetTextColour(m_primaryTextColour);
	m_radio2Human->SetTextColour(m_primaryTextColour);
	m_radio2AI->SetTextColour(m_primaryTextColour);
	m_checkbox1GPU->SetTextColour(m_primaryTextColour);
	m_checkbox2GPU->SetTextColour(m_primaryTextColour);
	m_okButton->SetTextColour(m_primaryTextColour);
	m_loadButton->SetTextColour(m_primaryTextColour);
	m_homeButton->SetTextColour(m_primaryTextColour);
}

void GameSetupScene::InitGUI()
{
	TexturePool& texturePool = TexturePool::Instance();
	QWindow& window = m_sceneManager->GetWindow();

	std::wstring fontFamily = GameSettings::Instance().GetFontFamily();

	m_sprBackground.SetTexture(texturePool.GetTexture(TextureID::Background));

	// Create the two panels
	m_sprPlayerSelectPanel1.SetTexture(texturePool.GetTexture(TextureID::PlayerSelectPanel));
	m_sprPlayerSelectPanel2.SetTexture(texturePool.GetTexture(TextureID::PlayerSelectPanel));

	// Create screen text
	auto dwriteFactory = m_guiCanvas->GetDWriteFactory();

	ComPtr<IDWriteTextFormat> titleTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		70.0f,
		L"en-gb",
		titleTextFormat.GetAddressOf());
	titleTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);

	ComPtr<IDWriteTextFormat> playerSelectTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		30.0f,
		L"en-gb",
		playerSelectTextFormat.GetAddressOf());
	playerSelectTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);

	ComPtr<IDWriteTextFormat> playerTypesTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		18.0f,
		L"en-gb",
		playerTypesTextFormat.GetAddressOf());

	auto titleText = L"Game Setup";
	dwriteFactory->CreateTextLayout(
		titleText,
		wcslen(titleText),
		titleTextFormat.Get(),
		1000.0f,
		400.0f,
		m_titleTextLayout.GetAddressOf());

	auto playerSelect1Text = L"Player 1";
	dwriteFactory->CreateTextLayout(
		playerSelect1Text,
		wcslen(playerSelect1Text),
		playerSelectTextFormat.Get(),
		static_cast<float>(m_sprPlayerSelectPanel1.GetSize().x),
		static_cast<float>(m_sprPlayerSelectPanel1.GetSize().y),
		m_playerSelect1TextLayout.GetAddressOf());

	auto playerSelect2Text = L"Player 2";
	dwriteFactory->CreateTextLayout(
		playerSelect2Text,
		wcslen(playerSelect2Text),
		playerSelectTextFormat.Get(),
		static_cast<float>(m_sprPlayerSelectPanel2.GetSize().x),
		static_cast<float>(m_sprPlayerSelectPanel2.GetSize().y),
		m_playerSelect2TextLayout.GetAddressOf());

	SetLoadedGameText(L"Loaded Game: (default)");

	float fontSize = 24.0f;
	int verticalSpacing = 20;
	float horizontalPadding = 30.0f;

	m_primaryTextColour = 0xff000000;

	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		m_primaryTextColour = 0xff27848c;
	}

	// Create player 1 radio group
	m_radioGroup1 = m_guiCanvas->AddRadioButtonGroup();
	m_radioGroup1->Init(texturePool.GetTexture(TextureID::Radio_On), texturePool.GetTexture(TextureID::Radio_Off));
	m_radioGroup1->SetPosition(100, 100);
	m_radioGroup1->SetVerticalSpacing(verticalSpacing);
	{
		m_radio1Human = m_radioGroup1->AddRadioButton();
		m_radio1Human->Init(L"Human", fontFamily, fontSize);
		m_radio1Human->SetCheckedCallback([&](){ m_player1.SetPlayerType(PlayerType::Human); });
		m_radio1Human->SetHorizontalPadding(horizontalPadding);
		m_radio1Human->SetTextColour(m_primaryTextColour);
	}
	{
		m_radio1AI = m_radioGroup1->AddRadioButton();
		m_radio1AI->SetHorizontalPadding(horizontalPadding);
		m_radio1AI->SetCheckedCallback(
			[&](){ 
			m_player1.SetPlayerType(PlayerType::CPU);
			m_checkbox1GPU->SetVisibility(true); });
		m_radio1AI->SetUncheckedCallback([&](){ m_checkbox1GPU->SetVisibility(false); });
		m_radio1AI->Init(L"AI", fontFamily, fontSize);
		m_radio1AI->SetTextColour(m_primaryTextColour);
	}

	// Create player 2 radio group
	m_radioGroup2 = m_guiCanvas->AddRadioButtonGroup();
	m_radioGroup2->Init(texturePool.GetTexture(TextureID::Radio_On), texturePool.GetTexture(TextureID::Radio_Off));
	m_radioGroup2->SetPosition(400, 100);
	m_radioGroup2->SetVerticalSpacing(verticalSpacing);
	{
		m_radio2Human = m_radioGroup2->AddRadioButton();
		m_radio2Human->Init(L"Human", fontFamily, fontSize);
		m_radio2Human->SetCheckedCallback([&](){ m_player2.SetPlayerType(PlayerType::Human); });
		m_radio2Human->SetHorizontalPadding(horizontalPadding);
		m_radio2Human->SetTextColour(m_primaryTextColour);
	}
	{
		m_radio2AI = m_radioGroup2->AddRadioButton();
		m_radio2AI->SetCheckedCallback(
			[&](){
			m_player2.SetPlayerType(PlayerType::CPU);
			m_checkbox2GPU->SetVisibility(true); });
		m_radio2AI->SetUncheckedCallback([&](){ m_checkbox2GPU->SetVisibility(false); });
		m_radio2AI->SetHorizontalPadding(horizontalPadding);
		m_radio2AI->Init(L"AI", fontFamily, fontSize);
		m_radio2AI->SetTextColour(m_primaryTextColour);
	}

	// Create player 1 checkboxes
	m_checkbox1GPU = m_guiCanvas->AddCheckbox();
	m_checkbox1GPU->Init(texturePool.GetTexture(TextureID::Checkbox_On), texturePool.GetTexture(TextureID::Checkbox_Off), L"GPU Acceleration", fontFamily, 15.0f);
	m_checkbox1GPU->SetVisibility(false);
	m_checkbox1GPU->SetHorizontalPadding(15.0f);
	m_checkbox1GPU->SetCheckedCallback([&](){ m_player1.SetPlayerType(PlayerType::GPU); });
	m_checkbox1GPU->SetUncheckedCallback([&](){ m_player1.SetPlayerType(PlayerType::CPU); });
	m_checkbox1GPU->SetTextColour(m_primaryTextColour);

	// Create player 2 checkboxes
	m_checkbox2GPU = m_guiCanvas->AddCheckbox();
	m_checkbox2GPU->Init(texturePool.GetTexture(TextureID::Checkbox_On), texturePool.GetTexture(TextureID::Checkbox_Off), L"GPU Acceleration", fontFamily, 15.0f);
	m_checkbox2GPU->SetVisibility(false);
	m_checkbox2GPU->SetHorizontalPadding(15.0f);
	m_checkbox2GPU->SetCheckedCallback([&](){ m_player2.SetPlayerType(PlayerType::GPU); });
	m_checkbox2GPU->SetUncheckedCallback([&](){ m_player2.SetPlayerType(PlayerType::CPU); });
	m_checkbox2GPU->SetTextColour(m_primaryTextColour);

	// Create buttons
	m_okButton = m_guiCanvas->AddButton();
	m_okButton->Init(texturePool.GetTexture(TextureID::ButtonNormalIdle), texturePool.GetTexture(TextureID::ButtonNormalMouseOver), L"OK", fontFamily, 54.0f);
	m_okButton->SetCallback(std::bind(&GameSetupScene::OnOKPressed, this));
	m_okButton->SetTextColour(m_primaryTextColour);

	m_loadButton = m_guiCanvas->AddButton();
	m_loadButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Load save file", fontFamily, 18.0f);
	m_loadButton->SetCallback(std::bind(&GameSetupScene::OnLoadPressed, this));
	m_loadButton->SetTextColour(m_primaryTextColour);

	m_homeButton = m_guiCanvas->AddButton();
	m_homeButton->Init(texturePool.GetTexture(TextureID::ButtonHomeIdle), texturePool.GetTexture(TextureID::ButtonHomeMouseOver), L"", fontFamily, 0.0f);
	m_homeButton->SetCallback(std::bind(&GameSetupScene::OnHomePressed, this));
	m_homeButton->SetTextColour(m_primaryTextColour);

	RecalculateSprites();
}

void GameSetupScene::OnOKPressed() const
{
	GameScene* gameScene = dynamic_cast<GameScene*>(m_sceneManager->GetScene("game"));
	if (gameScene)
	{
		gameScene->SetUp(m_player1, m_player2, true);
		if (m_checkersBoardLoaded)
		{
			gameScene->SetCheckersBoard(m_checkersBoard);
		}
	}

	m_sceneManager->SetScene("game");
}

void GameSetupScene::OnLoadPressed()
{
	std::wstring filename;
	m_checkersBoardLoaded = CheckersBoardFileHandler::Load(m_checkersBoard, filename, m_sceneManager->GetWindow());
	if (m_checkersBoardLoaded)
	{
		SetLoadedGameText(L"Loaded Game: " + filename);
	}
}

void GameSetupScene::OnHomePressed() const
{
	m_sceneManager->SetScene("menu");
}

void GameSetupScene::SetLoadedGameText(const std::wstring& text)
{
	auto dwriteFactory = m_guiCanvas->GetDWriteFactory();
	std::wstring fontFamily = GameSettings::Instance().GetFontFamily();

	ComPtr<IDWriteTextFormat> loadedFileTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		18.0f,
		L"en-gb",
		loadedFileTextFormat.GetAddressOf());

	dwriteFactory->CreateTextLayout(
		text.c_str(),
		wcslen(text.c_str()),
		loadedFileTextFormat.Get(),
		400.0f,
		100.0f,
		m_loadedFileTextLayout.GetAddressOf());
}

void GameSetupScene::RecalculateSprites()
{
	QWindow& window = m_sceneManager->GetWindow();
	UINT windowWidth = window.GetWidth();
	UINT windowHeight = window.GetHeight();

	m_sprBackground.SetScale({ windowWidth / 512.0f, windowHeight / 512.0f });
	m_sprBackground.SetPosition({ int(windowWidth / 2), int(windowHeight / 2) });	

	m_sprPlayerSelectPanel1.SetPosition({ 
		int(windowWidth / 2 - m_sprPlayerSelectPanel1.GetSize().x / 2) - 50,
		int(windowHeight / 2) + 50});
	m_sprPlayerSelectPanel2.SetPosition({ 
		int(windowWidth / 2 + m_sprPlayerSelectPanel2.GetSize().x / 2) + 50,
		int(windowHeight / 2) + 50});

	m_radioGroup1->SetPosition(
		int(m_sprPlayerSelectPanel1.GetPosition().x - m_sprPlayerSelectPanel1.GetSize().x / 2 + 40.0f),
		int(m_sprPlayerSelectPanel1.GetPosition().y - m_sprPlayerSelectPanel1.GetSize().y / 2 + 100.0f));
	m_radioGroup2->SetPosition(
		int(m_sprPlayerSelectPanel2.GetPosition().x - m_sprPlayerSelectPanel2.GetSize().x / 2 + 40.0f),
		int(m_sprPlayerSelectPanel2.GetPosition().y - m_sprPlayerSelectPanel2.GetSize().y / 2 + 100.0f));

	m_checkbox1GPU->SetPosition(int(m_radio1AI->GetSprite().GetPosition().x + 20.0f), int(m_radio1AI->GetSprite().GetPosition().y + 40.0f));
	m_checkbox2GPU->SetPosition(int(m_radio2AI->GetSprite().GetPosition().x + 20.0f), int(m_radio2AI->GetSprite().GetPosition().y + 40.0f));

	m_homeButton->SetPosition(m_homeButton->GetSize().x / 2 + 10, m_homeButton->GetSize().y / 2 + 10);
	m_okButton->SetPosition(windowWidth - m_okButton->GetSize().x / 2 - 30, windowHeight - m_okButton->GetSize().y / 2 - 30);
	m_loadButton->SetPosition(
		m_sprPlayerSelectPanel1.GetPosition().x - m_sprPlayerSelectPanel1.GetSize().x / 2 + m_loadButton->GetSize().x / 2,
		m_sprPlayerSelectPanel1.GetPosition().y - m_sprPlayerSelectPanel1.GetSize().y / 2 - m_loadButton->GetSize().y / 2 - 50);
}
