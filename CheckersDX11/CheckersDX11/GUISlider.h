#pragma once

#include <functional>
#include <string>
#include <dwrite.h>

#include "Quanta/QSprite.h"
#include "Quanta/QEvent.h"
#include "Quanta/QRenderer.h"

// Forward declarations
class GUICanvas;
class QTexture;

class GUISlider
{
public:
	explicit GUISlider(GUICanvas* parentCanvas);

	void Init(
		Quanta::QTexture* sliderTexture,
		Quanta::QTexture* handleTexture,
		const std::wstring& text,
		const std::wstring& fontFamily,
		float fontSize,
		int minVal,
		int maxVal,
		int curVal);

	void SetValueChangedCallback(std::function<void()> callback);

	void Render(Quanta::QRenderer& renderer);

	void ProcessEvent(const Quanta::QEvent& e);

	void SetPosition(int x, int y);
	void SetTextColour(UINT32 colour);
	void SetValue(int val);
	
	void SetVisiblity(bool visible);

	void SetActive(bool active);	

	int GetValue() const;

private:
	void CalculateHandlePosition();

	void CalculateValueFromHandlePosition();

	// The parent canvas that controls this slider.
	GUICanvas* m_parentCanvas;

	// The text layout used for the text displayed next to the slider.
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_textLayout;

	// The two textures that are used for the slider and the slider handle
	Quanta::QTexture* m_texSlider;
	Quanta::QTexture* m_texHandle;

	// The sprites which represents the drawable slider.
	Quanta::QSprite m_sprSlider;
	Quanta::QSprite m_sprHandle;

	std::function<void()> m_valueChangedCallback;

	UINT32 m_textColour;
	int m_fontSize;

	// Flag to determine if the slider is drawn to the screen.
	bool m_isVisible;
	// Flag to determine if the slider responds to user interaction.
	bool m_isActive;

	bool m_selected;

	int m_minValue;
	int m_maxValue;
	int m_currentValue;

	int m_startPosX;
	int m_endPosX;
	int m_lengthX;
	int m_pixelsPerValue;
};