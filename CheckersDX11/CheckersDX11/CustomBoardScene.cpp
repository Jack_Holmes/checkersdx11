#include "Quanta/QRenderer.h"

#include "CustomBoardScene.h"
#include "SceneManager.h"
#include "TexturePool.h"
#include "GameScene.h"
#include "CheckersBoardFileHandler.h"
#include "GameSettings.h"

using namespace Quanta;
using Microsoft::WRL::ComPtr;

CustomBoardScene::CustomBoardScene(SceneManager* sceneManager)
	: Scene("custom_board", sceneManager)
	, m_camera({0, 0, 0})
	, m_selectedTileType(CheckersTileType::Player1Piece)
{
	InitGUI();
}

void CustomBoardScene::ProcessEvent(Quanta::QEvent& e)
{
	m_guiCanvas->ProcessEvent(e);

	if (e.type == QEvent::Type::Resized)
	{
		RecalculateSprites();
	}
	else if (e.type == QEvent::Type::MouseButtonPressed)
	{
		if (e.mouse.button == QMouse::Button::Left)
		{
			if (m_guiCanvas->MouseIntersects(m_sprWhitePiece))
			{
				m_selectedTileType = CheckersTileType::Player1Piece;
				m_sprSelected.SetPosition(m_sprWhitePiece.GetPosition());
				return;
			}
			else if (m_guiCanvas->MouseIntersects(m_sprWhiteKing))
			{
				m_selectedTileType = CheckersTileType::Player1King;
				m_sprSelected.SetPosition(m_sprWhiteKing.GetPosition());
				return;
			}
			else if (m_guiCanvas->MouseIntersects(m_sprRedPiece))
			{
				m_selectedTileType = CheckersTileType::Player2Piece;
				m_sprSelected.SetPosition(m_sprRedPiece.GetPosition());
				return;
			}
			else if (m_guiCanvas->MouseIntersects(m_sprRedKing))
			{
				m_selectedTileType = CheckersTileType::Player2King;
				m_sprSelected.SetPosition(m_sprRedKing.GetPosition());
				return;
			}

			for (auto& tile : m_checkersTiles)
			{
				if (m_guiCanvas->MouseIntersects(tile.sprite))
				{
					if (tile.tileType != CheckersTileType::White)
					{
						tile.tileType = m_selectedTileType;
						SetSpriteTextureByTileType(m_selectedTileType, tile.sprite);
						return;
					}
				}
			}			
		}
		else if (e.mouse.button == QMouse::Button::Right)
		{
			for (auto& tile : m_checkersTiles)
			{
				if (m_guiCanvas->MouseIntersects(tile.sprite))
				{
					if (tile.tileType != CheckersTileType::White)
					{
						tile.tileType = CheckersTileType::Black;
						SetSpriteTextureByTileType(tile.tileType, tile.sprite);
						return;
					}
				}
			}
		}		
	}
}

void CustomBoardScene::Update(float deltaTime)
{
}

void CustomBoardScene::Render()
{
	QRenderer& renderer = m_sceneManager->GetRenderer();
	QWindow& window = m_sceneManager->GetWindow();

	renderer.BeginSpritePass(m_camera);

	renderer.DrawSprite(m_sprBackground);

	for (auto& checkersTile : m_checkersTiles)
	{
		renderer.DrawSprite(checkersTile.sprite);
	}
		
	RenderTileTypes();
	renderer.DrawSprite(m_sprSelected);
	
	renderer.EndSpritePass();

	m_guiCanvas->Render();

	m_guiCanvas->DrawString(m_titleTextLayout.Get(), float(window.GetWidth() / 2 - 500), 10.0f, m_primaryTextColour);
}

void CustomBoardScene::OnEnter()
{
	InitCheckersTiles();
	RecalculateSprites();
}

void CustomBoardScene::OnGameThemeChanged(GameTheme theme)
{
	m_primaryTextColour = 0xff000000;

	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		m_primaryTextColour = 0xff27848c;
	}

	m_clearButton->SetTextColour(m_primaryTextColour);
	m_loadFromFileButton->SetTextColour(m_primaryTextColour);
	m_saveToFileButton->SetTextColour(m_primaryTextColour);
	m_playButton->SetTextColour(m_primaryTextColour);
	m_homeButton->SetTextColour(m_primaryTextColour);
}

void CustomBoardScene::InitGUI()
{
	TexturePool& texturePool = TexturePool::Instance();

	m_sprBackground.SetTexture(texturePool.GetTexture(TextureID::Background));
	m_sprWhitePiece.SetTexture(texturePool.GetTexture(TextureID::Square_WhiteCircle_56x56));
	m_sprWhiteKing.SetTexture(texturePool.GetTexture(TextureID::Square_WhiteCircleKing_56x56));
	m_sprRedPiece.SetTexture(texturePool.GetTexture(TextureID::Square_RedCircle_56x56));
	m_sprRedKing.SetTexture(texturePool.GetTexture(TextureID::Square_RedCircleKing_56x56));
	m_sprSelected.SetTexture(texturePool.GetTexture(TextureID::Square_Border_56x56));

	m_sprSelected.SetColour({0, 1, 0, 1});

	// Fonts
	auto dwriteFactory = m_guiCanvas->GetDWriteFactory();

	std::wstring fontFamily = GameSettings::Instance().GetFontFamily();

	ComPtr<IDWriteTextFormat> titleTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		70.0f,
		L"en-gb",
		titleTextFormat.GetAddressOf());
	titleTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);

	auto selectionText = L"Editor";
	dwriteFactory->CreateTextLayout(
		selectionText,
		wcslen(selectionText),
		titleTextFormat.Get(),
		1000.0f,
		400.0f,
		m_titleTextLayout.GetAddressOf());

	m_primaryTextColour = 0xff000000;

	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		m_primaryTextColour = 0xff27848c;
	}

	m_clearButton = m_guiCanvas->AddButton();
	m_clearButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Clear", fontFamily, 24.0f);
	m_clearButton->SetCallback(std::bind(&CustomBoardScene::InitCheckersTiles, this));
	m_clearButton->SetTextColour(m_primaryTextColour);

	m_loadFromFileButton = m_guiCanvas->AddButton();
	m_loadFromFileButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Load From File", fontFamily, 18.0f);
	m_loadFromFileButton->SetCallback(std::bind(&CustomBoardScene::OnLoadFilePressed, this));
	m_loadFromFileButton->SetTextColour(m_primaryTextColour);

	m_saveToFileButton = m_guiCanvas->AddButton();
	m_saveToFileButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Save To File", fontFamily, 18.0f);
	m_saveToFileButton->SetCallback(std::bind(&CustomBoardScene::OnSaveToFilePressed, this));
	m_saveToFileButton->SetTextColour(m_primaryTextColour);

	m_playButton = m_guiCanvas->AddButton();
	m_playButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Play with this layout", fontFamily, 18.0f);
	m_playButton->SetCallback(std::bind(&CustomBoardScene::OnPlayPressed, this));
	m_playButton->SetTextColour(m_primaryTextColour);

	m_homeButton = m_guiCanvas->AddButton();
	m_homeButton->Init(texturePool.GetTexture(TextureID::ButtonHomeIdle), texturePool.GetTexture(TextureID::ButtonHomeMouseOver), L"", fontFamily, 0.0f);
	m_homeButton->SetCallback(std::bind(&CustomBoardScene::OnHomePressed, this));
	m_homeButton->SetTextColour(m_primaryTextColour);

	RecalculateSprites();
}

void CustomBoardScene::InitCheckersTiles()
{
	m_checkersTiles.clear();

	for (int index = 0; index < 64; ++index)
	{
		CheckersTile2D checkersTile;
		checkersTile.index = index;
		checkersTile.tileType = CheckersTileType::White;		

		int row = index / 8;
		int col = index % 8;
		int alternate = (row % 2 == 0) ? 1 : 0;
		if ((col + alternate) % 2 == 0)
		{
			checkersTile.tileType = CheckersTileType::Black;
		}

		QSprite sprite;		
		checkersTile.sprite = sprite;
		SetSpriteTextureByTileType(checkersTile.tileType, checkersTile.sprite);

		m_checkersTiles.push_back(checkersTile);
	}

	CalculateTilePositions();
}

void CustomBoardScene::MapFromCheckersBoard(const CheckersBoard& checkersboard)
{
	InitCheckersTiles();

	for (int index = 0; index < 64; ++index)
	{
		bool p1man, p1king, p2man, p2king;
		p1man = checkersboard.p1Man(index);
		p1king = checkersboard.p1King(index);
		p2man = checkersboard.p2Man(index);
		p2king = checkersboard.p2King(index);

		if (p1man)
		{
			m_checkersTiles[index].tileType = CheckersTileType::Player1Piece;		
		}
		else if (p2man)
		{
			m_checkersTiles[index].tileType = CheckersTileType::Player2Piece;
		}
		else if (p1king)
		{
			m_checkersTiles[index].tileType = CheckersTileType::Player1King;
		}
		else if (p2king)
		{
			m_checkersTiles[index].tileType = CheckersTileType::Player2King;
		}

		SetSpriteTextureByTileType(m_checkersTiles[index].tileType, m_checkersTiles[index].sprite);
	}
}

void CustomBoardScene::MapToCheckersBoard(CheckersBoard& checkersboard)
{
	checkersboard.clear();

	for (auto& checkersTile : m_checkersTiles)
	{
		if (checkersTile.tileType == CheckersTileType::Player1Piece)
		{
			checkersboard.addP1Man(checkersTile.index);
		}
		else if (checkersTile.tileType == CheckersTileType::Player1King)
		{
			checkersboard.addP1King(checkersTile.index);
		}
		else if (checkersTile.tileType == CheckersTileType::Player2Piece)
		{
			checkersboard.addP2Man(checkersTile.index);
		}
		else if (checkersTile.tileType == CheckersTileType::Player2King)
		{
			checkersboard.addP2King(checkersTile.index);
		}
	}
}

void CustomBoardScene::RecalculateSprites()
{
	QWindow& window = m_sceneManager->GetWindow();
	UINT windowWidth = window.GetWidth();
	UINT windowHeight = window.GetHeight();

	m_sprBackground.SetScale({ windowWidth / 512.0f, windowHeight / 512.0f });
	m_sprBackground.SetPosition({ int(windowWidth / 2), int(windowHeight / 2) });

	CalculateTilePositions();

	m_homeButton->SetPosition(m_homeButton->GetSize().x / 2 + 10, m_homeButton->GetSize().y / 2 + 10);

	{
		int x = windowWidth / 2 + 200;
		int y = int(windowHeight * 0.18f);
		int ySep = 60;
		int xSep = 60;
		m_sprWhitePiece.SetPosition({ x, y });
		m_sprWhiteKing.SetPosition({ m_sprWhitePiece.GetPosition().x + xSep, m_sprWhitePiece.GetPosition().y });
		m_sprRedPiece.SetPosition({ x, m_sprWhiteKing.GetPosition().y + ySep });
		m_sprRedKing.SetPosition({ m_sprRedPiece.GetPosition().x + xSep, m_sprRedPiece.GetPosition().y });

		m_sprSelected.SetPosition(m_sprWhitePiece.GetPosition());

		m_clearButton->SetPosition( int(x + m_clearButton->GetSize().x / 2) - 30, m_sprRedKing.GetPosition().y + ySep + 20);
		m_loadFromFileButton->SetPosition(m_clearButton->GetPosition().x, m_clearButton->GetPosition().y + ySep + 20);
		m_saveToFileButton->SetPosition(m_loadFromFileButton->GetPosition().x, m_loadFromFileButton->GetPosition().y + ySep + 20);
		m_playButton->SetPosition(m_saveToFileButton->GetPosition().x, m_saveToFileButton->GetPosition().y + ySep + 20);
	}	
}

void CustomBoardScene::RenderTileTypes()
{
	QRenderer& renderer = m_sceneManager->GetRenderer();
	renderer.DrawSprite(m_sprWhitePiece);
	renderer.DrawSprite(m_sprWhiteKing);
	renderer.DrawSprite(m_sprRedPiece);
	renderer.DrawSprite(m_sprRedKing);
}

void CustomBoardScene::SetSpriteTextureByTileType(CheckersTileType tileType, Quanta::QSprite& sprite)
{
	TexturePool& texturePool = TexturePool::Instance();

	if (tileType == CheckersTileType::White)
	{		
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_56x56));
		sprite.SetColour({ 1, 1, 1, 1 });
	}
	else if (tileType == CheckersTileType::Black)
	{
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_56x56));
		sprite.SetColour({ 0, 0, 0, 1 });
	}
	else if (tileType == CheckersTileType::Player1Piece)
	{
		sprite.SetColour({ 1, 1, 1, 1 });
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_WhiteCircle_56x56));
	}
	else if (tileType == CheckersTileType::Player2Piece)
	{
		sprite.SetColour({ 1, 1, 1, 1 });
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_RedCircle_56x56));
	}
	else if (tileType == CheckersTileType::Player1King)
	{
		sprite.SetColour({ 1, 1, 1, 1 });
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_WhiteCircleKing_56x56));
	}
	else if (tileType == CheckersTileType::Player2King)
	{
		sprite.SetColour({ 1, 1, 1, 1 });
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_RedCircleKing_56x56));
	}
}

void CustomBoardScene::CalculateTilePositions()
{
	QWindow& window = m_sceneManager->GetWindow();

	for (auto& checkersTile : m_checkersTiles)
	{
		int row = checkersTile.index / 8;
		int col = checkersTile.index % 8;
		int x = (window.GetWidth() / 2) - 300 + col * checkersTile.sprite.GetSize().x;
		int y = int(window.GetHeight() * 0.18f) + row * checkersTile.sprite.GetSize().y;
		checkersTile.sprite.SetPosition({x, y});
	}
}

void CustomBoardScene::OnLoadFilePressed()
{
	CheckersBoard checkersBoard;
	checkersBoard.clear();
	std::wstring filename;
	CheckersBoardFileHandler::Load(checkersBoard, filename, m_sceneManager->GetWindow());
	MapFromCheckersBoard(checkersBoard);
}

void CustomBoardScene::OnSaveToFilePressed()
{
	CheckersBoard checkersBoard;
	MapToCheckersBoard(checkersBoard);
	CheckersBoardFileHandler::Save(checkersBoard, m_sceneManager->GetWindow());
}

void CustomBoardScene::OnPlayPressed()
{
	CheckersBoard checkersboard;
	checkersboard.clear();
	
	for (auto& checkersTile : m_checkersTiles)
	{
		if (checkersTile.tileType == CheckersTileType::Player1Piece)
		{
			checkersboard.addP1Man(checkersTile.index);
		}
		else if (checkersTile.tileType == CheckersTileType::Player2Piece)
		{
			checkersboard.addP2Man(checkersTile.index);
		}
		else if (checkersTile.tileType == CheckersTileType::Player1King)
		{
			checkersboard.addP1King(checkersTile.index);
		}
		else if (checkersTile.tileType == CheckersTileType::Player2King)
		{
			checkersboard.addP2King(checkersTile.index);
		}
	}

	auto gameScene = dynamic_cast<GameScene*>(m_sceneManager->GetScene("game"));
	if (gameScene)
	{
		gameScene->ResetGame();
		gameScene->SetCheckersBoard(checkersboard);
		m_sceneManager->SetScene("game");
	}
}

void CustomBoardScene::OnHomePressed() const
{
	m_sceneManager->SetScene("menu");
}
