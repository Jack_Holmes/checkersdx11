#pragma once

#include <vector>

#include "PlayerType.h"

// The CheckersPlayer holds the current state of a player during a
// game of checkers which is specific to that individual player.
class CheckersPlayer
{
public:
	// Construct a CheckersPlayer.
	// playerType: Type of player CPU, GPU, human etc.
	CheckersPlayer(PlayerType playerType);

	// The the player type.
	void SetPlayerType(PlayerType playerType);

	// Get the number of moves this player has made during the
	// current checkers game.
	int GetNumberOfMoves() const;

	// Add a move to the player and set that elapsed time for this
	// particular move.
	void AddMove(float elapsedTime);

	// Get the total elapsed time for all the moves this player
	// has made.
	float GetTotalElapsedTime() const;

	// Get a list of elpased times, for every move the player has made.
	const std::vector<float>& GetElapsedTimes() const;

	// Get the player type.
	PlayerType GetPlayerType() const;

private:
	// Stores what type of player this is. CPU, GPU etc.
	PlayerType m_playerType;
	// The total amount of time the player has spent making
	// their moves.
	float m_totalElapsedTime;
	// The number of moves the player has made during a single game
	// of checkers.
	int m_numberOfMoves;
	// Holds the elapsed time for every move.
	std::vector<float> m_elapsedTimes;
};
