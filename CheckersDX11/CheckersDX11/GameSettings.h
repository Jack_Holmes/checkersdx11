#pragma once

#include <string>

// Forward references
namespace Quanta 
{ 
	class QWindow;
	class QRenderer;
}
class SceneManager;
class ResourceMapper;

// The game theme setting decides what assets are loaded/displayed.
enum class GameTheme
{
	Theme1,
	Theme2
};

// The ai difficulty setting
using AIDifficulty = int;

// The GameSettings singleton is created at the start of an application and
// holds all the values found in the settings file when the application launches.
// It provides the neccessary interface to retrieve and manipulate the game settings
// which will be saved when the application exits.
class GameSettings
{
public:
	// Retrieve the static instance of the game settings.
	static GameSettings& Instance()
	{
		static GameSettings inst;
		return inst;
	}

	// Initialise the game settings. This will load the settings from
	// the configuration file.
	void Init(
		SceneManager* sceneManager,
		ResourceMapper* resourceMapper,
		Quanta::QWindow* window,
		Quanta::QRenderer* renderer);

	// Set whether the music should be playing in the background.
	void SetMusicEnabled(bool enabled);

	// Set whether any sound effects should be playing.
	void SetSoundEffectsEnabled(bool enabled);

	// Set whether vertical-sync is enabled.
	void SetVSyncEnabled(bool enabled);

	// Set the current game theme.
	void SetGameTheme(GameTheme theme);

	// Set the difficulty of the ai.
	void SetAIDifficulty(AIDifficulty difficulty);

	// Check if the music game setting is enabled.
	bool IsMusicEnabled() const;

	// Check if the sound effects game settings is enabled.
	bool IsSoundEffectsEnabled() const;

	// Check if vertical-sync game setting is enabled.
	bool IsVSyncEnabled() const;

	// Get the current selected game theme settings.
	GameTheme GetGameTheme() const;

	// Get the current selected AI difficulty setting.
	AIDifficulty GetAIDifficulty() const;

	// Save the game settings to the configuration file.
	void Save() const;

	// Get the font family setting.
	const std::wstring& GetFontFamily() const;

private:
	// Loads the settings from the configuration file into
	// the GameSettings singleton.
	void LoadSettingsFromFile();

	// Game settings loaded with defaults
	GameSettings()
		: m_sceneManager(nullptr),
		m_window(nullptr),
		m_renderer(nullptr),
		m_musicEnabled(true),
		m_sfxEnabled(true),
		m_vSyncEnabled(true),
		m_gameTheme(GameTheme::Theme1),
		m_aiDifficulty(3),
		m_fontFamily(L"Lucida Sans")
	{
	}

	// Prevent copying the GameSettings instance.
	GameSettings(const GameSettings&) = delete;
	void operator=(const GameSettings&) = delete;

	// The application scene manager.
	SceneManager*      m_sceneManager;
	// The application resource mapper.
	ResourceMapper*    m_resourceMapper;
	// The application window.
	Quanta::QWindow*   m_window;
	// The renderer used for drawing the game assets.
	Quanta::QRenderer* m_renderer;

	// Game settings.
	bool               m_musicEnabled;
	bool               m_sfxEnabled;
	bool               m_vSyncEnabled;
	GameTheme          m_gameTheme;
	AIDifficulty       m_aiDifficulty;
	const std::wstring m_fontFamily;
};
