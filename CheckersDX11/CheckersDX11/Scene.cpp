#include "Scene.h"
#include "SceneManager.h"

#include "Quanta/QRenderer.h"
#include "Quanta/QEvent.h"

Scene::Scene(const std::string& name, SceneManager* sceneManager)
	: m_sceneManager(sceneManager)
	, m_name(name)
{
	m_guiCanvas = std::make_unique<GUICanvas>(
		&m_sceneManager->GetWindow(),
		&m_sceneManager->GetRenderer(),
		m_sceneManager->GetDevice().Get());
}

Scene::~Scene()
{
}

void Scene::ProcessEvent(Quanta::QEvent& e)
{
}

void Scene::Update(float deltaTime)
{
}

void Scene::Render()
{
}

const std::string& Scene::GetName() const
{
	return m_name;
}

void Scene::OnEnter()
{
}

void Scene::OnExit()
{
}

void Scene::OnGameThemeChanged(GameTheme theme)
{
}