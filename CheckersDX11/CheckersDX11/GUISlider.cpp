#include "GUISlider.h"
#include "Quanta/QTexture.h"
#include "GUICanvas.h"

using namespace Quanta;
using Microsoft::WRL::ComPtr;

GUISlider::GUISlider(GUICanvas* parentCanvas)
	: m_parentCanvas(parentCanvas)
	, m_texSlider(nullptr)
	, m_texHandle(nullptr)
	, m_textColour(0xff000000)
	, m_fontSize(20)
	, m_isVisible(true)
	, m_isActive(true)
	, m_selected(false)
	, m_minValue(0)
	, m_maxValue(100)
	, m_currentValue(0)
{
}

void GUISlider::Init(
	Quanta::QTexture* sliderTexture,
	Quanta::QTexture* handleTexture,
	const std::wstring& text,
	const std::wstring& fontFamily,
	float fontSize,
	int minVal,
	int maxVal,
	int curVal)
{
	assert(minVal < maxVal);
	SetValue(curVal);

	m_texSlider = sliderTexture;
	m_texHandle = handleTexture;
	m_sprSlider.SetTexture(m_texSlider);
	m_sprHandle.SetTexture(m_texHandle);

	m_minValue = minVal;
	m_maxValue = maxVal;

	m_fontSize = fontSize;

	auto dwriteFactory = m_parentCanvas->GetDWriteFactory();

	if (!text.empty() && m_fontSize > 0.0f)
	{
		ComPtr<IDWriteTextFormat> textFormat;
		dwriteFactory->CreateTextFormat(
			fontFamily.c_str(),
			nullptr,
			DWRITE_FONT_WEIGHT_REGULAR,
			DWRITE_FONT_STYLE_NORMAL,
			DWRITE_FONT_STRETCH_NORMAL,
			m_fontSize,
			L"en-gb",
			textFormat.GetAddressOf());

		textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);

		dwriteFactory->CreateTextLayout(
			text.c_str(),
			wcslen(text.c_str()),
			textFormat.Get(),
			static_cast<float>(m_sprSlider.GetSize().x),
			50.0f,
			m_textLayout.GetAddressOf());
	}
}

void GUISlider::SetValueChangedCallback(std::function<void()> callback)
{
	m_valueChangedCallback = callback;
}

void GUISlider::Render(Quanta::QRenderer& renderer)
{
	if (!m_isVisible)
	{
		return;
	}

	renderer.DrawSprite(m_sprSlider);
	renderer.DrawSprite(m_sprHandle);

	if (m_textLayout)
	{
		m_parentCanvas->DrawString(
		m_textLayout.Get(),
			float(m_sprSlider.GetPosition().x) - (m_sprSlider.GetSize().x / 2),
			float(m_sprSlider.GetPosition().y) - m_sprSlider.GetSize().y - m_sprHandle.GetSize().y,
			m_textColour);
	}

	m_parentCanvas->DrawString(
		std::to_wstring(m_currentValue),
		m_fontSize,
		m_sprHandle.GetPosition().x - (m_sprHandle.GetSize().x / 4),
		m_sprHandle.GetPosition().y + (m_sprHandle.GetSize().y / 2),
		m_textColour);
}

void GUISlider::ProcessEvent(const Quanta::QEvent& e)
{
	if (!m_isActive)
	{
		return;
	}

	if (e.type == QEvent::Type::MouseButtonPressed &&
		e.mouse.button == QMouse::Button::Left)
	{
		if (m_parentCanvas->MouseIntersects(m_sprHandle))
		{
			m_selected = true;
		}		
	}
	else if (e.type == QEvent::Type::MouseButtonReleased &&
		e.mouse.button == QMouse::Button::Left)
	{
		m_selected = false;
	}
	else if (e.type == QEvent::Type::MouseMoved && m_selected)
	{
		auto mouseXInBounds =
			[&](){ return e.mouse.x > m_startPosX && e.mouse.x < m_endPosX; };

		if ((m_sprHandle.GetPosition().x <= m_endPosX) && (m_sprHandle.GetPosition().x >= m_startPosX) && mouseXInBounds())
		{
			m_sprHandle.SetPosition({ e.mouse.x, m_sprHandle.GetPosition().y });
		}
		else if (e.mouse.x < m_startPosX)
		{
			m_sprHandle.SetPosition({ m_startPosX, m_sprHandle.GetPosition().y });
		}
		else if (e.mouse.x > m_endPosX)
		{
			m_sprHandle.SetPosition({ m_endPosX, m_sprHandle.GetPosition().y });
		}

		CalculateValueFromHandlePosition();
	}
}

void GUISlider::SetPosition(int x, int y)
{
	m_sprSlider.SetPosition({ x, y });

	m_startPosX = m_sprSlider.GetPosition().x - m_sprSlider.GetSize().x / 2;
	m_endPosX = m_startPosX + m_sprSlider.GetSize().x;
	m_lengthX = m_endPosX - m_startPosX;
	m_pixelsPerValue = m_lengthX / (m_maxValue - m_minValue);

	assert(m_pixelsPerValue != 0); // Not enough space to represent all the values!

	CalculateHandlePosition();
}

void GUISlider::SetTextColour(UINT32 colour)
{
	m_textColour = colour;
}

void GUISlider::SetValue(int val)
{
	assert(val >= m_minValue);
	assert(val <= m_maxValue);
	m_currentValue = val;
	CalculateHandlePosition();

	if (m_valueChangedCallback)
	{
		m_valueChangedCallback();
	}
}

void GUISlider::SetVisiblity(bool visible)
{
	m_isVisible = visible;
}

void GUISlider::SetActive(bool active)
{
	m_isActive = active;
}

int GUISlider::GetValue() const
{
	return m_currentValue;
}

void GUISlider::CalculateHandlePosition()
{
	int xPos = m_startPosX + (m_currentValue - m_minValue) * m_pixelsPerValue;
	int yPos = m_sprSlider.GetPosition().y;
	m_sprHandle.SetPosition({ xPos, yPos });
}

void GUISlider::CalculateValueFromHandlePosition()
{
	int xPos = m_sprHandle.GetPosition().x;
	int deltaX = xPos - m_startPosX;
	m_currentValue = m_minValue + deltaX / m_pixelsPerValue;
	if (m_currentValue < m_minValue)
	{
		m_currentValue = m_minValue;
	}
	else if (m_currentValue > m_maxValue)
	{
		m_currentValue = m_maxValue;
	}

	if (m_valueChangedCallback)
	{
		m_valueChangedCallback();
	}
}