#include "CheckersPlayer.h"	

CheckersPlayer::CheckersPlayer(PlayerType playerType)
	: m_playerType(playerType)
	, m_totalElapsedTime(0.0f)
	, m_numberOfMoves(0)
{
}

void CheckersPlayer::SetPlayerType(PlayerType playerType)
{
	m_playerType = playerType;
}

int CheckersPlayer::GetNumberOfMoves() const
{
	return m_numberOfMoves;
}

void CheckersPlayer::AddMove(float elapsedTime)
{
	++m_numberOfMoves;
	m_totalElapsedTime += elapsedTime;
	m_elapsedTimes.push_back(elapsedTime);
}

float CheckersPlayer::GetTotalElapsedTime() const
{
	return m_totalElapsedTime;
}

const std::vector<float>& CheckersPlayer::GetElapsedTimes() const
{
	return m_elapsedTimes;
}

PlayerType CheckersPlayer::GetPlayerType() const
{
	return m_playerType;
}
