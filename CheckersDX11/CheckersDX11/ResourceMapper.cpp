#include "ResourceMapper.h"
#include "TexturePool.h"
#include "MeshPool.h"

ResourceMapper::ResourceMapper(ID3D11Device* d3dDevice)
	: m_d3dDevice(d3dDevice)
	, m_initialised(false)
	, m_lastTheme(GameTheme::Theme1) 
{
}

void ResourceMapper::Map(GameTheme theme)
{
	MapTextures(theme);
	MapMeshes(theme);

	m_initialised = true;
	m_lastTheme = theme;
}

void ResourceMapper::MapTextures(GameTheme theme)
{
	TexturePool& texturePool = TexturePool::Instance();
	ID3D11Device* d3dDevice = m_d3dDevice.Get();	

	// Textures that are independent of selected theme
	if (!m_initialised)
	{		
		texturePool.Load(d3dDevice, TextureID::Title, L"title.dds");
		// UI
		texturePool.Load(d3dDevice, TextureID::Green_4x4, L"UI/green_4x4.dds");
		texturePool.Load(d3dDevice, TextureID::PausedBackground, L"UI/paused_background.dds");
		texturePool.Load(d3dDevice, TextureID::SemiTransparentSquare_4x4, L"UI/semi_trans_square.dds");
		// Model Textures
		texturePool.Load(d3dDevice, TextureID::Model_TileSelection, L"Models/tile_selection.dds");
		// Controls
		texturePool.Load(d3dDevice, TextureID::Controls, L"UI/controls.dds");
	}	

	if (theme == GameTheme::Theme1)
	{
		// Backgrounds
		texturePool.Load(d3dDevice, TextureID::Background, L"Backgrounds/background_theme1.dds");
		// UI Textures
		texturePool.Load(d3dDevice, TextureID::ButtonNormalIdle, L"UI/Theme_1/button_normal_idle.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonNormalMouseOver, L"UI/Theme_1/button_normal_mouseover.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonSmallIdle, L"UI/Theme_1/button_small_idle.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonSmallMouseOver, L"UI/Theme_1/button_small_mouseover.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonHomeIdle, L"UI/Theme_1/button_home_idle.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonHomeMouseOver, L"UI/Theme_1/button_home_mouseover.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonSwitchCameraIdle, L"UI/Theme_1/button_switchcamera_idle.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonSwitchCameraMouseOver, L"UI/Theme_1/button_switchcamera_mouseover.dds");
		texturePool.Load(d3dDevice, TextureID::Radio_On, L"UI/Theme_1/radio_on.dds");
		texturePool.Load(d3dDevice, TextureID::Radio_Off, L"UI/Theme_1/radio_off.dds");
		texturePool.Load(d3dDevice, TextureID::Checkbox_On, L"UI/Theme_1/checkbox_on.dds");
		texturePool.Load(d3dDevice, TextureID::Checkbox_Off, L"UI/Theme_1/checkbox_off.dds");
		texturePool.Load(d3dDevice, TextureID::MenuPanel, L"UI/Theme_1/menu_panel.dds");
		texturePool.Load(d3dDevice, TextureID::PausedPanel, L"UI/Theme_1/paused_panel.dds");
		texturePool.Load(d3dDevice, TextureID::SettingsPanel, L"UI/Theme_1/settings_panel.dds");
		texturePool.Load(d3dDevice, TextureID::PlayerSelectPanel, L"UI/Theme_1/playerselect_panel.dds");
		texturePool.Load(d3dDevice, TextureID::TopPanel, L"UI/Theme_1/top_panel.dds");
		texturePool.Load(d3dDevice, TextureID::GameOverPanel, L"UI/Theme_1/gameover_panel.dds");
		texturePool.Load(d3dDevice, TextureID::Slider, L"UI/Theme_1/Slider.dds");
		texturePool.Load(d3dDevice, TextureID::Slider_Handle, L"UI/Theme_1/Slider_Handle.dds");		
		texturePool.Load(d3dDevice, TextureID::TurnPiece_1, L"UI/Theme_1/white_piece.dds");
		texturePool.Load(d3dDevice, TextureID::TurnPiece_2, L"UI/Theme_1/red_piece.dds");
		// Checkers Board (Debug Overlay)
		texturePool.Load(d3dDevice, TextureID::Square_12x12, L"CheckersBoard/Theme_1/square_12x12.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircle_12x12, L"CheckersBoard/Theme_1/square_whitecircle_12x12.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircle_12x12, L"CheckersBoard/Theme_1/square_redcircle_12x12.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircleKing_12x12, L"CheckersBoard/Theme_1/square_whitecircleking_12x12.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircleKing_12x12, L"CheckersBoard/Theme_1/square_redcircleking_12x12.dds");
		texturePool.Load(d3dDevice, TextureID::Square_Selected_4x4, L"CheckersBoard/Theme_1/square_selected_4x4.dds");
		// Checkers Board Textures (Custom Board Scene)
		texturePool.Load(d3dDevice, TextureID::Square_28x28, L"CheckersBoard/Theme_1/square_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_Border_28x28, L"CheckersBoard/Theme_1/square_border_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircle_28x28, L"CheckersBoard/Theme_1/square_whitecircle_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircle_28x28, L"CheckersBoard/Theme_1/square_redcircle_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircleKing_28x28, L"CheckersBoard/Theme_1/square_whitecircleking_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircleKing_28x28, L"CheckersBoard/Theme_1/square_redcircleking_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_56x56, L"CheckersBoard/Theme_1/square_56x56.dds");
		texturePool.Load(d3dDevice, TextureID::Square_Border_56x56, L"CheckersBoard/Theme_1/square_border_56x56.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircle_56x56, L"CheckersBoard/Theme_1/square_whitecircle_56x56.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircle_56x56, L"CheckersBoard/Theme_1/square_redcircle_56x56.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircleKing_56x56, L"CheckersBoard/Theme_1/square_whitecircleking_56x56.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircleKing_56x56, L"CheckersBoard/Theme_1/square_redcircleking_56x56.dds");
		// Model Textures
		texturePool.Load(d3dDevice, TextureID::Model_CheckersBoard, L"Models/Theme_1/checkers_board.dds");
		texturePool.Load(d3dDevice, TextureID::Model_CheckersPiece_1, L"Models/Theme_1/checkers_piece_1.dds");
		texturePool.Load(d3dDevice, TextureID::Model_CheckersPiece_2, L"Models/Theme_1/checkers_piece_2.dds");
	}
	else if (theme == GameTheme::Theme2)
	{
		// Backgrounds
		texturePool.Load(d3dDevice, TextureID::Background, L"Backgrounds/background_theme2.dds");
		// UI Textures
		texturePool.Load(d3dDevice, TextureID::ButtonNormalIdle, L"UI/Theme_2/button_normal_idle.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonNormalMouseOver, L"UI/Theme_2/button_normal_mouseover.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonSmallIdle, L"UI/Theme_2/button_small_idle.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonSmallMouseOver, L"UI/Theme_2/button_small_mouseover.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonHomeIdle, L"UI/Theme_2/button_home_idle.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonHomeMouseOver, L"UI/Theme_2/button_home_mouseover.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonSwitchCameraIdle, L"UI/Theme_2/button_switchcamera_idle.dds");
		texturePool.Load(d3dDevice, TextureID::ButtonSwitchCameraMouseOver, L"UI/Theme_2/button_switchcamera_mouseover.dds");
		texturePool.Load(d3dDevice, TextureID::Radio_On, L"UI/Theme_2/radio_on.dds");
		texturePool.Load(d3dDevice, TextureID::Radio_Off, L"UI/Theme_2/radio_off.dds");
		texturePool.Load(d3dDevice, TextureID::Checkbox_On, L"UI/Theme_2/checkbox_on.dds");
		texturePool.Load(d3dDevice, TextureID::Checkbox_Off, L"UI/Theme_2/checkbox_off.dds");
		texturePool.Load(d3dDevice, TextureID::MenuPanel, L"UI/Theme_2/menu_panel.dds");
		texturePool.Load(d3dDevice, TextureID::PausedPanel, L"UI/Theme_2/paused_panel.dds");
		texturePool.Load(d3dDevice, TextureID::SettingsPanel, L"UI/Theme_2/settings_panel.dds");
		texturePool.Load(d3dDevice, TextureID::PlayerSelectPanel, L"UI/Theme_2/playerselect_panel.dds");
		texturePool.Load(d3dDevice, TextureID::TopPanel, L"UI/Theme_2/top_panel.dds");
		texturePool.Load(d3dDevice, TextureID::GameOverPanel, L"UI/Theme_2/gameover_panel.dds");
		texturePool.Load(d3dDevice, TextureID::Slider, L"UI/Theme_2/Slider.dds");
		texturePool.Load(d3dDevice, TextureID::Slider_Handle, L"UI/Theme_2/Slider_Handle.dds");
		texturePool.Load(d3dDevice, TextureID::TurnPiece_1, L"UI/Theme_2/gold_piece.dds");
		texturePool.Load(d3dDevice, TextureID::TurnPiece_2, L"UI/Theme_2/green_piece.dds");
		// Checkers Board (Debug Overlay)
		texturePool.Load(d3dDevice, TextureID::Square_12x12, L"CheckersBoard/Theme_2/square_12x12.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircle_12x12, L"CheckersBoard/Theme_2/square_goldcircle_12x12.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircle_12x12, L"CheckersBoard/Theme_2/square_greencircle_12x12.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircleKing_12x12, L"CheckersBoard/Theme_2/square_goldcircleking_12x12.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircleKing_12x12, L"CheckersBoard/Theme_2/square_greencircleking_12x12.dds");
		texturePool.Load(d3dDevice, TextureID::Square_Selected_4x4, L"CheckersBoard/Theme_2/square_selected_4x4.dds");
		// Checkers Board Textures (Custom Board Scene)
		texturePool.Load(d3dDevice, TextureID::Square_28x28, L"CheckersBoard/Theme_2/square_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_Border_28x28, L"CheckersBoard/Theme_2/square_border_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircle_28x28, L"CheckersBoard/Theme_2/square_goldcircle_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircle_28x28, L"CheckersBoard/Theme_2/square_greencircle_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircleKing_28x28, L"CheckersBoard/Theme_2/square_goldcircleking_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircleKing_28x28, L"CheckersBoard/Theme_2/square_greencircleking_28x28.dds");
		texturePool.Load(d3dDevice, TextureID::Square_56x56, L"CheckersBoard/Theme_2/square_56x56.dds");
		texturePool.Load(d3dDevice, TextureID::Square_Border_56x56, L"CheckersBoard/Theme_2/square_border_56x56.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircle_56x56, L"CheckersBoard/Theme_2/square_goldcircle_56x56.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircle_56x56, L"CheckersBoard/Theme_2/square_greencircle_56x56.dds");
		texturePool.Load(d3dDevice, TextureID::Square_WhiteCircleKing_56x56, L"CheckersBoard/Theme_2/square_goldcircleking_56x56.dds");
		texturePool.Load(d3dDevice, TextureID::Square_RedCircleKing_56x56, L"CheckersBoard/Theme_2/square_greencircleking_56x56.dds");
		// Model Textures
		texturePool.Load(d3dDevice, TextureID::Model_CheckersBoard, L"Models/Theme_2/checkers_board.dds");
		texturePool.Load(d3dDevice, TextureID::Model_CheckersPiece_1, L"Models/Theme_2/checkers_piece_1.dds");
		texturePool.Load(d3dDevice, TextureID::Model_CheckersPiece_2, L"Models/Theme_2/checkers_piece_2.dds");
	}
}

void ResourceMapper::MapMeshes(GameTheme theme)
{
	MeshPool& meshPool = MeshPool::Instance();
	TexturePool& texturePool = TexturePool::Instance();
	ID3D11Device* d3dDevice = m_d3dDevice.Get();

	

	meshPool.Load(d3dDevice, MeshID::SquareSelection, L"Theme_1/square_selection.obj", texturePool.GetTexture(TextureID::Model_TileSelection));

	if (theme == GameTheme::Theme1)
	{
		meshPool.Load(d3dDevice, MeshID::CheckersBoard, L"Theme_1/checkers_board.obj", texturePool.GetTexture(TextureID::Model_CheckersBoard));
		meshPool.Load(d3dDevice, MeshID::CheckersPiece1, L"Theme_1/checkers_piece.obj", texturePool.GetTexture(TextureID::Model_CheckersPiece_1));
		meshPool.Load(d3dDevice, MeshID::CheckersPiece2, L"Theme_1/checkers_piece.obj", texturePool.GetTexture(TextureID::Model_CheckersPiece_2));
		meshPool.Load(d3dDevice, MeshID::CheckersPieceKing1, L"Theme_1/checkers_piece_king.obj", texturePool.GetTexture(TextureID::Model_CheckersPiece_1));
		meshPool.Load(d3dDevice, MeshID::CheckersPieceKing2, L"Theme_1/checkers_piece_king.obj", texturePool.GetTexture(TextureID::Model_CheckersPiece_2));
	}
	else if (theme == GameTheme::Theme2)
	{
		meshPool.Load(d3dDevice, MeshID::CheckersBoard, L"Theme_1/checkers_board.obj", texturePool.GetTexture(TextureID::Model_CheckersBoard));
		meshPool.Load(d3dDevice, MeshID::CheckersPiece1, L"Theme_1/checkers_piece.obj", texturePool.GetTexture(TextureID::Model_CheckersPiece_1));
		meshPool.Load(d3dDevice, MeshID::CheckersPiece2, L"Theme_1/checkers_piece.obj", texturePool.GetTexture(TextureID::Model_CheckersPiece_2));
		meshPool.Load(d3dDevice, MeshID::CheckersPieceKing1, L"Theme_1/checkers_piece_king.obj", texturePool.GetTexture(TextureID::Model_CheckersPiece_1));
		meshPool.Load(d3dDevice, MeshID::CheckersPieceKing2, L"Theme_1/checkers_piece_king.obj", texturePool.GetTexture(TextureID::Model_CheckersPiece_2));
	}
}