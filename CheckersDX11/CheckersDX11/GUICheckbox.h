#pragma once

#include <string>
#include <dwrite.h>

#include "Quanta/QSprite.h"
#include "Quanta/QRenderer.h"
#include "Quanta/QEvent.h"

// Forward declarations
class GUICanvas;

// The GUICheckbox is an interactive element controlled by the GUICanvas. IT
// has two states, checked and unchecked.
class GUICheckbox
{
public:
	// Construct a GUICheckbox.
	// parentCanvas: The canvas parent to this checkbox.
	GUICheckbox(GUICanvas* parentCanvas);

	// Initialise the checkbox with the two textures and text.
	// Note that Init must be called after adding a checkbox to the canvas.
	void Init(
		Quanta::QTexture* texChecked,
		Quanta::QTexture* texUnchecked,
		const std::wstring& text,
		const std::wstring& fontFamily,
		float fontSize);

	// Set the callback that will be invoked when the user checks the checkbox.
	void SetCheckedCallback(std::function<void()> callback);

	// Set the callback that will be invoked when the user unchecks the checkbox.
	void SetUncheckedCallback(std::function<void()> callback);

	// Modify checkbox properties
	void SetPosition(int x, int y);	
	void SetHorizontalPadding(float padding);	
	void SetTextColour(UINT32 colour);	
	const DirectX::XMINT2& GetPosition() const;

	// Render the checkbox to the screen. Note this is managed by 
	// the parent canvas.
	void Render(Quanta::QRenderer& renderer);

	// Process windows events. Note this is managed by the parent canvas.
	void ProcessEvent(const Quanta::QEvent& e);

	// Set the checked state of the checkbox.
	void SetChecked(bool checked);

	// Get the checked state of the checkbox.
	bool IsChecked() const;

	// Determines whether the checkbox will be displayed on the screen.
	// Note that when visibility is set to false the checkbox will still be
	// active.
	void SetVisibility(bool visible);

	// When the checkbox is not active, all user interaction is ignored.
	void SetActive(bool active);

private:
	// Toggle the checked state of the checkbox.
	void ToggleChecked();
	// Invoked when the checked state of the checkbox changes.
	void OnCheckedChanged();

	// The parent canvas that controls this checkbox.
	GUICanvas* m_parentCanvas;

	// The text layout used for the text displayed beside the checkbox.
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_textLayout;

	// The sprite which represents the drawable checkbox.
	Quanta::QSprite m_sprCheckbox;

	// The two textures that are used for checked and unchecked states.
	Quanta::QTexture* m_texChecked;
	Quanta::QTexture* m_texUnchecked;

	// The callback function which is invoked when the checkbox goes
	// into a checked state.
	std::function<void()> m_checkedCallback;
	// The callback function which is invoked when the checkbox goes
	// into an unchecked state.
	std::function<void()> m_uncheckedCallback;

	// The text display beside the checkbox.
	std::wstring m_text;

	// Flag to track the checked state of the checkbox.
	bool m_isChecked;
	// Pixel space between the checkbox and the text.
	float m_horizontalPadding;
	// The text colour of the text beside the checkbox.
	UINT32 m_textColour;

	// Flag to determine if the checkbox is drawn to the screen.
	bool m_isVisible;
	// Flag to determine if the checkbox responds to user interaction.
	bool m_isActive;
};