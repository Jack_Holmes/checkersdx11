#pragma once

#include <DirectXCollision.h>

#include "Quanta/QRenderer.h"
#include "Quanta/QMesh.h"

#include "CheckersTileType.h"

// Represent the type of selection
enum class SelectionType
{
	None,
	Source,     // A selected tile (piece that the player wants to move)
	Destination // A destination tile (a possible location that selected piece can move to)
};

// Forward declarations
class CheckersBoardSelectionHandler;

// The CheckersBoardTile represents the selection state of a tile on the board. 
// It is managed by the CheckersBoardSelectionHandler and provides mouse picking
// checking from screen space to world space. Its also has a mesh that is rendered
// based on its selection type.
class CheckersBoardTile
{
public:
	// Construct a CheckersBoardTile.
	// selectionHandler: The CheckersBoardSelectionHandler which is managing this CheckersBoardTile.
	CheckersBoardTile(int index, CheckersBoardSelectionHandler* selectionHandler);

	// Update the animations of the renderable.
	void Update(float deltaTime) const;

	// Set the selection type of the checkers board tile.
	void SetSelected(SelectionType selectionType);

	// Set the type of tile it represents on the checkers board.
	void SetCheckersTileType(CheckersTileType type);

	// Get the index of the checkers board tile.
	int GetIndex() const;

	// Get the type of tile the selection is representing.
	CheckersTileType GetCheckersTileType() const;

	// Get the current state of selection.
	SelectionType GetSelectionType() const;

	// Render the selection mesh.
	void Render(Quanta::QRenderer& renderer);

	// Calcualte the world position of the selection based on its index.
	void RecalculatePosition();

	// Get the bounding box representing the collision volume
	// of the tile in world space.
	const DirectX::BoundingBox& GetBoundingBox() const;

private:
	// The index of the selection tile matches the index of the checkers board tile.
	int m_index;
	// Handle the CheckersBoardSelectionHandler which manages this tile.
	CheckersBoardSelectionHandler* m_selectionHandler;
	// The type of tile on the checkers board at its index.
	CheckersTileType m_checkersTileType;
	// The current state of selection.
	SelectionType m_selectionType;
	// The mesh which is drawn when the tile type is marked as a
	// destination for selection.
	Quanta::QMesh* m_selectionMesh;
	// The collision volume for mouse picking.
	DirectX::BoundingBox m_boundingBox;
};