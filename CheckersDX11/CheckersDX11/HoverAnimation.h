#pragma once

// The HoverAnimation provides a simple hover effect.  
class HoverAnimation
{
public:
	// Construct a HoverAnimation.
	// startY: The start y position.
	// minY: The minium y position when the direction changes.
	// maxY: The maximum y position when the direction changes.
	// speed: The rate at whch the y value changes.
	HoverAnimation(float startY, float minY, float maxY, float speed);

	// Update the y value based on the speed.
	void Update(float deltaTime);

	// Get the current y value.
	float GetValue();

private:
	// The current y (hover value)
	float m_yValue;
	// Controls the direction the hover is going.
	int m_direction;
	// Constraints that change direction.
	const float m_minY;
	const float m_maxY;
	// Speed of hover.
	const float m_speed;	
};