#pragma once

#include <unordered_map>
#include <memory>
#include <string>
#include <d3d11.h>
#include <wrl/client.h>

#include "Quanta/QTexture.h"
#include "TextureIDs.h"

// Map that holds the ids and textures managed by the texture pool.
using TextureContainer = std::unordered_map<int, std::unique_ptr<Quanta::QTexture>>;

// The TexturePool singleton resource managers texture assets.
class TexturePool
{
public:
	// Retrieve the texture pool static instance.
	static TexturePool& Instance()
	{
		static TexturePool instance;
		return instance;
	}

	// Load a .dds texture file into a QTexture.
	Quanta::QTexture* Load(ID3D11Device* d3dDevice, int id, const std::wstring& ddsFile) const;

	// Clear all the textures
	void Clear();

	// Retreive a pointer to a texture stored in the mesh pool.
	static Quanta::QTexture* GetTexture(int id);

	// Prevent copying the texture pool.
	TexturePool(const TexturePool&) = delete;
	void operator=(const TexturePool&) = delete;

private:
	TexturePool() {};

	// Add id texture pair to the textures container.
	static void Add(int id, std::unique_ptr<Quanta::QTexture> texture);

	// Container holding all the textures.
	static TextureContainer s_textures;
};