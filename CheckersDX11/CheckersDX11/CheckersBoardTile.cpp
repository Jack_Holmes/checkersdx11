#include "CheckersBoardTile.h"
#include "Quanta/QRay.h"
#include "CheckersBoardSelectionHandler.h"
#include "MeshPool.h"

using namespace Quanta;
using namespace DirectX;

CheckersBoardTile::CheckersBoardTile(int index, CheckersBoardSelectionHandler* selectionHandler)
	: m_index(index)
	, m_selectionHandler(selectionHandler)
	, m_checkersTileType(CheckersTileType::White)
	, m_selectionType(SelectionType::None)
{
	m_selectionMesh = MeshPool::Instance().GetMesh(MeshID::SquareSelection);

	RecalculatePosition();
}

void CheckersBoardTile::Update(float deltaTime) const
{
	// Rotate selection
	float yRot = 1.0f * deltaTime;
	m_selectionMesh->GetTransform().Rotate({ 0, XMConvertToRadians(yRot), 0.0f });

	// Apply pulse effect to selection
	static float scale = 1;
	static int pulseDir = 1;
	float pulseSpeed = 0.03f * deltaTime;
	if (scale > 1.5f || scale < 1)
	{
		pulseDir *= -1;
	}
	scale += pulseSpeed * pulseDir;	
	m_selectionMesh->GetTransform().SetScale({ scale, 1, scale });
}

void CheckersBoardTile::SetSelected(SelectionType selectionType)
{
	m_selectionType = selectionType;
}

void CheckersBoardTile::SetCheckersTileType(CheckersTileType type)
{
	m_checkersTileType = type;
}

int CheckersBoardTile::GetIndex() const
{
	return m_index;
}

CheckersTileType CheckersBoardTile::GetCheckersTileType() const
{
	return m_checkersTileType;
}

SelectionType CheckersBoardTile::GetSelectionType() const
{
	return m_selectionType;
}

void CheckersBoardTile::Render(QRenderer& renderer)
{
	if (m_selectionType == SelectionType::Destination)
	{
		RecalculatePosition();
		renderer.DrawMesh(*m_selectionMesh);
	}
}

void CheckersBoardTile::RecalculatePosition()
{
	int row = m_index / 8;
	int col = m_index % 8;
	float x = -7.f + col * 2.0f;
	float y = 0.4f;
	float z = 7.f - row * 2.0f;

	m_selectionMesh->GetTransform().SetPosition({ x, y, z });
	m_boundingBox.Center = { x, y - 0.05f, z };
	m_boundingBox.Extents = { 1.0f, 0.05f, 1.0f };
}

const DirectX::BoundingBox& CheckersBoardTile::GetBoundingBox() const
{
	return m_boundingBox;
}