#include <fstream>
#include "DebugLogFile.h"

void DebugLogFile::LogCheckersBoard(CheckersBoard* board, int turn, float p1ElapsedTime, float p2ElapsedTime)
{
	if (!board)
		return;

	// Map the data to a DebugState object.
	DebugState state;
	state.turn = turn;
	state.player1NumPieces = board->p1NumPieces();
	state.player2NumPieces = board->p2NumPieces();
	state.player1NumMen = board->p1NumMen();
	state.player2NumMen = board->p2NumMen();
	state.player1NumKings = board->p1NumKings();
	state.player2NumKings = board->p2NumKings();
	state.player1ElapsedTime = p1ElapsedTime;
	state.player2ElapsedTime = p2ElapsedTime;
	char boardState[9 * 8 + 1]; // CheckersBoard::toString req.
	board->toString(boardState);
	state.state = boardState;

	// Add the string representation of the debug state to the logs.
	std::string strDebugState = DebugStateToString(state);
	m_debugStates.push_back(strDebugState);
}

void DebugLogFile::SaveToFile(const std::string& filename)
{
	// Save the logs to a file.
	std::ofstream ofs(filename);
	if (!ofs.good())
		return;

	for (auto& debugState : m_debugStates)
	{
		ofs << debugState;
	}

	ofs.close();
}

void DebugLogFile::Clear()
{
	m_debugStates.clear();
}

std::string DebugLogFile::DebugStateToString(const DebugState& state) const
{
	std::string str;
	str = "Turn: " + std::to_string(state.turn) + "\n";
	str += "Player 1:\n\tPieces = " 
		+ std::to_string(state.player1NumPieces) 
		+ " (" + std::to_string(state.player1NumMen) + " men), (" 
		+ std::to_string(state.player1NumKings) + " kings)\n"
		+ "\tElapsed Time = " + std::to_string(state.player1ElapsedTime) + "s\n";
	str += "Player 2:\n\tPieces = "
		+ std::to_string(state.player2NumPieces)
		+ " (" + std::to_string(state.player2NumMen) + " men), ("
		+ std::to_string(state.player2NumKings) + " kings)\n"
		+ "\tElapsed Time = " + std::to_string(state.player2ElapsedTime) + "s\n";
	str += state.state + "\n";
	str += "---------------------------------------------------------------\n";

	return str;
}
