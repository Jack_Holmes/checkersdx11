#include "GUIRadioButtonGroup.h"
#include "GUICanvas.h"

using namespace Quanta;

GUIRadioButtonGroup::GUIRadioButtonGroup(GUICanvas* parentCanvas)
	: m_parentCanvas(parentCanvas)
	, m_texChecked(nullptr)
	, m_texUnchecked(nullptr)
	, m_xPos(0)
	, m_yPos(0)
	, m_verticalSpacing(0)
	, m_isVisible(true)
	, m_isActive(true)
{
}

void GUIRadioButtonGroup::Init(Quanta::QTexture* texChecked, Quanta::QTexture* texUnchecked)
{
	m_texChecked = texChecked;
	m_texUnchecked = texUnchecked;
}

GUIRadioButton* GUIRadioButtonGroup::AddRadioButton()
{
	auto radioButton = std::make_unique<GUIRadioButton>(this, m_parentCanvas);
	auto& sprite = radioButton->GetSprite();

	sprite.SetTexture(m_texUnchecked);

	int height = sprite.GetSize().y;

	sprite.SetPosition({ m_xPos, m_yPos + ((int)m_radioButtons.size() * (height + m_verticalSpacing))});
	if (m_radioButtons.empty())
	{
		radioButton->SetChecked(true);
		sprite.SetTexture(m_texChecked);
	}

	m_radioButtons.push_back(std::move(radioButton));

	return m_radioButtons.back().get();
}

void GUIRadioButtonGroup::Render(QRenderer& renderer)
{
	if (!m_isVisible)
	{
		return;
	}

	for (auto& radioButton : m_radioButtons)
	{
		radioButton->Render(renderer);
	}
}

void GUIRadioButtonGroup::SetPosition(int x, int y)
{
	m_xPos = x;
	m_yPos = y;

	for (int i = 0; i < (int)m_radioButtons.size(); ++i)
	{
		auto& sprite = m_radioButtons[i]->GetSprite();
		int height = sprite.GetSize().y;
		sprite.SetPosition({ m_xPos, m_yPos + i * (height + m_verticalSpacing) });
	}
}

void GUIRadioButtonGroup::SetVerticalSpacing(int spacing)
{
	m_verticalSpacing = spacing;
	SetPosition(m_xPos, m_yPos);
}

void GUIRadioButtonGroup::ProcessEvent(const QEvent& e)
{
	if (!m_isActive)
	{
		return;
	}

	if (e.type == QEvent::Type::MouseButtonPressed
		&& e.mouse.button == QMouse::Button::Left)
	{
		for (auto& radioButton : m_radioButtons)
		{
			if (m_parentCanvas->MouseIntersects(radioButton->GetSprite()))
			{
				SetChecked(*radioButton);
				break;
			}
		}
	}
}

void GUIRadioButtonGroup::SetVisibility(bool visible)
{
	m_isVisible = visible;
}

void GUIRadioButtonGroup::SetActive(bool active)
{
	m_isActive = active;
}

void GUIRadioButtonGroup::SetChecked(GUIRadioButton& radioButton)
{
	for (auto& rb : m_radioButtons)
	{
		rb->GetSprite().SetTexture(m_texUnchecked);
		rb->SetChecked(false);
	}

	radioButton.GetSprite().SetTexture(m_texChecked);
	radioButton.SetChecked(true);	
}