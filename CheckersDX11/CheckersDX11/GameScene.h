#pragma once

// DriectX includes
#include <DirectXMath.h>
#include <DirectXCollision.h>
#include <dwrite.h>

// Quanta includes
#include "Quanta/QCamera.h"
#include "Quanta/QMesh.h"
#include "Quanta/QSprite.h"
#include "Quanta/QTexture.h"
#include "FW1FontWrapper_1_1/FW1FontWrapper.h"

// Checkers agents includes
#include "Agents/Common/checkersboard.cpp"
#include "Agents/Common/bfs.hpp"
#include "Agents/Sequential/CheckersSearch.hpp"

#include "Scene.h"
#include "PlayerType.h"
#include "CheckersBoardSelectionHandler.h"
#include "CheckersPlayer.h"
#include "HoverAnimation.h"

// Debugging includes
#include "DebugLogFile.h"
#include "DebugOverlay.h"
#include <Quanta/QClock.h>

// The GameScene is the main scene when the user is playing
// a game of checkers.
class GameScene : public Scene
{
public:
	// Construct a GameScene.
	// sceneManager: The scene manager that this scene has been added to.
	GameScene(SceneManager* sceneManager);
	
	// Overrides
	virtual void ProcessEvent(Quanta::QEvent& e);
	virtual void Update(float deltaTime);
	virtual void Render();
	virtual void OnEnter();
	virtual void OnGameThemeChanged(GameTheme theme);

	// Set up a game of checkers. p1 and p2 are the type
	// of players playing the game. Note this resets the game.
	void SetUp(CheckersPlayer p1, CheckersPlayer p2, bool showDebugLayer);

	// Set the checkers board that will be used by the game. Note this
	// only needs to be called if a custom start board is required. Otherwise
	// the default checkers board will be used.
	void SetCheckersBoard(const CheckersBoard& checkersBoard);

	// Reset the game.
	void ResetGame();

private:
	// Initialisation
	void InitLights() const;
	void InitResources();

	// Checkers board query and actions
	float MakeMove(PlayerType playerType);
	bool CanMakeMove() const;
	PlayerType GetCurrentPlayerType() const;
	PlayerType GetOpponentPlayerType() const;
	int GetNumberOfOpponentPlayerPieces() const;
	void SwapPlayers();

	// Render
	void RenderBoard(CheckersBoard& board);
	void RenderCapturedCheckerPieces() const;
	void RenderTopPanel();
	void RenderPauseMenu();
	void RenderGameOverPanel();

	void RecalculateSprites();	

	// Callbacks
	void OnHumanPlayerMakeMove();
	void OnSavePressed() const;
	void OnSettingsPressed() const;
	void OnResumedPressed();
	void OnExitPressed() const;
	void OnGameOver();
	void OnSwitchCameraView();

	void SetPaused(bool paused);
	void SetGameOverText(const std::wstring& text);

	Quanta::QCamera m_camera;

	// Meshes
	Quanta::QMesh* m_checkersBoardMesh;
	Quanta::QMesh* m_checkersPieceMesh1;
	Quanta::QMesh* m_checkersPieceMesh2;
	Quanta::QMesh* m_checkersPieceKing1;
	Quanta::QMesh* m_checkersPieceKing2;

	// Sprites
	Quanta::QSprite m_sprBackground;
	Quanta::QSprite m_sprPausedBackground;
	
	Quanta::QSprite m_sprTopPanel;
	Quanta::QSprite m_sprPauseMenu;
	Quanta::QSprite m_sprTurnPiece;
	Quanta::QSprite m_sprGameOverPanel;

	// Checkers board specific
	CheckersBoard  m_gpuOtherCheckersBoard;
	CheckersBoard  m_checkersBoard;
	CheckersSearch m_checkersSearch;
	BFS            m_bfs;
	player         m_isPlayer1;
	CheckersPlayer m_player1;
	CheckersPlayer m_player2;

	// Handles human interaction with the board
	CheckersBoardSelectionHandler m_selectionHandler;

	// Text layouts
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_pauseMenuTitleTextLayout;
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_gameOverTextLayout;

	// Primiary text colour used by the GUI Elements.
	UINT32 m_primaryTextColour;

	// GUI elements
	GUIButton* m_saveButton;
	GUIButton* m_settingsButton;
	GUIButton* m_resumeButton;
	GUIButton* m_exitButton;
	GUIButton* m_playAgainButton;
	GUIButton* m_gameOverExitButton;
	GUIButton* m_cameraViewButton;

	// Tracks whether the game is paused or not
	bool m_isPaused;
	// Tracks the number of turns
	int m_turnCount;
	// Sets the number of turns allowed for an entire game
	const int m_turnsAllowed;
	// The search depth used by the checkers search
	int m_depth;
	// Keeps track of whether the latest checkers board layout has
	// been draw to the screen. This prevents skipping layouts in 
	// cases where updating is being called more times than render.
	bool m_latestBoardRendered;
	// Tracks if the game is in a game over state
	bool m_gameOver;
	// Used to toggle the visibility of the debug layer
	bool m_showDebugLayer;
	// Prevents continuation of an AI making a move
	// when waiting for a human player to make a move.
	bool m_waitingForPlayerToMakeMove;

	// Toggles the view the camera is in (perspective vs top-down)
	bool m_perspView;

	// Keeps the elapsed time a human player takes on their move.
	Quanta::QClock m_humanTurnClock;

	// A hover animation that is applied to the player selected peice
	HoverAnimation m_playerSelectionHover;

	// Debugging facilities
	DebugLogFile m_debugLogFile;
	bool         m_savedDebugLog;
	DebugOverlay m_debugOverlay;

	// Keeps hold of the board states considered by the CPU.
	NextStates m_nextStates;
};
