#pragma once

#include <memory>
#include <vector>
#include <d3d11.h>

#include "Quanta/QEvent.h"
#include "Quanta/QSprite.h"
#include "FW1FontWrapper_1_1/FW1FontWrapper.h"

// GUI Elements
#include "GUIButton.h"
#include "GUIRadioButtonGroup.h"
#include "GUICheckbox.h"
#include "GUISlider.h"

// The GUICanvas controls the event processing and rending of all GUI elements.
class GUICanvas
{
public:
	// Construct a GUICanvas.
	// window: The application window.
	// renderer: The application renderer used to draw the elements.
	// d3dDevice: The Direct3D 11 device.
	explicit GUICanvas(
		Quanta::QWindow* window,
		Quanta::QRenderer* renderer,
		ID3D11Device* d3dDevice);

	// Forwards windows events to the gui elements.
	void ProcessEvent(const Quanta::QEvent& e);

	// Renders all the gui elements. Note this must be
	// called in the sprite pass.
	void Render();

	// Add a button to the canvas. A pointer to the button is returned.
	GUIButton* AddButton();

	// Add a radio button group to the canvas. A pointer to the radio button group is returned.
	GUIRadioButtonGroup* AddRadioButtonGroup();

	// Add a checkbox to the canvas. A pointer to the checkbox is returned.
	GUICheckbox* AddCheckbox();

	// Add a slider to the canvas. A pointer to the slider is returned.
	GUISlider* AddSlider();

	// Check if the mouse intersects the specified sprite. Note this is relative
	// to the window that was passed when the canvas was constructed.
	bool MouseIntersects(const Quanta::QSprite& sprite) const;

	// Draw a string to the screen.
	void DrawString(const std::wstring& text, float fontSize, float x, float y, UINT32 colour) const;

	// Draw a string to the screen using the specified text layout.
	void DrawString(IDWriteTextLayout* textLayout, float originX, float originY, UINT32 colour) const;

	// Get the DWriteFactory used by the canvas to draw the strings. This is required when
	// creating text layouts.
	IDWriteFactory* GetDWriteFactory() const;

private:
	// The application window that elements are being rendered to.
	Quanta::QWindow* m_window;
	// The appliation renderer used to draw the elements.
	Quanta::QRenderer* m_renderer;

	// Direct3D 11 device context.
	Microsoft::WRL::ComPtr<ID3D11DeviceContext> m_d3dDeviceContext;

	// DWrite factory used for creating and drawing text.
	Microsoft::WRL::ComPtr<IDWriteFactory> m_dwriteFactory;
	// The FW1FontWrapper
	Microsoft::WRL::ComPtr<IFW1FontWrapper> m_fontWrapper;

	// List of buttons managed by the canvas.
	std::vector<std::unique_ptr<GUIButton>> m_buttons;
	// List of radio button groups managed by the canvas.
	std::vector<std::unique_ptr<GUIRadioButtonGroup>> m_radioButtonGroups;
	// List of checkboxes managed by the canvas.
	std::vector<std::unique_ptr<GUICheckbox>> m_checkboxes;
	// List of sliders managed by the canvas.
	std::vector<std::unique_ptr<GUISlider>> m_sliders;
};