#pragma once

#include <string>
#include <memory>

#include "GUICanvas.h"
#include "GameSettings.h"

// Forward declarations
namespace Quanta
{
	class QRenderer;
	class QWindow;
	class QEvent;
}

class SceneManager;

// The Scene class is managed by the SceneManager. Its main purpose
// is to isolate the event processing, updating and rendering during
// a point in the application. So that screens are only updated when they
// need to be.
class Scene
{
public:
	// Construct a scene.
	// name: The name of the scene. e.g. "menu"
	// sceneManager: The scene manager that managers this scene.
	Scene(const std::string& name, SceneManager* sceneManager);
	virtual ~Scene();

	// Process windows events.
	virtual void ProcessEvent(Quanta::QEvent& e);
	// Update game logic.
	virtual void Update(float deltaTime);
	// Update the game display.
	virtual void Render();

	// Get the name of the scene.
	const std::string& GetName() const;

	// Called once when the a user enters the scene.
	virtual void OnEnter();
	// Called once when the user exits a the scene.
	virtual void OnExit();
	// Called once when the game theme has changed.
	virtual void OnGameThemeChanged(GameTheme theme);

protected:
	// Holds a handle to the scene manager.
	SceneManager* m_sceneManager;
	// A GUI canvas that belongs to this scene.
	std::unique_ptr<GUICanvas> m_guiCanvas;

private:	
	// The name of the scene.
	std::string m_name;	
};