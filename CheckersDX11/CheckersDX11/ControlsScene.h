#pragma once

#include <dwrite.h>
#include <string>

#include "Quanta/QCamera.h"
#include "Quanta/QSprite.h"

#include "Scene.h"

class ControlsScene : public Scene
{
public:
	ControlsScene(SceneManager* sceneManager);

	// Scene overrides
	virtual void ProcessEvent(Quanta::QEvent& e);
	virtual void Update(float deltaTime);
	virtual void Render();
	virtual void OnEnter();
	virtual void OnGameThemeChanged(GameTheme theme);

private:
	// Initialise the GUI elements that are used in the scene.
	void InitResources();

	// Recalculate the sprite positions/sizes. This needs to be called
	// any time the screen size changes.
	void RecalculateSprites();

	// Callbacks.
	void OnHomePressed() const;

	// The scene camera.
	Quanta::QCamera m_camera;

	// Sprites.
	Quanta::QSprite m_sprBackground;
	Quanta::QSprite m_sprControls;

	// The text layout used for the title.
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_titleTextLayout;

	// Primiary text colour used by the GUI Elements.
	UINT32 m_primaryTextColour;

	// Buttons.
	GUIButton* m_homeButton;
	GUIButton* m_okButton;
};