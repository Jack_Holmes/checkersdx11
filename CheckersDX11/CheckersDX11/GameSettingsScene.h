#pragma once

#include <dwrite.h>

#include "Scene.h"
#include "GameSettings.h"

#include "Quanta/QSprite.h"
#include "Quanta/QTexture.h"
#include "Quanta/QCamera.h"

// The GameSettingsScene displays the current game setttings and allows the user
// to modify them.
class GameSettingsScene : public Scene
{
public:
	// Construct a GameSettingsScene.
	// sceneManager: The scene manager that this scene has been added to.
	GameSettingsScene(SceneManager* sceneManager);

	// Overrides.
	virtual void ProcessEvent(Quanta::QEvent& e);
	virtual void Update(float deltaTime);
	virtual void Render();
	virtual void OnEnter();
	virtual void OnGameThemeChanged(GameTheme theme);

private:
	// A proxy for the game settings that is set based on the user selection.
	// The proxy is only used to set the game settings once the user has applied
	// the changes.
	struct GameSettingsProxy
	{
		bool musicEnabled;
		bool sfxEnabled;
		bool VSyncEnabled;
		GameTheme gameTheme;
		AIDifficulty aiDifficulty;
	};

	// Initialise the game settings gui elements.
	void InitGUI();

	// Recalculate the sprite sizes/positions. This must be called when the
	// window size changes.
	void RecalculateSprites();

	// Callbacks.
	void OnOKPressed();
	void OnCancelPressed();

	// The scene camera.
	Quanta::QCamera m_camera;

	// Text layouts.
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_titleTextLayout;
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_videoSectionTitleTextLayout;
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_themeSubSectionTitleTextLayout;
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_audioSectionTitleTextLayout;
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_gameplaySectionTitleTextLayout;

	// Sprites.
	Quanta::QSprite m_sprBackground;
	Quanta::QSprite m_sprSettingsPanel;

	// The previous settings are used incase the user requests to cancel
	// the changes and so they can be revereted. The new settings are used
	// to set the game settings if they are applied.
	GameSettingsProxy m_previousSettings;
	GameSettingsProxy m_newSettings;

	// Primiary text colour used by the GUI Elements.
	UINT32 m_primaryTextColour;

	// GUI Elements.
	GUICheckbox* m_musicCheckbox;
	GUICheckbox* m_sfxCheckbox;	
	GUICheckbox* m_vSyncCheckbox;
	GUIRadioButtonGroup* m_themeRadioGroup;
	GUIRadioButton* m_theme1Radio;
	GUIRadioButton* m_theme2Radio;
	GUISlider* m_aiDifficultySlider;
	GUIButton* m_okButton;
	GUIButton* m_cancelButton;	
};