#pragma once

#include <dwrite.h>

#include "Quanta/QCamera.h"
#include "Scene.h"
#include "CheckersPlayer.h"
#include "Agents/Common/checkersboard.cpp"

// The GameSetupScene is displayed before a player enters the game scene. It
// gives them an opportunity to configure the game they are playing.
class GameSetupScene : public Scene
{
public:
	// Construct a GameSetupScene.
	// sceneManager: The scene manager that this scene has been added to.
	GameSetupScene(SceneManager* sceneManager);

	// Overrides.
	virtual void ProcessEvent(Quanta::QEvent& e);
	virtual void Update(float deltaTime);
	virtual void Render();
	virtual void OnEnter();
	virtual void OnGameThemeChanged(GameTheme theme);

private:
	// Initialise the GUI elements.
	void InitGUI();

	// Recalculate the sprite positions/sizes. This must be
	// called if the window size changes.
	void RecalculateSprites();

	// Callbaacks.
	void OnOKPressed() const;
	void OnLoadPressed();
	void OnHomePressed() const;

	// Change the text that is displayed based on the loaded checkers file.
	void SetLoadedGameText(const std::wstring& text);

	// Text layouts.
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_titleTextLayout;
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_playerSelect1TextLayout;
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_playerSelect2TextLayout;
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_loadedFileTextLayout;

	// The scene camera.
	Quanta::QCamera m_camera;

	// Sprites.
	Quanta::QSprite m_sprBackground;
	Quanta::QSprite m_sprPlayerSelectPanel1;
	Quanta::QSprite m_sprPlayerSelectPanel2;

	// The players that will be set and used for the game.
	CheckersPlayer m_player1;
	CheckersPlayer m_player2;

	// Primiary text colour used by the GUI Elements.
	UINT32 m_primaryTextColour;

	// GUI Elements.
	// Player 1 type radio group.
	GUIRadioButtonGroup* m_radioGroup1;
	GUIRadioButton* m_radio1Human;
	GUIRadioButton* m_radio1AI;
	// Player 2 type radio group.
	GUIRadioButtonGroup* m_radioGroup2;
	GUIRadioButton* m_radio2Human;
	GUIRadioButton* m_radio2AI;
	// Player 1 and 2 GPU acc. checkboxes
	GUICheckbox* m_checkbox1GPU;
	GUICheckbox* m_checkbox2GPU;
	// Buttons.
	GUIButton* m_okButton;
	GUIButton* m_loadButton;
	GUIButton* m_homeButton;

	// The checkers board that is passed to the game scene
	// if a custom board has been loaded.
	CheckersBoard m_checkersBoard;
	// Tracks whether we are using a custom board with the game.
	bool m_checkersBoardLoaded;
};