#pragma once

#include <vector>

#include "Quanta/QSprite.h"
#include "Quanta/QEvent.h"
#include "Quanta/QRenderer.h"
#include "GUIRadioButton.h"

// Forward declarations
class GUICanvas;
class QTexture;

// The GUIRadioButtonGroup controls a group of radio buttons added to a canvas.
class GUIRadioButtonGroup
{
public:
	// Construct a GUIRadioButtonGroup.
	// parentCanvas: The canvas parent to this radio button group.
	explicit GUIRadioButtonGroup(GUICanvas* parentCanvas);

	// Initialise the radio button group with the two textures that are used
	// for all the radio buttons that are added to the group for their checked and 
	// unchecked states. Note that Init must be called after adding a radio button
	// group to the canvas.
	void Init(Quanta::QTexture* texChecked, Quanta::QTexture* texUnchecked);

	// Add a radio button to this group. A pointer to the radio button is returned
	// so the user can control its properties /see GUIRadioButton.
	GUIRadioButton* AddRadioButton();	

	// Render all the radio buttons that are part of this group. Note this is
	// managed by the parent canvas.
	void Render(Quanta::QRenderer& renderer);

	// Modify the radio button group properties.
	void SetPosition(int x, int y);
	void SetVerticalSpacing(int spacing);

	// Process windows events. Note this is managed by the parent canvas.
	void ProcessEvent(const Quanta::QEvent& e);

	// Determines whether the radio buttons will be displayed on the screen.
	// Note that when visibility is set to false the radio buttons will still be
	// active.
	void SetVisibility(bool visible);

	// When the radio button group is not active, all user interaction is ignored.
	void SetActive(bool active);

	// Set the checked state of an individual radio button. This will uncheck
	// all other radio buttons part of this group.
	void SetChecked(GUIRadioButton& radioButton);

private:
	// The parent canvas that controls this radio button group.
	GUICanvas* m_parentCanvas;

	// The list of radio buttons that are part of this group.
	std::vector<std::unique_ptr<GUIRadioButton>> m_radioButtons;

	// The two textures that are used for checked and unchecked states.
	Quanta::QTexture* m_texChecked;
	Quanta::QTexture* m_texUnchecked;

	// The pixel x position of the radio button group which radio buttons
	// are relative to.
	int m_xPos;
	// The pixel y position of the radio button group which radio buttons
	// are relative to.
	int m_yPos;
	// The pixel vertical pixel spacing between each radio button in this
	// group.
	int m_verticalSpacing;

	// Flag to determine if the radio buttons are drawn to the screen.
	bool m_isVisible;
	// Flag to determine if the radio buttons group responds to user
	// interaction.
	bool m_isActive;
};