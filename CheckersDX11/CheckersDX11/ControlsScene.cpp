#include "ControlsScene.h"
#include "SceneManager.h"
#include "TexturePool.h"
#include "Quanta/QRenderer.h"
#include "GameSettings.h"

using namespace Quanta;
using Microsoft::WRL::ComPtr;

ControlsScene::ControlsScene(SceneManager* sceneManager)
	: Scene("controls", sceneManager)
	, m_camera({ 0, 0, 0 })
{
	InitResources();
}

void ControlsScene::ProcessEvent(Quanta::QEvent& e)
{
	m_guiCanvas->ProcessEvent(e);

	if (e.type == QEvent::Type::Resized)
	{
		QWindow& window = m_sceneManager->GetWindow();
		m_camera.SetAspectRatio(static_cast<float>(window.GetWidth()) / window.GetHeight());
		RecalculateSprites();
	}
}

void ControlsScene::Update(float deltaTime)
{
}

void ControlsScene::Render()
{
	QRenderer& renderer = m_sceneManager->GetRenderer();
	QWindow& window = m_sceneManager->GetWindow();

	renderer.BeginSpritePass(m_camera);
	renderer.DrawSprite(m_sprBackground);
	renderer.DrawSprite(m_sprControls);	
	renderer.EndSpritePass();

	m_guiCanvas->Render();
	m_guiCanvas->DrawString(m_titleTextLayout.Get(), float(window.GetWidth() / 2 - 500), 10.0f, m_primaryTextColour);
}

void ControlsScene::OnEnter()
{
	QWindow& window = m_sceneManager->GetWindow();
	m_camera.SetAspectRatio(static_cast<float>(window.GetWidth()) / window.GetHeight());
	RecalculateSprites();
}

void ControlsScene::OnGameThemeChanged(GameTheme theme)
{
	m_primaryTextColour = 0xff000000;

	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		m_primaryTextColour = 0xff27848c;
	}

	m_okButton->SetTextColour(m_primaryTextColour);
}

void ControlsScene::InitResources()
{
	TexturePool& texturePool = TexturePool::Instance();
	m_sprBackground.SetTexture(texturePool.GetTexture(TextureID::Background));	
	m_sprControls.SetTexture(texturePool.GetTexture(TextureID::Controls));
	
	m_primaryTextColour = 0xff000000;

	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		m_primaryTextColour = 0xff27848c;
	}

	// Fonts
	auto dwriteFactory = m_guiCanvas->GetDWriteFactory();

	std::wstring fontFamily = GameSettings::Instance().GetFontFamily();

	ComPtr<IDWriteTextFormat> titleTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		70.0f,
		L"en-gb",
		titleTextFormat.GetAddressOf());
	titleTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);

	auto titleText = L"Controls";
	dwriteFactory->CreateTextLayout(
		titleText,
		wcslen(titleText),
		titleTextFormat.Get(),
		1000.0f,
		400.0f,
		m_titleTextLayout.GetAddressOf());

	m_homeButton = m_guiCanvas->AddButton();
	m_homeButton->Init(texturePool.GetTexture(TextureID::ButtonHomeIdle), texturePool.GetTexture(TextureID::ButtonHomeMouseOver), L"", fontFamily, 0.0f);
	m_homeButton->SetCallback(std::bind(&ControlsScene::OnHomePressed, this));
	m_homeButton->SetTextColour(m_primaryTextColour);

	m_okButton = m_guiCanvas->AddButton();
	m_okButton->Init(texturePool.GetTexture(TextureID::ButtonNormalIdle), texturePool.GetTexture(TextureID::ButtonNormalMouseOver), L"OK", fontFamily, 54.0f);
	m_okButton->SetCallback(std::bind(&ControlsScene::OnHomePressed, this));
	m_okButton->SetTextColour(m_primaryTextColour);

	RecalculateSprites();
}

void ControlsScene::RecalculateSprites()
{
	QWindow& window = m_sceneManager->GetWindow();
	UINT windowWidth = window.GetWidth();
	UINT windowHeight = window.GetHeight();

	m_sprBackground.SetScale({ windowWidth / 512.0f, windowHeight / 512.0f });
	m_sprBackground.SetPosition({ int(windowWidth / 2), int(windowHeight / 2) });
	
	m_sprControls.SetPosition({ int(windowWidth / 2), int(windowHeight / 2) });

	m_homeButton->SetPosition(m_homeButton->GetSize().x / 2 + 10, m_homeButton->GetSize().y / 2 + 10);
	m_okButton->SetPosition(windowWidth - m_okButton->GetSize().x / 2 - 30, windowHeight - m_okButton->GetSize().y / 2 - 30);
}

void ControlsScene::OnHomePressed() const
{
	m_sceneManager->SetScene("menu");
}