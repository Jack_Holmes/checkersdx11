#pragma once

#include <string>

// The PlayerType represent a list of supported player
// types for the checkers game.
enum class PlayerType
{
	Human,
	CPU,
	CPU_BFS,
	GPU
};

// Converts a PlayerType to a string.
std::wstring PlayerTypeToWString(PlayerType playerType);