#include "GUIButton.h"
#include "Quanta/QTexture.h"
#include "GUICanvas.h"

using namespace Quanta;
using Microsoft::WRL::ComPtr;

GUIButton::GUIButton(GUICanvas* parentCanvas)
	: m_parentCanvas(parentCanvas)
	, m_texIdle(nullptr)
	, m_texMouseOver(nullptr)
	, m_buttonState(ButtonState::Idle)
	, m_textColour(0xff000000)
	, m_isVisible(true)
	, m_isActive(true)
{
}

void GUIButton::Init(
	Quanta::QTexture* idleTexture,
	Quanta::QTexture* mouseOverTexture,
	const std::wstring& text,
	const std::wstring& fontFamily,
	float fontSize)
{
	m_texIdle = idleTexture;
	m_texMouseOver = mouseOverTexture;
	m_sprButton.SetTexture(m_texIdle);

	auto dwriteFactory = m_parentCanvas->GetDWriteFactory();

	if (!text.empty() && fontSize > 0.0f)
	{
		ComPtr<IDWriteTextFormat> textFormat;
		dwriteFactory->CreateTextFormat(
			fontFamily.c_str(),
			nullptr,
			DWRITE_FONT_WEIGHT_REGULAR,
			DWRITE_FONT_STYLE_NORMAL,
			DWRITE_FONT_STRETCH_NORMAL,
			fontSize,
			L"en-gb",
			textFormat.GetAddressOf());

		textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
		textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);

		dwriteFactory->CreateTextLayout(
			text.c_str(),
			wcslen(text.c_str()),
			textFormat.Get(),
			static_cast<float>(m_sprButton.GetSize().x),
			static_cast<float>(m_sprButton.GetSize().y),
			m_textLayout.GetAddressOf());
	}
}

void GUIButton::SetCallback(std::function<void()> callback)
{
	m_callback = callback;
}

void GUIButton::ProcessEvent(const Quanta::QEvent& e)
{
	if (!m_isActive)
	{
		return;
	}

	if (e.type == QEvent::Type::MouseMoved)
	{
		ButtonState prevState = m_buttonState;

		if (m_parentCanvas->MouseIntersects(m_sprButton))
		{
			m_buttonState = ButtonState::MouseOver;
		}
		else
		{
			m_buttonState = ButtonState::Idle;
		}

		if (m_buttonState != prevState)
		{
			if (m_buttonState == ButtonState::Idle)
			{
				m_sprButton.SetTexture(m_texIdle);
			}
			else if (m_buttonState == ButtonState::MouseOver)
			{
				m_sprButton.SetTexture(m_texMouseOver);
			}
		}
	}
	else if (e.type == QEvent::Type::MouseButtonPressed)
	{
		if (e.mouse.button == QMouse::Button::Left
			&& m_buttonState == ButtonState::MouseOver)
		{
			if (m_callback)
			{
				m_callback();
			}
		}
	}
}

void GUIButton::SetVisibility(bool visible)
{
	m_isVisible = visible;
}

void GUIButton::SetActive(bool active)
{
	m_isActive = active;
}

void GUIButton::Render(Quanta::QRenderer& renderer)
{
	if (!m_isVisible)
	{
		return;
	}

	renderer.DrawSprite(m_sprButton);

	if (m_textLayout)
	{
		m_parentCanvas->DrawString(
			m_textLayout.Get(),
			float(m_sprButton.GetPosition().x) - (m_sprButton.GetSize().x / 2),
			float(m_sprButton.GetPosition().y) - (m_sprButton.GetSize().y / 2),
			m_textColour);
	}	
}

void GUIButton::SetPosition(int x, int y)
{
	m_sprButton.SetPosition({ x, y });
}

void GUIButton::SetColour(float r, float g, float b, float a)
{
	m_sprButton.SetColour({ r, g, b, a });
}

void GUIButton::SetTextColour(UINT32 colour)
{
	m_textColour = colour;
}

void GUIButton::SetScale(float x, float y)
{
	m_sprButton.SetScale({ x, y });
}

const DirectX::XMINT2& GUIButton::GetPosition() const
{
	return m_sprButton.GetPosition();
}

DirectX::XMUINT2 GUIButton::GetSize() const
{
	return m_sprButton.GetSize();
}