#pragma once

#include <vector>
#include <memory>
#include <string>

#include "Scene.h"
#include "irrKlang.h"
#include "GameSettings.h"

#include <wrl/client.h>

// Forward declarations
namespace irrklang { class ISoundEngine; }

class SceneManager
{
public:
	SceneManager(Quanta::QRenderer* renderer, Quanta::QWindow* window, Microsoft::WRL::ComPtr<ID3D11Device> device);
	~SceneManager();

	// Add a scene for the scene manager to update/render
	void AddScene(std::unique_ptr<Scene> scene);

	// Send windows events to the active scene.
	void ProcessEvent(Quanta::QEvent& e) const;
	// Update the active scene.
	void Update(float deltaTime) const;
	// Render the active scene.
	void Render() const;

	// Set the current scene that will be updated/rendered.
	void SetScene(const std::string& name);

	// If the user has visited a previous scene, you can call this
	// to return to it (the last visisted scene).
	void ReturnToPreviousScene();

	// Get a raw pointer to a scene. You can dynamic cast into
	// the concrete type.
	Scene* GetScene(const std::string& name);
	
	// Get the renderer used by the scene manager.
	Quanta::QRenderer& GetRenderer() const;

	// Get the widow used by the scene manager.
	Quanta::QWindow& GetWindow() const;

	// Get the sound engine used by the scene manager.
	irrklang::ISoundEngine& GetSoundEngine() const;

	// Get the direct3D 11 device used by the scene manager.
	Microsoft::WRL::ComPtr<ID3D11Device> GetDevice() const;

	// Notifys scenes the game theme has changed
	void OnGameThemeChanged(GameTheme theme) const;

private:
	// Holds all the scenes that are currently been managed.
	std::vector<std::unique_ptr<Scene>> m_scenes;
	// Handle to the active scene that is currently being updated/rendered.
	Scene* m_activeScene;

	Quanta::QRenderer*                   m_renderer;
	Quanta::QWindow*                     m_window;
	irrklang::ISoundEngine*              m_soundEngine;
	Microsoft::WRL::ComPtr<ID3D11Device> m_device;

	std::string m_previousSceneName;
};