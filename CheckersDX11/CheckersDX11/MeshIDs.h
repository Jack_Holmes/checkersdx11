#pragma once

// Mesh IDs to uniquely identify mesh assets
namespace MeshID
{
	enum MeshID
	{
		SquareSelection,
		CheckersBoard,
		CheckersPiece1,
		CheckersPiece2,
		CheckersPieceKing1,
		CheckersPieceKing2
	};
}