#include "DebugOverlay.h"
#include "TexturePool.h"
#include "GameSettings.h"
#include "Agents/Sequential/CheckersSearch.hpp"
#include <iomanip>

using namespace Quanta;
using Microsoft::WRL::ComPtr;

DebugOverlay::DebugOverlay(QWindow* window, GUICanvas* guiCanvas)
	: m_window(window)
	, m_guiCanvas(guiCanvas)
	, m_player1HiCount(0)
	, m_player1LoCount(0)
	, m_boardOffsetY(0)
	, m_boardOffsetX(0)
{
	// Set the textures that are used by the debug overlay.
	m_sprTextDebugPanel.SetTexture(TexturePool::Instance().GetTexture(TextureID::SemiTransparentSquare_4x4));
	m_sprSelected.SetTexture(TexturePool::Instance().GetTexture(TextureID::Square_Selected_4x4));

	RecalculatePositions();
	SetDebugText(L"[F1] - Toggle debug overlay");
	InitCheckersTiles();
}

void DebugOverlay::ProcessEvent(QEvent& e)
{
	if (e.type == QEvent::Type::Resized)
	{
		// Window size has changed, so recalculate debug element positions.
		RecalculatePositions();
	}
}

void DebugOverlay::Update(int turn, const CheckersBoard& selectedBoard, const NextStates& nextStates)
{
	m_selectedBoard = selectedBoard;
	m_nextStates = nextStates;

	// Display turn
	std::wstring debugText = L"[F1] - Toggle debug overlay\nTurn " + std::to_wstring(turn + 1) + L"\n\n";

	// Display timings
	debugText += L"TIMINGS\n";
	debugText += L"--------------------------------------------------------------------------------------------------------\n";
	debugText += L"Player 1 (" + m_strPlayer1Type + L"): " + std::to_wstring(m_player1->GetTotalElapsedTime()) + L" seconds\n";
	debugText += L"Player 2 (" + m_strPlayer2Type + L"): " + std::to_wstring(m_player2->GetTotalElapsedTime()) + L" seconds\n";
	
	// Display the amount of times player 1 was faster/slower than player 2
	if (m_player1->GetElapsedTimes().size() == m_player2->GetElapsedTimes().size()
		&& m_player1->GetElapsedTimes().size() > 0)
	{		
		if (m_player1->GetElapsedTimes().back() < m_player2->GetElapsedTimes().back())
		{	
			m_player1LoCount++;
		}
		else
		{
			m_player1HiCount++;
		}		
	}

	debugText += L"\n\nPER MOVE ANALYSIS\n";
	debugText += L"--------------------------------------------------------------------------------------------------------\n";
	debugText += L"Player 1 has been faster than player 2 (" + std::to_wstring(m_player1LoCount) + L" / " + std::to_wstring(m_player2->GetNumberOfMoves()) + L") moves(s) taken\n\n";

	// Display title/information for next boards
	if (m_nextStates.checkersBoards.size() > 0)
	{
		debugText += L"\nBOARDS (First depth)\n";
		debugText += L"--------------------------------------------------------------------------------------------------------\n";
	}

	SetDebugText(debugText);
}

void DebugOverlay::Render(QRenderer& renderer)
{
	// Draw the debug panels (backgrounds)
	renderer.DrawSprite(m_sprTextDebugPanel);
	//renderer.DrawSprite(m_sprNextStatesDebugPanel);

	// Draw the debug text panel text.
	m_guiCanvas->DrawString(m_debugTextLayout.Get(), 2, 2, 0xffffffff);

	int selectedIdx = -1;
	// Draw the boards that were considered by the AI.
	for (int i = 0; i < m_nextStates.checkersBoards.size(); ++i)
	{
		m_boardOffsetX = i / 4 * 110;
		m_boardOffsetY = i % 4 * 120;		
		MapFromCheckersBoard(m_nextStates.checkersBoards[i]);

		if (m_nextStates.checkersBoards[i] == m_selectedBoard)
		{
			selectedIdx = i;
			m_sprSelected.SetPosition({ m_boardOffsetX + 56, m_boardOffsetY + 328 });
			renderer.DrawSprite(m_sprSelected);
		}

		for (auto& checkersTile : m_checkersTiles)
		{
			renderer.DrawSprite(checkersTile.sprite);
		}
	}
	
	// Draw the scores
	for (int i = 0; i < m_nextStates.scores.size(); ++i)
	{
		SetScoreText(std::to_wstring((int)m_nextStates.scores[i]));

		int x = 10 + i / 4 * 110;
		int y = 377 + i % 4 * 120;
		if (i == selectedIdx)
		{
			m_guiCanvas->DrawString(m_scoreTextLayout.Get(), x, y, 0xff00ff00);
		}
		else
		{
			m_guiCanvas->DrawString(m_scoreTextLayout.Get(), x, y, 0xffffffff);
		}
	}	
}

void DebugOverlay::Reset(CheckersPlayer* p1, CheckersPlayer* p2)
{
	m_player1 = p1;
	m_player2 = p2;

	m_strPlayer1Type = PlayerTypeToWString(m_player1->GetPlayerType());
	m_strPlayer2Type = PlayerTypeToWString(m_player2->GetPlayerType());

	m_player1HiCount = 0;
	m_player1LoCount = 0;

	m_timeDiffTextHi = L"";
	m_timeDiffTextLo = L"";
	SetDebugText(L"[F1] - Toggle debug overlay");

	m_nextStates.clear();
}

void DebugOverlay::RecalculatePositions()
{
	m_sprTextDebugPanel.SetScale({ 120, 192 });
	m_sprTextDebugPanel.SetPosition({ 
		int(m_sprTextDebugPanel.GetSize().x / 2),
		int(m_sprTextDebugPanel.GetSize().y / 2) });

	m_sprSelected.SetScale({ 26, 26 });
}

void DebugOverlay::SetDebugText(const std::wstring& text)
{
	std::wstring fontFamily = GameSettings::Instance().GetFontFamily();
	auto dwriteFactory = m_guiCanvas->GetDWriteFactory();

	ComPtr<IDWriteTextFormat> debugTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		14.0f,
		L"en-gb",
		debugTextFormat.GetAddressOf());

	dwriteFactory->CreateTextLayout(
		text.c_str(),
		wcslen(text.c_str()),
		debugTextFormat.Get(),
		static_cast<float>(m_sprTextDebugPanel.GetSize().x),
		static_cast<float>(m_sprTextDebugPanel.GetSize().y),
		m_debugTextLayout.GetAddressOf());
}

void DebugOverlay::SetScoreText(const std::wstring& text)
{
	std::wstring fontFamily = GameSettings::Instance().GetFontFamily();
	auto dwriteFactory = m_guiCanvas->GetDWriteFactory();

	ComPtr<IDWriteTextFormat> scoreTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		14.0f,
		L"en-gb",
		scoreTextFormat.GetAddressOf());

	dwriteFactory->CreateTextLayout(
		text.c_str(),
		wcslen(text.c_str()),
		scoreTextFormat.Get(),
		100.0f,
		20.0f,
		m_scoreTextLayout.GetAddressOf());
}

void DebugOverlay::InitCheckersTiles()
{
	m_checkersTiles.clear();

	for (int index = 0; index < 64; ++index)
	{
		CheckersTile2D checkersTile;
		checkersTile.index = index;
		checkersTile.tileType = CheckersTileType::White;

		int row = index / 8;
		int col = index % 8;
		int alternate = (row % 2 == 0) ? 1 : 0;
		if ((col + alternate) % 2 == 0)
		{
			checkersTile.tileType = CheckersTileType::Black;
		}

		QSprite sprite;
		checkersTile.sprite = sprite;
		SetSpriteTextureByTileType(checkersTile.tileType, checkersTile.sprite);

		m_checkersTiles.push_back(checkersTile);
	}

	CalculateTilePositions();
}

void DebugOverlay::MapFromCheckersBoard(const CheckersBoard& board)
{
	InitCheckersTiles();

	for (int index = 0; index < 64; ++index)
	{
		bool p1man, p1king, p2man, p2king;
		p1man = board.p1Man(index);
		p1king = board.p1King(index);
		p2man = board.p2Man(index);
		p2king = board.p2King(index);

		if (p1man)
		{
			m_checkersTiles[index].tileType = CheckersTileType::Player1Piece;
		}
		else if (p2man)
		{
			m_checkersTiles[index].tileType = CheckersTileType::Player2Piece;
		}
		else if (p1king)
		{
			m_checkersTiles[index].tileType = CheckersTileType::Player1King;
		}
		else if (p2king)
		{
			m_checkersTiles[index].tileType = CheckersTileType::Player2King;
		}

		SetSpriteTextureByTileType(m_checkersTiles[index].tileType, m_checkersTiles[index].sprite);
	}
}

void DebugOverlay::SetSpriteTextureByTileType(CheckersTileType tileType, Quanta::QSprite& sprite) const
{
	TexturePool& texturePool = TexturePool::Instance();

	if (tileType == CheckersTileType::White)
	{
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_12x12));
		sprite.SetColour({ 1, 1, 1, 1 });
	}
	else if (tileType == CheckersTileType::Black)
	{
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_12x12));
		sprite.SetColour({ 0, 0, 0, 1 });
	}
	else if (tileType == CheckersTileType::Player1Piece)
	{
		sprite.SetColour({ 1, 1, 1, 1 });
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_WhiteCircle_12x12));
	}
	else if (tileType == CheckersTileType::Player2Piece)
	{
		sprite.SetColour({ 1, 1, 1, 1 });
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_RedCircle_12x12));
	}
	else if (tileType == CheckersTileType::Player1King)
	{
		sprite.SetColour({ 1, 1, 1, 1 });
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_WhiteCircleKing_12x12));
	}
	else if (tileType == CheckersTileType::Player2King)
	{
		sprite.SetColour({ 1, 1, 1, 1 });
		sprite.SetTexture(texturePool.GetTexture(TextureID::Square_RedCircleKing_12x12));
	}
}

void DebugOverlay::CalculateTilePositions()
{
	for (auto& checkersTile : m_checkersTiles)
	{
		int row = checkersTile.index / 8;
		int col = checkersTile.index % 8;
		int x = m_boardOffsetX + 14 + col * checkersTile.sprite.GetSize().x;
		int y = m_boardOffsetY + 286 + row * checkersTile.sprite.GetSize().y;
		checkersTile.sprite.SetPosition({ x, y });
	}
}