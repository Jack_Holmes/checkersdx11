#include "MeshPool.h"
#include "Application.h"
#include "Quanta/QTexture.h"

#include <cassert>

MeshContainer MeshPool::s_meshes;

using namespace Quanta;

QMesh* MeshPool::Load(ID3D11Device* d3dDevice, int id, const std::wstring& objFile, Quanta::QTexture* texture) const
{
	if (s_meshes.find(id) == s_meshes.end())
	{
		auto mesh = std::make_unique<QMesh>();
		mesh->Load(d3dDevice, objFile, texture);
		Add(id, std::move(mesh));
	}
	else
	{
		s_meshes[id]->Load(d3dDevice, objFile, texture);
	}
	
	return GetMesh(id);
}

QMesh* MeshPool::GetMesh(int id)
{
	return s_meshes[id].get();
}

void MeshPool::Add(int id, std::unique_ptr<Quanta::QMesh> mesh)
{	
	assert(mesh);
	s_meshes.insert(std::make_pair(id, std::move(mesh)));
}