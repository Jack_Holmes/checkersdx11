#include "GUICheckbox.h"
#include "GUICanvas.h"

using namespace Quanta;
using Microsoft::WRL::ComPtr;

GUICheckbox::GUICheckbox(GUICanvas* parentCanvas)
	: m_parentCanvas(parentCanvas),
	m_texChecked(nullptr),
	m_texUnchecked(nullptr),
	m_isChecked(false),
	m_horizontalPadding(0.0f),
	m_textColour(0xff000000),
	m_isVisible(true),
	m_isActive(true)
{
}

void GUICheckbox::Init(
	Quanta::QTexture* texChecked,
	Quanta::QTexture* texUnchecked,
	const std::wstring& text,
	const std::wstring& fontFamily,
	float fontSize)
{
	m_texChecked = texChecked;
	m_texUnchecked = texUnchecked;
	m_sprCheckbox.SetTexture(texUnchecked);

	auto dwriteFactory = m_parentCanvas->GetDWriteFactory();

	ComPtr<IDWriteTextFormat> textFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		fontSize,
		L"en-gb",
		textFormat.GetAddressOf());

	textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);

	dwriteFactory->CreateTextLayout(
		text.c_str(),
		wcslen(text.c_str()),
		textFormat.Get(),
		200.0f,
		static_cast<float>(m_sprCheckbox.GetSize().y),
		m_textLayout.GetAddressOf());
}

void GUICheckbox::SetCheckedCallback(std::function<void()> callback)
{
	m_checkedCallback = callback;
}

void GUICheckbox::SetUncheckedCallback(std::function<void()> callback)
{
	m_uncheckedCallback = callback;
}

void GUICheckbox::SetPosition(int x, int y)
{
	m_sprCheckbox.SetPosition({ x, y });
}

void GUICheckbox::SetHorizontalPadding(float padding)
{
	m_horizontalPadding = padding;
}

void GUICheckbox::SetTextColour(UINT32 colour)
{
	m_textColour = colour;
}

const DirectX::XMINT2& GUICheckbox::GetPosition() const
{
	return m_sprCheckbox.GetPosition();
}

void GUICheckbox::Render(QRenderer& renderer)
{
	if (!m_isVisible)
	{
		return;
	}

	renderer.DrawSprite(m_sprCheckbox);
	m_parentCanvas->DrawString(
		m_textLayout.Get(),
		float(m_sprCheckbox.GetPosition().x + m_sprCheckbox.GetSize().x) + m_horizontalPadding - (m_sprCheckbox.GetSize().x / 2),
		float(m_sprCheckbox.GetPosition().y) - (m_sprCheckbox.GetSize().y / 2),
		m_textColour);
}

void GUICheckbox::ProcessEvent(const QEvent& e)
{
	if (!m_isActive)
	{
		return;
	}

	if (e.type == QEvent::Type::MouseButtonReleased
		&& e.mouse.button == QMouse::Button::Left)
	{
		if (m_parentCanvas->MouseIntersects(m_sprCheckbox))
		{
			ToggleChecked();
		}		
	}
}

void GUICheckbox::SetChecked(bool checked)
{
	m_isChecked = checked;
	OnCheckedChanged();
}

bool GUICheckbox::IsChecked() const
{
	return m_isChecked;
}

void GUICheckbox::SetVisibility(bool visible)
{
	m_isVisible = visible;
}

void GUICheckbox::SetActive(bool active)
{
	m_isActive = active;
}

void GUICheckbox::ToggleChecked()
{
	m_isChecked = !m_isChecked;
	OnCheckedChanged();
}

void GUICheckbox::OnCheckedChanged()
{
	m_sprCheckbox.SetTexture(m_isChecked ? m_texChecked : m_texUnchecked);

	if (m_isChecked && m_checkedCallback)
	{
		m_checkedCallback();
	}
	else if (!m_isChecked && m_uncheckedCallback)
	{
		m_uncheckedCallback();
	}
}