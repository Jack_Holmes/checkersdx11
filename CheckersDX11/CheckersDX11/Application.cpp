#include "Application.h"
#include "Quanta/QClock.h"

#include "MenuScene.h"
#include "ControlsScene.h"
#include "GameSettingsScene.h"
#include "CustomBoardScene.h"
#include "GameSetupScene.h"
#include "GameScene.h"

#include "TexturePool.h"
#include "MeshPool.h"
#include "GameSettings.h"

using namespace DirectX;
using namespace Quanta;

Application::Application()
	: m_sceneManager(std::make_unique<SceneManager>(m_renderer.get(), m_window.get(), m_d3dDevice))
{
	m_window->SetTitle("Checkers");
	m_window->SetIcon("Media/Textures/checkers_icon.ico");
	m_renderer->SetVSyncEnabled(true);

	// Initialise the resource mapper
	m_resourceMapper = std::make_unique<ResourceMapper>(m_d3dDevice.Get());

	// Initialise the game settings. This must be done before setup as some stages during the
	// setup depend on the game settings (otherwise they will be set to defaulted settings).
	GameSettings::Instance().Init(m_sceneManager.get(), m_resourceMapper.get(), m_window.get(), m_renderer.get());
}

Application::~Application()
{
	// Before exiting the application save the user settings.
	GameSettings::Instance().Save();
}

int Application::Run()
{
	// Perform the application setup before entering the main application loop.
	SetUp();

	QClock clock;
	auto timeSinceLastUpdate = 0.0f;
	const auto targetDeltaTime = 1.0f / 60.0f;
	while (m_window->IsOpen())
	{
		ProcessEvents();		
		timeSinceLastUpdate += clock.Restart();
		while (timeSinceLastUpdate > targetDeltaTime)
		{
			timeSinceLastUpdate -= targetDeltaTime;			
			ProcessEvents();
			Update(targetDeltaTime);			
		}

		Render();
	}

	return 0;
}

void Application::ProcessEvents() const
{
	QEvent event;
	while (m_window->PollEvent(event))
	{
		if (event.type == QEvent::Type::Closed)
		{
			m_window->Close();
		}

		m_sceneManager->ProcessEvent(event);
	}
}

void Application::Update(float deltaTime) const
{
	m_sceneManager->Update(deltaTime);
}

void Application::Render() const
{
	// No point rendering if the window is minimised.
	if (m_window->IsMinimised())
	{
		return;
	}

	m_renderer->Clear(Colors::Blue);

	m_sceneManager->Render();

	m_renderer->Display();
}

void Application::SetUp()
{
	// Add scenes to the scene manager
	m_sceneManager->AddScene(std::make_unique<MenuScene>(m_sceneManager.get()));
	m_sceneManager->AddScene(std::make_unique<ControlsScene>(m_sceneManager.get()));
	m_sceneManager->AddScene(std::make_unique<GameSettingsScene>(m_sceneManager.get()));
	m_sceneManager->AddScene(std::make_unique<CustomBoardScene>(m_sceneManager.get()));
	m_sceneManager->AddScene(std::make_unique<GameSetupScene>(m_sceneManager.get()));
	m_sceneManager->AddScene(std::make_unique<GameScene>(m_sceneManager.get()));

	// Set the start scene
	m_sceneManager->SetScene("menu");
}