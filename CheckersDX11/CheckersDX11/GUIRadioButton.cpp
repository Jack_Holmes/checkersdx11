#include "GUIRadioButton.h"
#include "Quanta/QTexture.h"
#include "GUIRadioButtonGroup.h"
#include "GUICanvas.h"

using Microsoft::WRL::ComPtr;

GUIRadioButton::GUIRadioButton(GUIRadioButtonGroup* parentRadioButtonGroup, GUICanvas* parentCanvas)
	: m_parentRadioButtonGroup(parentRadioButtonGroup)
	, m_parentCanvas(parentCanvas)
	, m_isChecked(false)
	, m_horizontalPadding(0.0f)
	, m_textColour(0xff000000)
{
}

void GUIRadioButton::Init(
	const std::wstring& text,
	const std::wstring& fontFamily,
	float fontSize)
{
	auto dwriteFactory = m_parentCanvas->GetDWriteFactory();

	ComPtr<IDWriteTextFormat> textFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		fontSize,
		L"en-gb",
		textFormat.GetAddressOf());

	textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);

	dwriteFactory->CreateTextLayout(
		text.c_str(),
		wcslen(text.c_str()),
		textFormat.Get(),
		200.0f,
		static_cast<float>(m_sprRadioButton.GetSize().y),
		m_textLayout.GetAddressOf());
}

void GUIRadioButton::SetCheckedCallback(std::function<void()> callback)
{
	m_checkedCallback = callback;
}

void GUIRadioButton::SetUncheckedCallback(std::function<void()> callback)
{
	m_uncheckedCallback = callback;
}

void GUIRadioButton::SetChecked(bool check)
{
	m_isChecked = check;
	if (m_isChecked && m_checkedCallback)
	{
		m_checkedCallback();
	}
	else if (!m_isChecked && m_uncheckedCallback)
	{
		m_uncheckedCallback();
	}
}

void GUIRadioButton::SetTextColour(UINT32 colour)
{
	m_textColour = colour;
}

void GUIRadioButton::SetPosition(int x, int y)
{
	m_sprRadioButton.SetPosition({ x, y });
}

void GUIRadioButton::Render(Quanta::QRenderer& renderer)
{
	renderer.DrawSprite(m_sprRadioButton);
	m_parentCanvas->DrawString(
		m_textLayout.Get(),
		float(m_sprRadioButton.GetPosition().x + m_sprRadioButton.GetSize().x) + m_horizontalPadding - (m_sprRadioButton.GetSize().x / 2),
		float(m_sprRadioButton.GetPosition().y) - (m_sprRadioButton.GetSize().y / 2),
		m_textColour);
}

void GUIRadioButton::SetHorizontalPadding(float padding)
{
	m_horizontalPadding = padding;
}

const Quanta::QSprite& GUIRadioButton::GetSprite() const
{
	return m_sprRadioButton;
}

Quanta::QSprite& GUIRadioButton::GetSprite()
{
	return m_sprRadioButton;
}