#pragma once

#include <functional>
#include <string>
#include <dwrite.h>

#include "Quanta/QSprite.h"
#include "Quanta/QEvent.h"
#include "Quanta/QRenderer.h"

// Forward declarations
class GUICanvas;
class QTexture;

// The GUIButton is an interactive element controlled by the GUICanvas. It has
// two sprites, one for idle state and one when the user mouse is over the button.
class GUIButton
{
public:
	// Construct a GUIButton.
	// parentCanvas: The canvas parent to this button.
	explicit GUIButton(GUICanvas* parentCanvas);

	// Initialise the button with the two textures and text.
	// Note that Init must be called after adding a button to the canvas.
	void Init(
		Quanta::QTexture* idleTexture,
		Quanta::QTexture* mouseOverTexture,
		const std::wstring& text,
		const std::wstring& fontFamily,
		float fontSize);

	// Set the callback that will be invoked once the user presses the button.
	void SetCallback(std::function<void()> callback);

	// Render the button to the screen. Note this is managed by 
	// the parent canvas.
	void Render(Quanta::QRenderer& renderer);

	// Modify button properties
	void SetPosition(int x, int y);
	void SetColour(float r, float g, float b, float a);
	void SetTextColour(UINT32 colour);
	void SetScale(float x, float y);

	// Get button properties
	const DirectX::XMINT2& GetPosition() const;
	DirectX::XMUINT2 GetSize() const;

	// Process windows events. Note this is managed by the parent canvas.
	void ProcessEvent(const Quanta::QEvent& e);

	// Determines whether the button will be displayed on the screen.
	// Note that when visibility is set to false the button will still be
	// active (useful for invisible triggers).
	void SetVisibility(bool visible);

	// When the button is not active, all user interaction is ignored.
	void SetActive(bool active);

private:
	// The parent canvas that controls this button.
	GUICanvas* m_parentCanvas;

	// The text layout used for the text displayed on the button.
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_textLayout;

	// The two textures that are used for idle and mouseover states.
	Quanta::QTexture* m_texIdle;
	Quanta::QTexture* m_texMouseOver;

	// The sprite which represents the drawable button.
	Quanta::QSprite m_sprButton;

	// Button states
	enum class ButtonState
	{
		Idle, MouseOver
	} m_buttonState;

	// The callback function which is invoked when the user
	// presses the button (when active).
	std::function<void()> m_callback;

	// The colour of the text displayed on the button.
	UINT32 m_textColour;

	// Flag to determine if the button is drawn to the screen.
	bool m_isVisible;
	// Flag to determine if the button responds to user interaction.
	bool m_isActive;
};