#pragma once

#include <string>
#include <dwrite.h>
#include <functional>

#include "Quanta/QSprite.h"
#include "Quanta/QRenderer.h"

// Forward declarations
class GUIRadioButtonGroup;
class GUICanvas;

// The GUIRadioButton is an interactive element controlled by the GUIRadioButtonGroup. 
class GUIRadioButton
{
public:
	// Construct a GUIRadioButton.
	// parentRadioButtonGroup: The parent which controls a group of radio buttons
	// including this one.
	// parentCanvas: The canvas parent to this GUIRadioButton.
	GUIRadioButton(GUIRadioButtonGroup* parentRadioButtonGroup, GUICanvas* parentCanvas);

	// Initialise the radio button with the the text.
	// Note that Init must be called after adding a button to the radio button group.
	void Init(
		const std::wstring& text,
		const std::wstring& fontFamily,
		float fontSize);

	// Set the callback which is invoked when the radio button becomes checked.
	void SetCheckedCallback(std::function<void()> callback);

	// Set the callback which is invoked when the radio button becomes unchecked.
	void SetUncheckedCallback(std::function<void()> callback);

	// Set the checked state of the radio button.
	void SetChecked(bool check);

	// Modify the radio button properties.
	void SetTextColour(UINT32 colour);
	void SetPosition(int x, int y);
	void SetHorizontalPadding(float padding);

	// Render the radio button to the screen. Note this is managed by the
	// radio button group.
	void Render(Quanta::QRenderer& renderer);	

	// Get the internal sprite of the radio button. Note this is required
	// for the radio button group.
	const Quanta::QSprite& GetSprite() const;

	// Get the internal sprite of the radio button. Note this is required
	// for the radio button group.
	Quanta::QSprite& GetSprite();

private:
	// The parent radio button group managing this radio button.
	GUIRadioButtonGroup* m_parentRadioButtonGroup;
	// The parent canvas this radio button (and group) is a part of.
	GUICanvas* m_parentCanvas;

	// The text layout used for the text displayed beside the radio button.
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_textLayout;

	// Flag to track the checked state of the radio button.
	bool m_isChecked;
	
	// The drawable sprite that represents this radio button.
	Quanta::QSprite m_sprRadioButton;

	// The callback which is invoked when the radio button becomes checked.
	std::function<void()> m_checkedCallback;
	// The callback which is invoked when the radio button becomes unchecked.
	std::function<void()> m_uncheckedCallback;

	// The pixel space between the radio button and the text.
	float m_horizontalPadding;
	// The text colour of the text beside the radio button.
	UINT32 m_textColour;
};