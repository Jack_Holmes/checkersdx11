#include "HoverAnimation.h"

HoverAnimation::HoverAnimation(float startY, float minY, float maxY, float speed)
	: m_yValue(startY)
	, m_minY(minY)
	, m_maxY(maxY)
	, m_speed(speed)
	, m_direction(1)
{
}

void HoverAnimation::Update(float deltaTime)
{
	if (m_yValue > m_maxY)
		m_direction = -1;
	else if (m_yValue < m_minY)
		m_direction = 1;

	m_yValue += m_speed * deltaTime * m_direction;
}

float HoverAnimation::GetValue()
{
	return m_yValue;
}