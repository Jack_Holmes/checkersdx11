#pragma once

#include <d3d11.h>
#include <wrl/client.h>
#include "GameSettings.h"

class ResourceMapper
{
public:
	ResourceMapper(ID3D11Device* d3dDevice);

	void Map(GameTheme theme);

private:
	void MapTextures(GameTheme theme);
	void MapMeshes(GameTheme theme);

	Microsoft::WRL::ComPtr<ID3D11Device> m_d3dDevice;

	bool m_initialised;
	GameTheme m_lastTheme;

};