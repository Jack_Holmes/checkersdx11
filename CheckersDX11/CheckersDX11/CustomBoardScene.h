#pragma once

#include <dwrite.h>
#include <vector>
#include <string>

#include "Quanta/QCamera.h"
#include "Quanta/QSprite.h"

#include "Scene.h"
#include "CheckersTile2D.h"
#include "Agents/Common/checkersboard.cpp"

// The CustomBoardScene displays the checkers board editor. It enables the user
// to create their own checkers board layouts (and save/load them). Useful for debugging
// purposes and experimenting.
class CustomBoardScene : public Scene
{
public:
	// Construct a CustomBoardScene.
	// sceneManager: The scene manager that this scene has been added to.
	CustomBoardScene(SceneManager* sceneManager);

	// Scene overrides
	virtual void ProcessEvent(Quanta::QEvent& e);
	virtual void Update(float deltaTime);
	virtual void Render();
	virtual void OnEnter();
	virtual void OnGameThemeChanged(GameTheme theme);

private:
	// Initialise the GUI elements that are used in the scene.
	void InitGUI();

	// Initialises the 2D tiles that represent the checkers board layout.
	void InitCheckersTiles();

	// Convert the checkers board object to the 2d tiles that are
	// drawn on screen.
	void MapFromCheckersBoard(const CheckersBoard& checkerboard);

	// Convert from the 2d tiles to a checkers board object.
	void MapToCheckersBoard(CheckersBoard& checkersboard);

	// Recalculate the sprite positions/sizes. This needs to be called
	// any time the screen size changes.
	void RecalculateSprites();

	// Draw the different types of checkers tiles the user can select from.
	void RenderTileTypes();

	// Sets the texture the 2d tile sprite uses based on the checkers tile
	// it represents.
	static void SetSpriteTextureByTileType(CheckersTileType tileType, Quanta::QSprite& sprite);

	// Calculates the 2d tile positions based on the tile index to construct the
	// full 2d board.
	void CalculateTilePositions();

	// Invoked when the user chooses to load a checkers file.
	void OnLoadFilePressed();

	// Invoked when the user chooses to save the current checkers
	// board layout to a checkers file.
	void OnSaveToFilePressed();

	// Invoked when the user want to play a game of checkers with the
	// current board layout.
	void OnPlayPressed();

	// Invoked when the user want to return to the home (menu) screen.
	void OnHomePressed() const;

	// The text layout used for the title.
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_titleTextLayout;

	// Contains all the 2d checkers tiles that are draw to screen.
	std::vector<CheckersTile2D> m_checkersTiles;

	// The scene camera.
	Quanta::QCamera m_camera;

	// Sprites.
	Quanta::QSprite m_sprBackground;
	Quanta::QSprite m_sprWhitePiece;
	Quanta::QSprite m_sprWhiteKing;
	Quanta::QSprite m_sprRedPiece;
	Quanta::QSprite m_sprRedKing;
	Quanta::QSprite m_sprSelected;

	// Used to track which which tile type the user has selected
	// to place on the checkers board.
	CheckersTileType m_selectedTileType;

	// Primiary text colour used by the GUI Elements.
	UINT32 m_primaryTextColour;

	// Buttons.
	GUIButton* m_clearButton;
	GUIButton* m_loadFromFileButton;
	GUIButton* m_saveToFileButton;
	GUIButton* m_playButton;
	GUIButton* m_homeButton;
};