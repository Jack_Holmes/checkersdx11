#include "SceneManager.h"

#include <algorithm>

#include "Quanta/QEvent.h"

using namespace Quanta;
using Microsoft::WRL::ComPtr;

SceneManager::SceneManager(QRenderer* renderer, QWindow* window, ComPtr<ID3D11Device> device)
	: m_activeScene(nullptr)
	, m_renderer(renderer)
	, m_window(window)
	, m_soundEngine(nullptr)
	, m_device(device)
{
	m_soundEngine = irrklang::createIrrKlangDevice();
}

SceneManager::~SceneManager()
{
	if (m_soundEngine)
		m_soundEngine->drop();
}

void SceneManager::AddScene(std::unique_ptr<Scene> scene)
{
	m_scenes.push_back(std::move(scene));
}

void SceneManager::ProcessEvent(QEvent& e) const
{
	if (m_activeScene)
	{
		m_activeScene->ProcessEvent(e);
	}
}

void SceneManager::Update(float deltaTime) const
{
	if (m_activeScene)
	{
		m_activeScene->Update(deltaTime);
	}
}

void SceneManager::Render() const
{
	if (m_activeScene)
	{
		m_activeScene->Render();
	}
}

void SceneManager::SetScene(const std::string& name)
{
	auto res = std::find_if(m_scenes.begin(), m_scenes.end(), 
		[&](std::unique_ptr<Scene>& scene){ return scene->GetName() == name; });

	if (res != m_scenes.end())
	{
		if (m_activeScene)
		{
			m_activeScene->OnExit();
			m_previousSceneName = m_activeScene->GetName();
		}

		m_activeScene = (*res).get();
		m_activeScene->OnEnter();		
	}
}

void SceneManager::ReturnToPreviousScene()
{
	SetScene(m_previousSceneName);
}

Scene* SceneManager::GetScene(const std::string& name)
{
	auto res = std::find_if(m_scenes.begin(), m_scenes.end(),
		[&](std::unique_ptr<Scene>& scene){ return scene->GetName() == name; });

	if (res != m_scenes.end())
	{
		return (*res).get();
	}

	return nullptr;
}

QRenderer& SceneManager::GetRenderer() const
{
	return *m_renderer;
}

QWindow& SceneManager::GetWindow() const
{
	return *m_window;
}

irrklang::ISoundEngine& SceneManager::GetSoundEngine() const
{
	return *m_soundEngine;
}

ComPtr<ID3D11Device> SceneManager::GetDevice() const
{
	return m_device;
}

void SceneManager::OnGameThemeChanged(GameTheme theme) const
{
	for (auto& scene : m_scenes)
	{
		scene->OnGameThemeChanged(theme);
	}
}