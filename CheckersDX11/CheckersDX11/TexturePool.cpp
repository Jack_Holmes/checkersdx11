#include "TexturePool.h"
#include "Application.h"

#include <cassert>

TextureContainer TexturePool::s_textures;

using namespace Quanta;

Quanta::QTexture* TexturePool::Load(ID3D11Device* d3dDevice, int id, const std::wstring& ddsFile) const
{
	if (s_textures.find(id) == s_textures.end())
	{
		auto texture = std::make_unique<Quanta::QTexture>();
		texture->LoadDDSFromFile(d3dDevice, ddsFile);
		Add(id, std::move(texture));
	}
	else
	{
		//s_textures[id]->LoadDDSFromFile(d3dDevice, ddsFile);

		auto& t = s_textures[id];
		t->LoadDDSFromFile(d3dDevice, ddsFile);
	}
	
	return GetTexture(id);
}

void TexturePool::Clear()
{
	s_textures.clear();
}

Quanta::QTexture* TexturePool::GetTexture(int id)
{
	return s_textures[id].get();
}

void TexturePool::Add(int id, std::unique_ptr<Quanta::QTexture> texture)
{
	assert(texture);
	s_textures.insert(std::make_pair(id, std::move(texture)));
}