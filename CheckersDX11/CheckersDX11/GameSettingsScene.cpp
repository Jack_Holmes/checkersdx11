#include "GameSettingsScene.h"
#include "TexturePool.h"
#include "SceneManager.h"

using namespace Quanta;
using Microsoft::WRL::ComPtr;

GameSettingsScene::GameSettingsScene(SceneManager* sceneManager)
	: Scene("game_settings", sceneManager)
	, m_camera({0, 0, 0})
{
	GameSettings& gameSettings = GameSettings::Instance();
	m_previousSettings.musicEnabled = gameSettings.IsMusicEnabled();
	m_previousSettings.sfxEnabled = gameSettings.IsSoundEffectsEnabled();
	m_previousSettings.VSyncEnabled = gameSettings.IsVSyncEnabled();
	m_previousSettings.gameTheme = gameSettings.GetGameTheme();
	m_previousSettings.aiDifficulty = gameSettings.GetAIDifficulty();

	InitGUI();	
}

void GameSettingsScene::ProcessEvent(QEvent& e)
{
	m_guiCanvas->ProcessEvent(e);

	if (e.type == QEvent::Type::Resized)
	{
		RecalculateSprites();
	}
}

void GameSettingsScene::Update(float deltaTime)
{
}

void GameSettingsScene::Render()
{
	QRenderer& renderer = m_sceneManager->GetRenderer();
	QWindow& window = m_sceneManager->GetWindow();

	renderer.BeginSpritePass(m_camera);
	renderer.DrawSprite(m_sprBackground);
	renderer.DrawSprite(m_sprSettingsPanel);
	m_guiCanvas->Render();
	renderer.EndSpritePass();

	m_guiCanvas->DrawString(m_titleTextLayout.Get(), float(window.GetWidth() / 2 - 500), 10.0f, m_primaryTextColour);
	m_guiCanvas->DrawString(
		m_videoSectionTitleTextLayout.Get(),
		float(m_sprSettingsPanel.GetPosition().x - m_sprSettingsPanel.GetSize().x / 2) + 20.0f,
		float(m_sprSettingsPanel.GetPosition().y - m_sprSettingsPanel.GetSize().y / 2) + 20.0f,
		m_primaryTextColour);
	m_guiCanvas->DrawString(
		m_audioSectionTitleTextLayout.Get(),
		float(m_sprSettingsPanel.GetPosition().x - m_sprSettingsPanel.GetSize().x / 2) + 20.0f,
		float(m_sprSettingsPanel.GetPosition().y + 40),
		m_primaryTextColour);
	m_guiCanvas->DrawString(
		m_themeSubSectionTitleTextLayout.Get(),
		float(m_sprSettingsPanel.GetPosition().x - m_sprSettingsPanel.GetSize().x / 2) + 40.0f,
		float(m_sprSettingsPanel.GetPosition().y - m_sprSettingsPanel.GetSize().y / 2) + 100.0f,
		m_primaryTextColour);
	m_guiCanvas->DrawString(
		m_gameplaySectionTitleTextLayout.Get(),
		float(m_sprSettingsPanel.GetPosition().x) + 20.0f,
		float(m_sprSettingsPanel.GetPosition().y - m_sprSettingsPanel.GetSize().y / 2) + 20.0f,
		m_primaryTextColour);
}

void GameSettingsScene::OnEnter()
{
	RecalculateSprites();
	m_newSettings = m_previousSettings;
}

void GameSettingsScene::OnGameThemeChanged(GameTheme theme)
{
	m_primaryTextColour = 0xff000000;

	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		m_primaryTextColour = 0xff27848c;
	}

	m_musicCheckbox->SetTextColour(m_primaryTextColour);
	m_sfxCheckbox->SetTextColour(m_primaryTextColour);
	m_vSyncCheckbox->SetTextColour(m_primaryTextColour);
	m_theme1Radio->SetTextColour(m_primaryTextColour);
	m_theme2Radio->SetTextColour(m_primaryTextColour);
	m_aiDifficultySlider->SetTextColour(m_primaryTextColour);
	m_okButton->SetTextColour(m_primaryTextColour);
	m_cancelButton->SetTextColour(m_primaryTextColour);
}

void GameSettingsScene::InitGUI()
{
	TexturePool& texturePool = TexturePool::Instance();

	m_sprBackground.SetTexture(texturePool.GetTexture(TextureID::Background));
	m_sprSettingsPanel.SetTexture(texturePool.GetTexture(TextureID::SettingsPanel));

	std::wstring fontFamily = GameSettings::Instance().GetFontFamily();

	m_primaryTextColour = 0xff000000;
	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		m_primaryTextColour = 0xff27848c;
	}

	m_musicCheckbox = m_guiCanvas->AddCheckbox();
	m_musicCheckbox->Init(texturePool.GetTexture(TextureID::Checkbox_On), texturePool.GetTexture(TextureID::Checkbox_Off), L"Music", fontFamily, 12.0f);
	m_musicCheckbox->SetHorizontalPadding(20.0f);
	m_musicCheckbox->SetCheckedCallback([&](){ m_newSettings.musicEnabled = true; });
	m_musicCheckbox->SetUncheckedCallback([&](){ m_newSettings.musicEnabled = false; });
	m_musicCheckbox->SetChecked(m_previousSettings.musicEnabled);	
	m_musicCheckbox->SetTextColour(m_primaryTextColour);

	m_sfxCheckbox = m_guiCanvas->AddCheckbox();
	m_sfxCheckbox->Init(texturePool.GetTexture(TextureID::Checkbox_On), texturePool.GetTexture(TextureID::Checkbox_Off), L"Sound Effects", fontFamily, 12.0f);
	m_sfxCheckbox->SetHorizontalPadding(20.0f);
	m_sfxCheckbox->SetCheckedCallback([&](){ m_newSettings.sfxEnabled = true; });
	m_sfxCheckbox->SetUncheckedCallback([&](){ m_newSettings.sfxEnabled = false; });
	m_sfxCheckbox->SetChecked(m_previousSettings.sfxEnabled);
	m_sfxCheckbox->SetTextColour(m_primaryTextColour);

	m_vSyncCheckbox = m_guiCanvas->AddCheckbox();
	m_vSyncCheckbox->Init(texturePool.GetTexture(TextureID::Checkbox_On), texturePool.GetTexture(TextureID::Checkbox_Off), L"Vertical Sync", fontFamily, 12.0f);
	m_vSyncCheckbox->SetHorizontalPadding(20.0f);
	m_vSyncCheckbox->SetCheckedCallback([&](){ m_newSettings.VSyncEnabled = true; });
	m_vSyncCheckbox->SetUncheckedCallback([&](){ m_newSettings.VSyncEnabled = false; });
	m_vSyncCheckbox->SetChecked(m_previousSettings.VSyncEnabled);
	m_vSyncCheckbox->SetTextColour(m_primaryTextColour);

	m_themeRadioGroup = m_guiCanvas->AddRadioButtonGroup();
	m_themeRadioGroup->Init(texturePool.GetTexture(TextureID::Radio_On), texturePool.GetTexture(TextureID::Radio_Off));

	m_theme1Radio = m_themeRadioGroup->AddRadioButton();
	m_theme1Radio->Init(L"Theme 1", fontFamily, 12.0f);
	m_theme1Radio->SetHorizontalPadding(20.0f);
	m_theme1Radio->SetTextColour(m_primaryTextColour);
	m_theme1Radio->SetCheckedCallback([&](){ m_newSettings.gameTheme = GameTheme::Theme1; });
	if (m_previousSettings.gameTheme == GameTheme::Theme1)
	{
		m_themeRadioGroup->SetChecked(*m_theme1Radio);
	}

	m_theme2Radio = m_themeRadioGroup->AddRadioButton();
	m_theme2Radio->Init(L"Theme 2", fontFamily, 12.0f);
	m_theme2Radio->SetHorizontalPadding(20.0f);
	m_theme2Radio->SetTextColour(m_primaryTextColour);
	m_theme2Radio->SetCheckedCallback([&](){ m_newSettings.gameTheme = GameTheme::Theme2; });
	if (m_previousSettings.gameTheme == GameTheme::Theme2)
	{
		m_themeRadioGroup->SetChecked(*m_theme2Radio);
	}

	m_aiDifficultySlider = m_guiCanvas->AddSlider();
	m_aiDifficultySlider->Init(texturePool.GetTexture(TextureID::Slider), texturePool.GetTexture(TextureID::Slider_Handle), L"AI Difficulty", fontFamily, 16.0f, 1, 10, m_previousSettings.aiDifficulty);
	m_aiDifficultySlider->SetValueChangedCallback([&](){ m_newSettings.aiDifficulty = m_aiDifficultySlider->GetValue(); });
	m_aiDifficultySlider->SetTextColour(m_primaryTextColour);

	m_okButton = m_guiCanvas->AddButton();
	m_okButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"OK", fontFamily, 24.0f);
	m_okButton->SetCallback(std::bind(&GameSettingsScene::OnOKPressed, this));
	m_okButton->SetTextColour(m_primaryTextColour);

	m_cancelButton = m_guiCanvas->AddButton();
	m_cancelButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Cancel", fontFamily, 24.0f);
	m_cancelButton->SetCallback(std::bind(&GameSettingsScene::OnCancelPressed, this));
	m_cancelButton->SetTextColour(m_primaryTextColour);	

	auto dwriteFactory = m_guiCanvas->GetDWriteFactory();

	ComPtr<IDWriteTextFormat> titleTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		70.0f,
		L"en-gb",
		titleTextFormat.GetAddressOf());
	titleTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);

	ComPtr<IDWriteTextFormat> sectionTitleTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		25.0f,
		L"en-gb",
		sectionTitleTextFormat.GetAddressOf());

	ComPtr<IDWriteTextFormat> subSectionTitleTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		14.0f,
		L"en-gb",
		subSectionTitleTextFormat.GetAddressOf());

	auto titleText = L"Settings";
	dwriteFactory->CreateTextLayout(
		titleText,
		wcslen(titleText),
		titleTextFormat.Get(),
		1000.0f,
		400.0f,
		m_titleTextLayout.GetAddressOf());

	auto videoTitleText = L"Graphics/Video";
	dwriteFactory->CreateTextLayout(
		videoTitleText,
		wcslen(videoTitleText),
		sectionTitleTextFormat.Get(),
		float(m_sprSettingsPanel.GetSize().x),
		100.0f,
		m_videoSectionTitleTextLayout.GetAddressOf());

	auto audioTitleText = L"Audio";
	dwriteFactory->CreateTextLayout(
		audioTitleText,
		wcslen(audioTitleText),
		sectionTitleTextFormat.Get(),
		float(m_sprSettingsPanel.GetSize().x),
		100.0f,
		m_audioSectionTitleTextLayout.GetAddressOf());

	auto themeTitleText = L"Theme";
	dwriteFactory->CreateTextLayout(
		themeTitleText,
		wcslen(themeTitleText),
		subSectionTitleTextFormat.Get(),
		float(m_sprSettingsPanel.GetSize().x),
		100.0f,
		m_themeSubSectionTitleTextLayout.GetAddressOf());

	auto gameplayTitleText = L"Gameplay";
	dwriteFactory->CreateTextLayout(
		gameplayTitleText,
		wcslen(gameplayTitleText),
		sectionTitleTextFormat.Get(),
		float(m_sprSettingsPanel.GetSize().x),
		100.0f,
		m_gameplaySectionTitleTextLayout.GetAddressOf());

	RecalculateSprites();
}

void GameSettingsScene::RecalculateSprites()
{
	QWindow& window = m_sceneManager->GetWindow();
	UINT windowWidth = window.GetWidth();
	UINT windowHeight = window.GetHeight();

	m_sprBackground.SetScale({ windowWidth / 512.0f, windowHeight / 512.0f });
	m_sprBackground.SetPosition({ int(windowWidth / 2), int(windowHeight / 2) });

	m_sprSettingsPanel.SetPosition({ int(windowWidth / 2), int(windowHeight * 0.4f) });

	m_musicCheckbox->SetPosition(
		m_sprSettingsPanel.GetPosition().x - m_sprSettingsPanel.GetSize().x / 2 + 60,
		m_sprSettingsPanel.GetPosition().y + 100);

	m_sfxCheckbox->SetPosition(m_musicCheckbox->GetPosition().x, m_musicCheckbox->GetPosition().y + 32);

	m_vSyncCheckbox->SetPosition(
		m_sprSettingsPanel.GetPosition().x - m_sprSettingsPanel.GetSize().x / 2 + 60,
		m_sprSettingsPanel.GetPosition().y - m_sprSettingsPanel.GetSize().y / 2 + 70);

	m_themeRadioGroup->SetPosition(
		m_vSyncCheckbox->GetPosition().x,
		m_vSyncCheckbox->GetPosition().y + 70);

	m_aiDifficultySlider->SetPosition(
		m_sprSettingsPanel.GetPosition().x + 140,
		m_sprSettingsPanel.GetPosition().y - m_sprSettingsPanel.GetSize().y / 2 + 100);

	m_okButton->SetPosition(windowWidth / 2 - m_okButton->GetSize().x / 2, windowHeight - m_okButton->GetSize().y - 30);
	m_cancelButton->SetPosition(windowWidth / 2 + m_cancelButton->GetSize().x / 2, windowHeight - m_okButton->GetSize().y - 30);
}

void GameSettingsScene::OnOKPressed()
{
	GameSettings& gameSettings = GameSettings::Instance();
	gameSettings.SetGameTheme(m_newSettings.gameTheme);
	gameSettings.SetMusicEnabled(m_newSettings.musicEnabled);
	gameSettings.SetSoundEffectsEnabled(m_newSettings.sfxEnabled);
	gameSettings.SetVSyncEnabled(m_newSettings.VSyncEnabled);	
	gameSettings.SetAIDifficulty(m_newSettings.aiDifficulty);
	m_previousSettings = m_newSettings;
	m_sceneManager->ReturnToPreviousScene();
}

void GameSettingsScene::OnCancelPressed()
{
	m_newSettings = m_previousSettings;
	m_sceneManager->ReturnToPreviousScene();
}