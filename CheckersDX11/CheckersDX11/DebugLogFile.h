#pragma once

#include <vector>
#include <string>
#include "Agents/Common/checkersboard.cpp"

// The DebugState structure holds all the information that
// will be outputted to the debug log file.
struct DebugState
{
	int turn;
	int player1NumPieces;
	int player2NumPieces;
	int player1NumMen;
	int player2NumMen;
	int player1NumKings;
	int player2NumKings;
	float player1ElapsedTime;
	float player2ElapsedTime;
	std::string state;
};

// The DebugLogFile is responsible for holding a collections of logs based
// on the provided information (through LogCheckersBoard). These logs can
// be written to a log file.
class DebugLogFile
{
public:
	// Add a log.
	// board: The checkers board to log.
	// turn: The current turn number.
	// p1ElapsedTime: The time (in seconds) taken by player 1.
	// p2ElapsedTime: The time (in seconds) taken by player 2.
	void LogCheckersBoard(CheckersBoard* board, int turn, float p1ElapsedTime, float p2ElapsedTime);

	// Save all current logs to a file.
	// filename: The name of the file to save the logs to.
	void SaveToFile(const std::string& filename);

	// Clear any current logs.
	void Clear();

private:
	// Converts a DebugState object to a string.
	std::string DebugStateToString(const DebugState& state) const;

	// Holds all the debug states (logs).
	std::vector<std::string> m_debugStates; 
};