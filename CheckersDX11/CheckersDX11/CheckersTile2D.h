#pragma once

#include "CheckersTileType.h"
#include <Quanta/QSprite.h>

// The CheckersTile2D is a 2D renderable that represents a tile
// on the checkers board.
struct CheckersTile2D
{
	// The tile index that matches the checkers board index.
	int index;
	// The sprite used to draw the 2D tile.
	Quanta::QSprite sprite;
	// The type of tile this represents.
	CheckersTileType tileType;
};