#pragma once

#include <memory>

#include "Scene.h"

#include "Quanta/QSprite.h"
#include "Quanta/QCamera.h"
#include "Quanta/QMesh.h"

// The MenuScene is displayed to the user at the beginning of the application.
// And provides several options of navigation.
class MenuScene : public Scene
{
public:
	// Construct a MenuScene.
	// sceneManager: The scene manager that this scene has been added to.
	MenuScene(SceneManager* sceneManager);

	// Overrides.
	virtual void ProcessEvent(Quanta::QEvent& e);
	virtual void Update(float deltaTime);
	virtual void Render();
	virtual void OnEnter();
	virtual void OnGameThemeChanged(GameTheme theme);

private:
	// Initialise the GUI Elements.
	void InitGUI();

	// Recalculate the sprite positions/sizes. This must
	// be called if the window size changes.
	void RecalculateSprites();

	// Callbacks.
	void OnPlayPressed() const;
	void OnSettingsPressed() const;
	void OnControlsPressed() const;
	void OnCustomBoardLayoutPressed() const;
	void OnExitPressed();

	// The scene camera.
	Quanta::QCamera m_camera;

	// Sprites.
	Quanta::QSprite m_sprBackground;
	Quanta::QSprite m_sprTitle;
	Quanta::QSprite m_sprMenuPanel;

	// GUI Elements.
	GUIButton* m_playButton;
	GUIButton* m_settingsButton;
	GUIButton* m_controlsButton;
	GUIButton* m_customBoardButton;
	GUIButton* m_exitButton;

	Quanta::QMesh* m_checkersPiece1;
	Quanta::QMesh* m_checkersPiece2;
	Quanta::QTransform m_checkers1Transforms[5];
	Quanta::QTransform m_checkers2Transforms[5];
};