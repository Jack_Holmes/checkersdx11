#include <string>
#include "PlayerType.h"

std::wstring PlayerTypeToWString(PlayerType playerType)
{
	switch (playerType)
	{
	case PlayerType::CPU: return L"CPU";
	case PlayerType::CPU_BFS: return L"CPU_BFS";
	case PlayerType::GPU: return L"GPU";
	case PlayerType::Human: return L"Human";
	default: return L"Unknown";
	}
}