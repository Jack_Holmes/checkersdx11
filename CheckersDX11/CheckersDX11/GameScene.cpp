#include "GameScene.h"

#include "SceneManager.h"
#include "MeshPool.h"
#include "TexturePool.h"
#include "Agents/CUDA/kernel.hu"
#include "CheckersBoardFileHandler.h"

#include "Quanta/QUtility.h"
#include "Quanta/QRay.h"
#include "GameSettings.h"

using namespace DirectX;
using namespace Quanta;
using Microsoft::WRL::ComPtr;

GameScene::GameScene(SceneManager* sceneManager)
	: Scene("game", sceneManager)
	, m_camera({ 0, 16, -22 })
	, m_isPlayer1(true)
	, m_player1(PlayerType::Human)
	, m_player2(PlayerType::Human)
	, m_selectionHandler(&m_sceneManager->GetWindow(), &m_camera)
	, m_isPaused(false)
	, m_turnCount(0)
	, m_turnsAllowed(100)
	, m_depth(9)
	, m_latestBoardRendered(false)
	, m_gameOver(false)
	, m_showDebugLayer(false)
	, m_waitingForPlayerToMakeMove(false)
	, m_perspView(true)
	, m_playerSelectionHover(0.6f, 0.5f, 0.7f, 0.1f)
	, m_savedDebugLog(false)
	, m_debugOverlay(&m_sceneManager->GetWindow(), m_guiCanvas.get())
{
	m_camera.GetTransform().Rotate({ XMConvertToRadians(35), 0, 0 });

	InitLights();
	InitResources();

	m_debugOverlay.Reset(&m_player1, &m_player2);
	m_selectionHandler.SetMakeMoveCallback(std::bind(&GameScene::OnHumanPlayerMakeMove, this));
}

void GameScene::ProcessEvent(Quanta::QEvent& e)
{
	QWindow& window = m_sceneManager->GetWindow();

	m_guiCanvas->ProcessEvent(e);
	
	if (!m_isPaused && GetCurrentPlayerType() == PlayerType::Human)
	{
		m_selectionHandler.ProcessEvent(e);
	}	

	if (e.type == QEvent::Type::Resized)
	{
		m_camera.SetAspectRatio(static_cast<float>(window.GetWidth()) / window.GetHeight());
		RecalculateSprites();
	}
	else if (e.type == QEvent::Type::KeyPressed)
	{
		if (e.keyboard.key == QKeyboard::Key::F1)
		{
			m_showDebugLayer = !m_showDebugLayer;
		}
		else if (e.keyboard.key == QKeyboard::Key::Escape)
		{
			SetPaused(!m_isPaused);
		}
		else if (e.keyboard.key == QKeyboard::Key::Space)
		{
			OnSwitchCameraView();
		}
	}
}

void GameScene::Update(float deltaTime)
{
	// Update the selection handler which manages what a human player is selecting on the board
	m_selectionHandler.Update(deltaTime);

	// Update the hover animation on the currently selected piece
	m_playerSelectionHover.Update(deltaTime);

	// Check if the game is over
	if (m_checkersBoard.p1NumPieces() <= 0 ||
		m_checkersBoard.p2NumPieces() <= 0 ||
		m_turnCount >= m_turnsAllowed)
	{
		m_gameOver = true;
	}

	// Proceed the AI to make move
	if (!m_isPaused && CanMakeMove())
	{
		bool aiMadeMove = false;

		// If the current player is AI, then make a move and update the players
		if (m_isPlayer1 && m_player1.GetPlayerType() != PlayerType::Human)
		{		
			auto elapsedTime = MakeMove(m_player1.GetPlayerType());
			m_player1.AddMove(elapsedTime);
			aiMadeMove = true;
		}
		else if (!m_isPlayer1 &&  m_player2.GetPlayerType() != PlayerType::Human)
		{
			auto elapsedTime = MakeMove(m_player2.GetPlayerType());
			m_player2.AddMove(elapsedTime);
			aiMadeMove = true;
		}

		if (aiMadeMove)
		{
			// Update the debug overlay with the new information
			m_debugOverlay.Update(m_turnCount, m_checkersBoard, m_nextStates);
			m_nextStates.clear(); m_nextStates.clear();
			m_debugLogFile.LogCheckersBoard(
				&m_checkersBoard,
				m_turnCount,
				m_player1.GetTotalElapsedTime(),
				m_player2.GetTotalElapsedTime());

			// End of turn so swap the players and increase the turn count
			SwapPlayers();			
			m_turnCount++;			

			// Start the clock if our opponent is a human player
			if (GetOpponentPlayerType() == PlayerType::Human)
			{
				m_humanTurnClock.Restart();
			}
		}		
	}


	if (m_gameOver)
	{
		OnGameOver();
	}
}

void GameScene::Render()
{
	QRenderer& renderer = m_sceneManager->GetRenderer();	

	// Background
	renderer.BeginSpritePass(m_camera);
	renderer.DrawSprite(m_sprBackground);
	renderer.EndSpritePass();

	// Checkers board and pieces
	renderer.BeginOpaquePass(m_camera);
	RenderBoard(m_checkersBoard);
	RenderCapturedCheckerPieces();
	m_selectionHandler.Render(renderer);
	renderer.EndOpaquePass();

	// Game overlays
	renderer.BeginSpritePass(m_camera);
	if (!m_isPaused)
	{
		if (!m_gameOver)
		{
			RenderTopPanel();
		}
		else
		{
			RenderGameOverPanel();
		}
	}
	else
	{
		RenderPauseMenu();
	}

	m_guiCanvas->Render();

	if (m_showDebugLayer)
	{
		m_debugOverlay.Render(renderer);
	}
	renderer.EndSpritePass();		
}

void GameScene::OnEnter()
{
	QWindow& window = m_sceneManager->GetWindow();
	m_camera.SetAspectRatio(static_cast<float>(window.GetWidth()) / window.GetHeight());
	SetPaused(false);
	RecalculateSprites();	
	OnGameOver(); // reset gameover overlay
}

void GameScene::OnGameThemeChanged(GameTheme theme)
{
	// Update the text colours when the game theme changes
	m_primaryTextColour = 0xff000000;
	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		m_primaryTextColour = 0xff27848c;
	}

	m_cameraViewButton->SetTextColour(m_primaryTextColour);
	m_saveButton->SetTextColour(m_primaryTextColour);
	m_settingsButton->SetTextColour(m_primaryTextColour);
	m_resumeButton->SetTextColour(m_primaryTextColour);
	m_exitButton->SetTextColour(m_primaryTextColour);
	m_playAgainButton->SetTextColour(m_primaryTextColour);
	m_gameOverExitButton->SetTextColour(m_primaryTextColour);
}

void GameScene::SetUp(CheckersPlayer p1, CheckersPlayer p2, bool showDebugLayer)
{
	// Set the player types and reset the game with these types
	m_player1 = p1;
	m_player2 = p2;
	ResetGame();	
	m_showDebugLayer = showDebugLayer;
}

void GameScene::SetCheckersBoard(const CheckersBoard& checkersBoard)
{
	m_checkersBoard = checkersBoard;
}

void GameScene::ResetGame()
{
	// Reset the following properties ready for a new game
	// Checkers boards
	m_checkersBoard = CheckersBoard();
	m_gpuOtherCheckersBoard = CheckersBoard();
	m_turnCount = 0;

	// State flags
	m_isPlayer1 = true;
	m_isPaused = false;
	m_gameOver = false;
	
	// Selection handler
	m_selectionHandler.Reset();	

	// Debugging
	m_savedDebugLog = false;
	m_debugOverlay.Reset(&m_player1, &m_player2);
	m_debugLogFile.Clear();

	// Reset mesh tranforms
	m_checkersPieceMesh1->GetTransform() = QTransform();
	m_checkersPieceMesh2->GetTransform() = QTransform();
	m_checkersPieceKing1->GetTransform() = QTransform();
	m_checkersPieceKing2->GetTransform() = QTransform();

	// Set the AI difficulty obtained from the game settings
	m_depth = GameSettings::Instance().GetAIDifficulty();

	m_humanTurnClock.Restart();
}

void GameScene::InitLights() const
{
	QRenderer& renderer = m_sceneManager->GetRenderer();
	
	QLight pointLight;
	pointLight.type = QLightType::Point;
	pointLight.position = { 0, 4, 0 };
	pointLight.range = 100.0f;
	pointLight.colour = { 0.5f, 0.4f, 0.8f, 0.0f };
	pointLight.attenuation = { 0.4f, 0.0f, 0.0f };
	renderer.AddLight(pointLight);

	QLight directionalLight;
	directionalLight.type = QLightType::Directional;
	directionalLight.colour = { 0.6f, 0.6f, 0.6f, 0.0f };
	directionalLight.direction = { 0.7f, 0.7f, 0.0f };
	renderer.AddLight(directionalLight);
}

void GameScene::InitResources()
{
	// Meshes
	MeshPool& meshPool = MeshPool::Instance();
	m_checkersBoardMesh = meshPool.GetMesh(MeshID::CheckersBoard);
	m_checkersPieceMesh1 = meshPool.GetMesh(MeshID::CheckersPiece1);
	m_checkersPieceMesh2 = meshPool.GetMesh(MeshID::CheckersPiece2);
	m_checkersPieceKing1 = meshPool.GetMesh(MeshID::CheckersPieceKing1);
	m_checkersPieceKing2 = meshPool.GetMesh(MeshID::CheckersPieceKing2);

	// Sprites
	TexturePool& texturePool = TexturePool::Instance();
	m_sprBackground.SetTexture(texturePool.GetTexture(TextureID::Background));
	m_sprPausedBackground.SetTexture(texturePool.GetTexture(TextureID::PausedBackground));
	m_sprTopPanel.SetTexture(texturePool.GetTexture(TextureID::TopPanel));
	m_sprPauseMenu.SetTexture(texturePool.GetTexture(TextureID::PausedPanel));
	m_sprTurnPiece.SetTexture(texturePool.GetTexture(TextureID::TurnPiece_1));
	m_sprGameOverPanel.SetTexture(texturePool.GetTexture(TextureID::GameOverPanel));

	// Fonts
	auto dwriteFactory = m_guiCanvas->GetDWriteFactory();

	std::wstring fontFamily = GameSettings::Instance().GetFontFamily();

	ComPtr<IDWriteTextFormat> pauseMenuTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		32.0f,
		L"en-gb",
		pauseMenuTextFormat.GetAddressOf());

	pauseMenuTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);

	auto pauseMenuText = L"PAUSED";
	dwriteFactory->CreateTextLayout(
		pauseMenuText,
		wcslen(pauseMenuText),
		pauseMenuTextFormat.Get(),
		static_cast<float>(m_sprPauseMenu.GetSize().x),
		static_cast<float>(m_sprPauseMenu.GetSize().y),
		m_pauseMenuTitleTextLayout.GetAddressOf());

	m_primaryTextColour = 0xff000000;
	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		m_primaryTextColour = 0xff27848c;
	}

	// Setup the buttons (textures, text, callback etc.)
	m_cameraViewButton = m_guiCanvas->AddButton();
	m_cameraViewButton->Init(texturePool.GetTexture(TextureID::ButtonSwitchCameraIdle), texturePool.GetTexture(TextureID::ButtonSwitchCameraMouseOver), L"", fontFamily, 24.0f);
	m_cameraViewButton->SetCallback(std::bind(&GameScene::OnSwitchCameraView, this));
	m_cameraViewButton->SetTextColour(m_primaryTextColour);

	m_saveButton = m_guiCanvas->AddButton();
	m_saveButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Save", fontFamily, 24.0f);
	m_saveButton->SetCallback(std::bind(&GameScene::OnSavePressed, this));
	m_saveButton->SetVisibility(m_isPaused);
	m_saveButton->SetTextColour(m_primaryTextColour);

	m_settingsButton = m_guiCanvas->AddButton();
	m_settingsButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Settings", fontFamily, 24.0f);
	m_settingsButton->SetCallback(std::bind(&GameScene::OnSettingsPressed, this));
	m_settingsButton->SetVisibility(m_isPaused);
	m_settingsButton->SetTextColour(m_primaryTextColour);

	m_resumeButton = m_guiCanvas->AddButton();
	m_resumeButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Resume", fontFamily, 24.0f);
	m_resumeButton->SetCallback(std::bind(&GameScene::OnResumedPressed, this));
	m_resumeButton->SetVisibility(m_isPaused);
	m_resumeButton->SetTextColour(m_primaryTextColour);

	m_exitButton = m_guiCanvas->AddButton();
	m_exitButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Exit", fontFamily, 24.0f);
	m_exitButton->SetCallback(std::bind(&GameScene::OnExitPressed, this));
	m_exitButton->SetVisibility(m_isPaused);
	m_exitButton->SetTextColour(m_primaryTextColour);

	m_playAgainButton = m_guiCanvas->AddButton();
	m_playAgainButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Play Again", fontFamily, 24.0f);
	m_playAgainButton->SetCallback([&](){ m_sceneManager->SetScene("game_setup"); });
	m_playAgainButton->SetActive(m_gameOver);
	m_playAgainButton->SetVisibility(m_gameOver);
	m_playAgainButton->SetTextColour(m_primaryTextColour);

	m_gameOverExitButton = m_guiCanvas->AddButton();
	m_gameOverExitButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Exit", fontFamily, 24.0f);
	m_gameOverExitButton->SetCallback(std::bind(&GameScene::OnExitPressed, this));
	m_gameOverExitButton->SetActive(m_gameOver);
	m_gameOverExitButton->SetVisibility(m_gameOver);
	m_gameOverExitButton->SetTextColour(m_primaryTextColour);

	RecalculateSprites();
}

float GameScene::MakeMove(PlayerType playerType)
{
	int opponentPiecesBefore = GetNumberOfOpponentPlayerPieces();

	QClock clock;
	if (playerType == PlayerType::CPU)
	{
		if (!m_checkersSearch.isTerminal(&m_checkersBoard, m_isPlayer1))
		{
			m_checkersBoard = m_checkersSearch.search(&m_checkersBoard, m_nextStates, m_isPlayer1, m_depth);
		}
		else
		{
			m_gameOver = true;
		}		
	}
	else if (playerType == PlayerType::GPU)
	{
		m_checkersBoard = getOptimalNextBoard(m_gpuOtherCheckersBoard);
		m_gpuOtherCheckersBoard = m_checkersBoard;

		if (m_isPlayer1 || GetOpponentPlayerType() == PlayerType::GPU)
		{
			if (!m_checkersBoard.getP1Up())
			{
				m_checkersBoard.swapPlayers();
			}
		}		
	}
	else if (playerType == PlayerType::CPU_BFS)
	{
		if (!m_bfs.isTerminal(m_checkersBoard, m_isPlayer1, m_turnCount))
		{
			int depth = 6;
			m_checkersBoard = m_bfs.search(m_checkersBoard, depth, m_isPlayer1, m_turnCount);
		}
		else
		{
			m_gameOver = true;
		}
	}

	auto elapsedTime = clock.GetElapsedTime();

	// Play a sound effect depending on the move type
	if (GameSettings::Instance().IsSoundEffectsEnabled())
	{
		if (opponentPiecesBefore > GetNumberOfOpponentPlayerPieces())
		{
			m_sceneManager->GetSoundEngine().play2D("Media/Audio/Sounds/capture_piece.wav");
		}
		else
		{
			m_sceneManager->GetSoundEngine().play2D("Media/Audio/Sounds/move_piece.wav");
		}
	}	

	return elapsedTime;
}

bool GameScene::CanMakeMove() const
{
	return (!m_gameOver
		&& !m_waitingForPlayerToMakeMove
		&& m_latestBoardRendered
		&& (m_turnCount < m_turnsAllowed)
		&& m_checkersBoard.p1NumPieces() > 0
		&& m_checkersBoard.p2NumPieces() > 0);
}

PlayerType GameScene::GetCurrentPlayerType() const
{
	return m_isPlayer1
		? m_player1.GetPlayerType()
		: m_player2.GetPlayerType();
}

PlayerType GameScene::GetOpponentPlayerType() const
{
	return m_isPlayer1 
		? m_player2.GetPlayerType()
		: m_player1.GetPlayerType();
}

int GameScene::GetNumberOfOpponentPlayerPieces() const
{
	return m_isPlayer1
		? m_checkersBoard.p2NumPieces()
		: m_checkersBoard.p1NumPieces();
}

void GameScene::RenderBoard(CheckersBoard& board)
{
	QRenderer& renderer = m_sceneManager->GetRenderer();
	TexturePool& texturePool = TexturePool::Instance();

	// Draw the checkersboard mesh
	renderer.DrawMesh(*m_checkersBoardMesh);

	for (size_t index = 0; index < 64; ++index)
	{		
		if (board.isOccupiableSpace(index))
		{
			int row = index / 8;
			int col = index % 8;
			// World position based on row/col
			float x = -7.f + col * 2.0f;
			float y = 0.4f;
			float z = 7.f - row * 2.0f;

			// Update the selection handler with the tile type
			m_selectionHandler.UpdateTile(index, CheckersTileType::White);
			int alternate = 0;
			if (row % 2 == 0) alternate = 1;
			if ((col + alternate) % 2 == 0)
			{
				m_selectionHandler.UpdateTile(index, CheckersTileType::Black);
			}

			// Raise the tile that is selected by the player
			if (m_selectionHandler.GetSourceIndex() == index)
			{
				y = m_playerSelectionHover.GetValue();
			}

			// Get the piece type at the current index
			bool p1man, p1king, p2man, p2king;
			p1man = board.p1Man(index);
			p1king = board.p1King(index);
			p2man = board.p2Man(index);
			p2king = board.p2King(index);

			// Draw the mesh checkers peices
			if (p1man)
			{
				m_checkersPieceMesh1->GetTransform().SetPosition({ x, y, z });				
				renderer.DrawMesh(*m_checkersPieceMesh1);				
				m_selectionHandler.UpdateTile(index, CheckersTileType::Player1Piece);
			}
			else if (p1king)
			{
				m_checkersPieceMesh1->GetTransform().SetPosition({ x, y, z });
				renderer.DrawMesh(*m_checkersPieceMesh1);
				m_checkersPieceKing1->GetTransform().SetPosition({ x, y + 0.4f, z });
				renderer.DrawMesh(*m_checkersPieceKing1);
				m_selectionHandler.UpdateTile(index, CheckersTileType::Player1King);
			}
			else if (p2man)
			{
				m_checkersPieceMesh2->GetTransform().SetPosition({ x, y, z });
				renderer.DrawMesh(*m_checkersPieceMesh2);
				m_selectionHandler.UpdateTile(index, CheckersTileType::Player2Piece);
			}
			else if (p2king)
			{
				m_checkersPieceMesh2->GetTransform().SetPosition({ x, y, z });
				renderer.DrawMesh(*m_checkersPieceMesh2);
				m_checkersPieceKing2->GetTransform().SetPosition({ x, y + 0.4f, z });
				renderer.DrawMesh(*m_checkersPieceKing2);
				m_selectionHandler.UpdateTile(index, CheckersTileType::Player2King);
			}
		}
	}

	// Notfiy the most up to date checkersboard has been rendered to the display
	m_latestBoardRendered = true;
}

void GameScene::RenderCapturedCheckerPieces() const
{
	QRenderer& renderer = m_sceneManager->GetRenderer();
	
	// Render the pieces on either side of the checkers board that have been captured
	// by the players

	// Pieces capture by player 1
	int capturedPieces = 12 - m_checkersBoard.p2NumPieces();
	float x = -11.8f;
	for (int i = 0; i < capturedPieces; ++i)
	{
		m_checkersPieceMesh2->GetTransform().SetPosition({ x + (i % 2) * 2.0f, 0.0f, 5.0f - (i / 2) * 2.0f });
		renderer.DrawMesh(*m_checkersPieceMesh2);
	}	
	
	// Pieces capture by player 2
	capturedPieces = 12 - m_checkersBoard.p1NumPieces();
	x *= -1;
	for (int i = 0; i < capturedPieces; ++i)
	{
		m_checkersPieceMesh1->GetTransform().SetPosition({ x - (i % 2) * 2.0f, 0.0f, 5.0f - (i / 2) * 2.0f });
		renderer.DrawMesh(*m_checkersPieceMesh1);
	}
}

void GameScene::RenderTopPanel()
{
	QRenderer& renderer = m_sceneManager->GetRenderer();

	// Draw the panel that is displayed at the top center of the screen which displays
	// whos turn it is
	renderer.DrawSprite(m_sprTopPanel);

	std::wstring turnText = L"Turn: ";
	m_sprTurnPiece.SetPosition({ m_sprTopPanel.GetPosition().x - 80, m_sprTopPanel.GetPosition().y });
	if (m_isPlayer1 && m_player1.GetPlayerType() != PlayerType::Human)
	{
		turnText = L"Turn (AI):";
		m_sprTurnPiece.SetPosition({ m_sprTopPanel.GetPosition().x, m_sprTopPanel.GetPosition().y });
	}
	else if (!m_isPlayer1 && m_player2.GetPlayerType() != PlayerType::Human)
	{
		turnText = L"Turn (AI):";
		m_sprTurnPiece.SetPosition({ m_sprTopPanel.GetPosition().x, m_sprTopPanel.GetPosition().y });
	}

	m_guiCanvas->DrawString(
		turnText.c_str(),
		44.0f,
		m_sprTopPanel.GetPosition().x - m_sprTopPanel.GetSize().x / 2 + 30.0f,
		m_sprTopPanel.GetPosition().y - m_sprTopPanel.GetSize().y / 2 + 10.0f,
		m_primaryTextColour);

	if (m_isPlayer1)
	{
		m_sprTurnPiece.SetTexture(TexturePool::Instance().GetTexture(TextureID::TurnPiece_1));
	}
	else
	{
		m_sprTurnPiece.SetTexture(TexturePool::Instance().GetTexture(TextureID::TurnPiece_2));
	}

	renderer.DrawSprite(m_sprTurnPiece);
}

void GameScene::RenderPauseMenu()
{
	QRenderer& renderer = m_sceneManager->GetRenderer();

	renderer.DrawSprite(m_sprPausedBackground);
	renderer.DrawSprite(m_sprPauseMenu);

	m_guiCanvas->DrawString(
		m_pauseMenuTitleTextLayout.Get(),
		static_cast<float>(m_sprPauseMenu.GetPosition().x - m_sprPauseMenu.GetSize().x / 2),
		static_cast<float>(m_sprPauseMenu.GetPosition().y - m_sprPauseMenu.GetSize().y / 2) + 10,
		m_primaryTextColour);
}

void GameScene::RenderGameOverPanel()
{
	QRenderer& renderer = m_sceneManager->GetRenderer();

	renderer.DrawSprite(m_sprGameOverPanel);
	m_guiCanvas->DrawString(
		m_gameOverTextLayout.Get(),
		static_cast<float>(m_sprGameOverPanel.GetPosition().x - m_sprGameOverPanel.GetSize().x / 2),
		static_cast<float>(m_sprGameOverPanel.GetPosition().y - m_sprGameOverPanel.GetSize().y / 2) + 10,
		m_primaryTextColour);
}

void GameScene::RecalculateSprites()
{
	// Calculate sprite positions on the screen. This only needs to be called on initialisation
	// and when the window is resized.
	QWindow& window = m_sceneManager->GetWindow();
	UINT windowWidth = window.GetWidth();
	UINT windowHeight = window.GetHeight();

	// Background
	m_sprBackground.SetScale({ windowWidth / 512.0f, windowHeight / 512.0f });
	m_sprBackground.SetPosition({ int(windowWidth / 2), int(windowHeight / 2) });

	// Pause background
	m_sprPausedBackground.SetScale({ windowWidth / 4.0f, windowHeight / 4.0f });
	m_sprPausedBackground.SetPosition({ int(windowWidth / 2), int(windowHeight / 2) });

	// Top panel
	m_sprTopPanel.SetPosition({ int(windowWidth / 2), int(m_sprTopPanel.GetSize().y / 2) });
	m_sprTurnPiece.SetPosition({ m_sprTopPanel.GetPosition().x - 80, m_sprTopPanel.GetPosition().y });

	// Game over panel
	m_sprGameOverPanel.SetPosition({ int(windowWidth / 2), int(windowHeight / 2) });
	m_playAgainButton->SetPosition(
		m_sprGameOverPanel.GetPosition().x - m_playAgainButton->GetSize().x / 2,
		m_sprGameOverPanel.GetPosition().y + m_sprGameOverPanel.GetSize().y / 2 - m_playAgainButton->GetSize().y / 2 - 30);
	m_gameOverExitButton->SetPosition(
		m_sprGameOverPanel.GetPosition().x + m_gameOverExitButton->GetSize().x / 2,
		m_sprGameOverPanel.GetPosition().y + m_sprGameOverPanel.GetSize().y / 2 - m_gameOverExitButton->GetSize().y / 2 - 30);

	// Pause menu
	m_sprPauseMenu.SetPosition({ int(window.GetWidth() / 2), int(window.GetHeight() / 2) });
	m_saveButton->SetPosition(
		m_sprPauseMenu.GetPosition().x, 
		m_sprPauseMenu.GetPosition().y - m_sprPauseMenu.GetSize().y / 2 + m_saveButton->GetSize().y / 2 + 50);
	m_settingsButton->SetPosition(m_saveButton->GetPosition().x, m_saveButton->GetPosition().y + m_settingsButton->GetSize().y + 10);
	m_resumeButton->SetPosition(m_settingsButton->GetPosition().x, m_settingsButton->GetPosition().y + m_resumeButton->GetSize().y + 10);
	m_exitButton->SetPosition(m_resumeButton->GetPosition().x, m_resumeButton->GetPosition().y + m_exitButton->GetSize().y + 10);

	// Camera view button
	m_cameraViewButton->SetPosition(
		windowWidth - m_cameraViewButton->GetSize().x / 2 - 10,
		m_cameraViewButton->GetSize().y / 2 + 10);
}

void GameScene::OnHumanPlayerMakeMove()
{
	// Obtain the source and destination from our selection handler
	int sourceIndex = m_selectionHandler.GetSourceIndex();
	int destinationIndex = m_selectionHandler.GetDestinationIndex();

	// Check that this is a legal move
	if (m_checkersBoard.isOccupiableSpace(sourceIndex)
		&& m_checkersBoard.isOccupiableSpace(destinationIndex))
	{
		if (m_checkersBoard.canMove(destinationIndex, sourceIndex, m_isPlayer1))
		{
			bool captureMove = false;
			bool isKingPiece =
				(m_isPlayer1 && m_checkersBoard.p1King(sourceIndex))
				|| (!m_isPlayer1 && m_checkersBoard.p2King(sourceIndex));

			// Based on the source and destination this maybe a capture move
			if (destinationIndex == sourceIndex - 14)
			{
				m_checkersBoard.captureMove(destinationIndex, sourceIndex - 7, sourceIndex);
				captureMove = true;
			}
			else if (destinationIndex == sourceIndex - 18)
			{
				m_checkersBoard.captureMove(destinationIndex, sourceIndex - 9, sourceIndex);
				captureMove = true;
			}
			else if (destinationIndex == sourceIndex + 14)
			{
				m_checkersBoard.captureMove(destinationIndex, sourceIndex + 7, sourceIndex);
				captureMove = true;
			}
			else if (destinationIndex == sourceIndex + 18)
			{
				m_checkersBoard.captureMove(destinationIndex, sourceIndex + 9, sourceIndex);
				captureMove = true;
			}
			// Otherwise, a simple move.
			else
			{
				m_checkersBoard.move(destinationIndex, sourceIndex);
			}			
			
			// Check if our man piece has been promoted to a king
			bool promotedToKing = false;
			if (!isKingPiece && (
				(m_isPlayer1 && m_checkersBoard.p1King(destinationIndex))
				|| (!m_isPlayer1 && m_checkersBoard.p2King(destinationIndex))) )
			{
				promotedToKing = true;
			}

			bool isMultiJump = false;
			if (captureMove)
			{
				RenderBoard(m_checkersBoard);
				// Player has captured a piece. Check if multi jump is available.
				m_selectionHandler.SetSourceIndex(destinationIndex);
				isMultiJump = m_selectionHandler.IsMultiJump();
				
				// Multijump is not allowed when promoted to king...
				if (isMultiJump && promotedToKing)
				{
					isMultiJump = false;
					m_selectionHandler.SetMultiJump(isMultiJump);
					m_selectionHandler.ClearSelections();
				}
			}

			// Play a sound effect depending on the move type
			if (GameSettings::Instance().IsSoundEffectsEnabled())
			{
				captureMove 
					? m_sceneManager->GetSoundEngine().play2D("Media/Audio/Sounds/capture_piece.wav")
					: m_sceneManager->GetSoundEngine().play2D("Media/Audio/Sounds/move_piece.wav");
			}

			// Update the player and restart the clock
			if (m_isPlayer1)
			{
				m_player1.AddMove(m_humanTurnClock.Restart());
			}
			else
			{
				m_player2.AddMove(m_humanTurnClock.Restart());
			}

			// Update the debug overlay with the new information
			m_debugOverlay.Update(m_turnCount, m_checkersBoard, m_nextStates);
			m_nextStates.clear();
			m_debugLogFile.LogCheckersBoard(&m_checkersBoard, m_turnCount, m_player1.GetTotalElapsedTime(), m_player2.GetTotalElapsedTime());

			// We only swap players if a multijump isn't available
			if (!isMultiJump)
			{
				SwapPlayers();
			}

			// End of turn increase the turn count
			m_turnCount++;			
			
			// Notify that the player has finished their turn
			m_waitingForPlayerToMakeMove = false;
		}
	}
}

void GameScene::SwapPlayers()
{
	// Swap the checkersboard players	
	if (GetOpponentPlayerType() == PlayerType::GPU)
	{
		// Update the board used by gpu with the current board
		m_gpuOtherCheckersBoard = m_checkersBoard;
		// If we are player 1, then we need to rotate the board
		if (m_isPlayer1)
		{
			m_gpuOtherCheckersBoard.swapPlayers();
		}
	}

	// Update the player 1 state flag and let the selection handler know
	// so it knows who can select pieces
	m_isPlayer1 = !m_isPlayer1;
	m_selectionHandler.SetIsPlayer1(m_isPlayer1);

	m_latestBoardRendered = false;
}

void GameScene::OnSavePressed() const
{
	CheckersBoardFileHandler::Save(m_checkersBoard, m_sceneManager->GetWindow());
}

void GameScene::OnSettingsPressed() const
{
	// Navigate to the game setting screen
	m_sceneManager->SetScene("game_settings");
}

void GameScene::OnResumedPressed()
{
	SetPaused(false);
}

void GameScene::OnExitPressed() const
{
	// Navigate to the menu (home) screen
	m_sceneManager->SetScene("menu");
}

void GameScene::OnGameOver()
{
	// Toggle visiblity
	m_playAgainButton->SetVisibility(m_gameOver && !m_isPaused);
	m_gameOverExitButton->SetVisibility(m_gameOver && !m_isPaused);
	// Toggle active/inactive
	m_playAgainButton->SetActive(m_gameOver && !m_isPaused);
	m_gameOverExitButton->SetActive(m_gameOver && !m_isPaused);

	bool player1Lost = m_checkersBoard.p1NumPieces() <= 0;
	bool player2Lost = m_checkersBoard.p2NumPieces() <= 0;

	if (player1Lost)
	{
		SetGameOverText(L"Player 2 Wins!");
	}
	else if (player2Lost)
	{
		SetGameOverText(L"Player 1 Wins!");
	}
	else
	{
		SetGameOverText(L"Draw!");
	}

	// When the game is over save a dump of the debug information to a log file
	if (m_gameOver && !m_savedDebugLog)
	{
		m_debugLogFile.SaveToFile("data/checkersgame.txt");
		m_savedDebugLog = true;
	}
}

void GameScene::OnSwitchCameraView()
{
	// Change between perspective forward and perspective top-down views
	m_perspView = !m_perspView;
	if (m_perspView)
	{
		m_camera.GetTransform().SetRotation({ 0, 0, 0 });
		m_camera.GetTransform().SetPosition({ 0, 16, -22 });
		m_camera.GetTransform().Rotate({ XMConvertToRadians(35), 0, 0 });
	}
	else
	{
		m_camera.GetTransform().SetPosition({ 0, 25, 0 });
		m_camera.GetTransform().SetRotation({ XMConvertToRadians(90), 0, 0 });
	}	
}

void GameScene::SetPaused(bool paused)
{
	m_isPaused = paused;
	// Set the visibility and 'activeness' of pause buttons
	// Toggle visiblity
	m_saveButton->SetVisibility(m_isPaused);
	m_settingsButton->SetVisibility(m_isPaused);
	m_resumeButton->SetVisibility(m_isPaused);
	m_exitButton->SetVisibility(m_isPaused);
	m_cameraViewButton->SetVisibility(!m_isPaused);
	// Toggle active/inactive
	m_saveButton->SetActive(m_isPaused);
	m_settingsButton->SetActive(m_isPaused);
	m_resumeButton->SetActive(m_isPaused);
	m_exitButton->SetActive(m_isPaused);
	m_cameraViewButton->SetActive(!m_isPaused);
}

void GameScene::SetGameOverText(const std::wstring& text)
{
	auto dwriteFactory = m_guiCanvas->GetDWriteFactory();
	std::wstring fontFamily = GameSettings::Instance().GetFontFamily();

	ComPtr<IDWriteTextFormat> gameOverTextFormat;
	dwriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		44.0f,
		L"en-gb",
		gameOverTextFormat.GetAddressOf());

	gameOverTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);

	dwriteFactory->CreateTextLayout(
		text.c_str(),
		wcslen(text.c_str()),
		gameOverTextFormat.Get(),
		static_cast<float>(m_sprGameOverPanel.GetSize().x),
		100.0f,
		m_gameOverTextLayout.GetAddressOf());
}