#include "Quanta/QRenderer.h"
#include "Quanta/QEvent.h"
#include "Quanta/QUtility.h"

#include "MenuScene.h"
#include "SceneManager.h"
#include "GameScene.h"
#include "TexturePool.h"
#include "MeshPool.h"
#include "GameSettings.h"

using namespace Quanta;
using Microsoft::WRL::ComPtr;

MenuScene::MenuScene(SceneManager* sceneManager)
	: Scene("menu", sceneManager)
	, m_camera({0, 0, -15})
{
	InitGUI();

	m_checkers1Transforms[0].SetPosition({ -5, 0, 0 });
	m_checkers1Transforms[1].SetPosition({ -2, -3, 7 });
	m_checkers1Transforms[2].SetPosition({ -1, -3,  -3 });
	m_checkers1Transforms[3].SetPosition({ 4, 0,  4 });
	m_checkers1Transforms[4].SetPosition({ 5, -4,  -1 });

	m_checkers2Transforms[0].SetPosition({ -4, 0,  12 });
	m_checkers2Transforms[1].SetPosition({ -3, 5,  7 });
	m_checkers2Transforms[2].SetPosition({ 1, 3,  3 });
	m_checkers2Transforms[3].SetPosition({ 6, 3,  -2 });
	m_checkers2Transforms[4].SetPosition({ -5, 3,  -1 });
}

void MenuScene::ProcessEvent(Quanta::QEvent& e)
{
	m_guiCanvas->ProcessEvent(e);

	if (e.type == QEvent::Type::Resized)
	{
		RecalculateSprites();
		QWindow& window = m_sceneManager->GetWindow();
		m_camera.SetAspectRatio(static_cast<float>(window.GetWidth()) / window.GetHeight());
	}
}

void MenuScene::Update(float deltaTime)
{
	float rotation_1 = DirectX::XMConvertToRadians(20.0f * deltaTime);
	float rotation_2 = DirectX::XMConvertToRadians(15.0f * deltaTime);
	float rotation_3 = DirectX::XMConvertToRadians(25.0f * deltaTime);
	m_checkers1Transforms[0].Rotate({ rotation_1, rotation_2, 0.0f });
	m_checkers1Transforms[1].Rotate({ rotation_1, rotation_3, rotation_1 });
	m_checkers1Transforms[2].Rotate({ rotation_3, 0.0f, rotation_2 });
	m_checkers1Transforms[3].Rotate({ 0.0f, rotation_2, rotation_3 });
	m_checkers1Transforms[4].Rotate({ rotation_1, rotation_1, rotation_1 });
	m_checkers2Transforms[0].Rotate({ rotation_2, rotation_2, 0.0f });
	m_checkers2Transforms[1].Rotate({ rotation_3, rotation_3, rotation_1 });
	m_checkers2Transforms[2].Rotate({ 0.0f, rotation_3, rotation_2 });
	m_checkers2Transforms[3].Rotate({ rotation_3, rotation_2, rotation_3 });
	m_checkers2Transforms[4].Rotate({ rotation_2, rotation_1, rotation_1 });

	m_checkers1Transforms[0].Rotate({ 0.0f, 0.0f, rotation_1 }, Space::World);
	m_checkers1Transforms[1].Rotate({ 0.0f, 0.0f, rotation_2 }, Space::World);
	m_checkers1Transforms[2].Rotate({ 0.0f, 0.0f, rotation_3 }, Space::World);
	m_checkers1Transforms[3].Rotate({ 0.0f, 0.0f, rotation_1 }, Space::World);
	m_checkers1Transforms[4].Rotate({ 0.0f, 0.0f, rotation_2 }, Space::World);
	m_checkers2Transforms[0].Rotate({ 0.0f, 0.0f, rotation_3 }, Space::World);
	m_checkers2Transforms[1].Rotate({ 0.0f, 0.0f, rotation_1 }, Space::World);
	m_checkers2Transforms[2].Rotate({ 0.0f, 0.0f, rotation_2 }, Space::World);
	m_checkers2Transforms[3].Rotate({ 0.0f, 0.0f, rotation_3 }, Space::World);
	m_checkers2Transforms[4].Rotate({ 0.0f, 0.0f, rotation_1 }, Space::World);
}

void MenuScene::Render()
{
	QRenderer& renderer = m_sceneManager->GetRenderer();

	renderer.BeginSpritePass(m_camera);
	renderer.DrawSprite(m_sprBackground);
	renderer.EndSpritePass();

	renderer.BeginOpaquePass(m_camera);
	for (int i = 0; i < 5; ++i)
	{
		m_checkersPiece1->GetTransform() = m_checkers1Transforms[i];
		renderer.DrawMesh(*m_checkersPiece1);
	}
	for (int i = 0; i < 5; ++i)
	{
		m_checkersPiece2->GetTransform() = m_checkers2Transforms[i];
		renderer.DrawMesh(*m_checkersPiece2);
	}

	renderer.EndOpaquePass();

	renderer.BeginSpritePass(m_camera);
	renderer.DrawSprite(m_sprTitle);
	renderer.DrawSprite(m_sprMenuPanel);
	m_guiCanvas->Render();
	renderer.EndSpritePass();
}

void MenuScene::OnEnter()
{
	QWindow& window = m_sceneManager->GetWindow();
	m_camera.SetAspectRatio(static_cast<float>(window.GetWidth()) / window.GetHeight());
	RecalculateSprites();
}

void MenuScene::OnGameThemeChanged(GameTheme theme)
{
	UINT32 textColour = 0xff000000;
	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		textColour = 0xff27848c;
	}
	m_playButton->SetTextColour(textColour);
	m_settingsButton->SetTextColour(textColour);
	m_controlsButton->SetTextColour(textColour);
	m_customBoardButton->SetTextColour(textColour);
	m_exitButton->SetTextColour(textColour);
}

void MenuScene::InitGUI()
{
	// Textures
	TexturePool& texturePool = TexturePool::Instance();	

	UINT32 textColour = 0xff000000;
	if (GameSettings::Instance().GetGameTheme() == GameTheme::Theme2)
	{
		textColour = 0xff27848c;
	}

	std::wstring fontFamily = GameSettings::Instance().GetFontFamily();
	m_playButton = m_guiCanvas->AddButton();
	m_playButton->Init(texturePool.GetTexture(TextureID::ButtonNormalIdle), texturePool.GetTexture(TextureID::ButtonNormalMouseOver), L"Play", fontFamily, 34.0f);
	m_playButton->SetCallback(std::bind(&MenuScene::OnPlayPressed, this));
	m_playButton->SetTextColour(textColour);

	m_settingsButton = m_guiCanvas->AddButton();
	m_settingsButton->Init(texturePool.GetTexture(TextureID::ButtonNormalIdle), texturePool.GetTexture(TextureID::ButtonNormalMouseOver), L"Settings", fontFamily, 34.0f);
	m_settingsButton->SetCallback(std::bind(&MenuScene::OnSettingsPressed, this));
	m_settingsButton->SetTextColour(textColour);

	m_controlsButton = m_guiCanvas->AddButton();
	m_controlsButton->Init(texturePool.GetTexture(TextureID::ButtonNormalIdle), texturePool.GetTexture(TextureID::ButtonNormalMouseOver), L"Controls", fontFamily, 34.0f);
	m_controlsButton->SetCallback(std::bind(&MenuScene::OnControlsPressed, this));
	m_controlsButton->SetTextColour(textColour);

	m_customBoardButton = m_guiCanvas->AddButton();
	m_customBoardButton->Init(texturePool.GetTexture(TextureID::ButtonNormalIdle), texturePool.GetTexture(TextureID::ButtonNormalMouseOver), L"Editor", fontFamily, 34.0f);
	m_customBoardButton->SetCallback(std::bind(&MenuScene::OnCustomBoardLayoutPressed, this));
	m_customBoardButton->SetTextColour(textColour);

	m_exitButton = m_guiCanvas->AddButton();
	m_exitButton->Init(texturePool.GetTexture(TextureID::ButtonSmallIdle), texturePool.GetTexture(TextureID::ButtonSmallMouseOver), L"Exit", fontFamily, 34.0f);
	m_exitButton->SetCallback(std::bind(&MenuScene::OnExitPressed, this));
	m_exitButton->SetTextColour(textColour);

	m_sprBackground.SetTexture(texturePool.GetTexture(TextureID::Background));
	m_sprTitle.SetTexture(texturePool.GetTexture(TextureID::Title));
	m_sprMenuPanel.SetTexture(texturePool.GetTexture(TextureID::MenuPanel));

	MeshPool& meshPool = MeshPool::Instance();
	m_checkersPiece1 = meshPool.GetMesh(MeshID::CheckersPiece1);
	m_checkersPiece2 = meshPool.GetMesh(MeshID::CheckersPiece2);	

	RecalculateSprites();
}

void MenuScene::RecalculateSprites()
{
	QWindow& window = m_sceneManager->GetWindow();
	UINT windowWidth = window.GetWidth();
	UINT windowHeight = window.GetHeight();

	m_sprBackground.SetScale({ windowWidth / 512.0f, windowHeight / 512.0f });
	m_sprBackground.SetPosition({ int(windowWidth / 2), int(windowHeight / 2) });

	m_sprTitle.SetPosition({int(windowWidth / 2), 100 });

	m_sprMenuPanel.SetPosition({ int(window.GetWidth() / 2), int(window.GetHeight() * 0.55f) });

	m_playButton->SetPosition(int(m_sprMenuPanel.GetPosition().x), int(m_sprMenuPanel.GetPosition().y - 180));
	m_settingsButton->SetPosition(int(m_sprMenuPanel.GetPosition().x), int(m_playButton->GetPosition().y + m_playButton->GetSize().y + 10));
	m_controlsButton->SetPosition(int(m_sprMenuPanel.GetPosition().x), int(m_settingsButton->GetPosition().y + m_settingsButton->GetSize().y + 10));
	m_customBoardButton->SetPosition(int(m_sprMenuPanel.GetPosition().x), int(m_controlsButton->GetPosition().y + m_controlsButton->GetSize().y + 10));
	m_exitButton->SetPosition(int(m_sprMenuPanel.GetPosition().x), int(m_customBoardButton->GetPosition().y + m_customBoardButton->GetSize().y));
}	

void MenuScene::OnPlayPressed() const
{
	m_sceneManager->SetScene("game_setup");
}

void MenuScene::OnSettingsPressed() const
{
	m_sceneManager->SetScene("game_settings");
}

void MenuScene::OnControlsPressed() const
{
	m_sceneManager->SetScene("controls");
}

void MenuScene::OnCustomBoardLayoutPressed() const
{
	m_sceneManager->SetScene("custom_board");
}

void MenuScene::OnExitPressed()
{
	m_sceneManager->GetWindow().Close();
}