#pragma once

#include <string>
#include "Agents/Common/checkersboard.cpp"

namespace Quanta { class QWindow; }

// The CheckersBoardFileHandler provides static methods to save and
// load to and from .checkersboard files and a CheckersBoard object.
class CheckersBoardFileHandler
{
public:
	CheckersBoardFileHandler();

	// Load a .checkersboard file.
	// checkersboard: The loaded checkers board from the file.
	// outFilename: The filename that was selected from the windows open file window
	// window: The window that is requesting the windows open file window
	static bool Load(CheckersBoard& checkersboard, std::wstring& outFilename, Quanta::QWindow& window);

	// Save a checkers board into a .checkersboard file
	// checkersboard: The checkers board to save.
	// window: The window that is requesting the windows save file window
	static bool Save(const CheckersBoard& checkersboard, Quanta::QWindow& window);
};