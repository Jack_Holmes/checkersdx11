#include "GUICanvas.h"
#include "Quanta/QWindow.h"
#include "Quanta/QRenderer.h"
#include "Quanta/QUtility.h"

using namespace Quanta;
using Microsoft::WRL::ComPtr;

GUICanvas::GUICanvas(Quanta::QWindow* window, Quanta::QRenderer* renderer, ID3D11Device* d3dDevice)
	: m_window(window)
	, m_renderer(renderer)
{
	d3dDevice->GetImmediateContext(m_d3dDeviceContext.GetAddressOf());

	// Fonts
	ComPtr<IFW1Factory> FW1Factory;
	QuantaHR(
		"GUICanvas::GUICanvas",
		"FW1CreateFactory",
		FW1CreateFactory(FW1_VERSION, FW1Factory.GetAddressOf()));

	QuantaHR(
		"GUICanvas::GUICanvas",
		"IFW1Factory::CreateFontWrapper",
		FW1Factory->CreateFontWrapper(d3dDevice, L"Segoe UI", m_fontWrapper.GetAddressOf()));

	QuantaHR(
		"GUICanvas::GUICanvas",
		"IFW1FontWrapper::GetDWriteFactory",
		m_fontWrapper->GetDWriteFactory(m_dwriteFactory.GetAddressOf()));
}

void GUICanvas::ProcessEvent(const Quanta::QEvent& e)
{
	for (auto& button : m_buttons)
	{
		button->ProcessEvent(e);
	}

	for (auto& radioButtonGroup : m_radioButtonGroups)
	{
		radioButtonGroup->ProcessEvent(e);
	}

	for (auto& checkbox : m_checkboxes)
	{
		checkbox->ProcessEvent(e);
	}

	for (auto& slider : m_sliders)
	{
		slider->ProcessEvent(e);
	}
}

void GUICanvas::Render()
{
	for (auto& button : m_buttons)
	{
		button->Render(*m_renderer);
	}

	for (auto& radioButtonGroup : m_radioButtonGroups)
	{
		radioButtonGroup->Render(*m_renderer);
	}

	for (auto& checkbox : m_checkboxes)
	{
		checkbox->Render(*m_renderer);
	}

	for (auto& slider : m_sliders)
	{
		slider->Render(*m_renderer);
	}
}

GUIButton* GUICanvas::AddButton()
{
	m_buttons.push_back(std::make_unique<GUIButton>(this));
	return m_buttons.back().get();
}

GUIRadioButtonGroup* GUICanvas::AddRadioButtonGroup()
{
	m_radioButtonGroups.push_back(std::make_unique<GUIRadioButtonGroup>(this));
	return m_radioButtonGroups.back().get();
}

GUICheckbox* GUICanvas::AddCheckbox()
{
	m_checkboxes.push_back(std::make_unique<GUICheckbox>(this));
	return m_checkboxes.back().get();
}

GUISlider* GUICanvas::AddSlider()
{
	m_sliders.push_back(std::make_unique<GUISlider>(this));
	return m_sliders.back().get();
}

bool GUICanvas::MouseIntersects(const Quanta::QSprite& sprite) const
{
	POINT mousePos = QMouse::GetPosition(m_window->GetHandle());
	auto size = sprite.GetSize();
	int halfSizeX = size.x / 2;
	int halfSizeY = size.y / 2;
	return (mousePos.x > (sprite.GetPosition().x - halfSizeX)
		&& (mousePos.x < sprite.GetPosition().x + halfSizeX)
		&& mousePos.y >(sprite.GetPosition().y - halfSizeY)
		&& (mousePos.y < sprite.GetPosition().y + halfSizeY));
}

void GUICanvas::DrawString(const std::wstring& text, float fontSize, float x, float y, UINT32 colour) const
{
	m_fontWrapper->DrawString(
		m_d3dDeviceContext.Get(),
		text.c_str(),
		fontSize,
		x,
		y,
		colour,
		FW1_RESTORESTATE);
}

void GUICanvas::DrawString(IDWriteTextLayout* textLayout, float originX, float originY, UINT32 colour) const
{
	m_fontWrapper->DrawTextLayout(
		m_d3dDeviceContext.Get(),
		textLayout,
		originX,
		originY,
		colour,
		FW1_RESTORESTATE);
}

IDWriteFactory* GUICanvas::GetDWriteFactory() const
{
	return m_dwriteFactory.Get();
}
