#pragma once

#include <vector>
#include <functional>

#include "CheckersBoardTile.h"

#include "Quanta/QRenderer.h"
#include "Quanta/QEvent.h"
#include "Quanta/QWindow.h"

// The CheckersBoardSelectionHandler managers the piece selection on the 3D
// board that the player has chosen. This includes the rendering of the 
// selection mesh.
class CheckersBoardSelectionHandler
{
public:
	// Construct a CheckersBoardSelectionHandler.
	// window: The application window being rendered to.
	// camera: The same camera that is used when rendering the 3D checkers board.
	CheckersBoardSelectionHandler(Quanta::QWindow* window, Quanta::QCamera* camera);

	// Update positions/animations of the selections.
	void Update(float deltaTime);

	// Redner the selections.
	void Render(Quanta::QRenderer& renderer);

	// Checks for mouse event when player is selecting pieces on the board.
	void ProcessEvent(const Quanta::QEvent& e);

	// Update the selection handler what type of tile is as the specified index.
	// This should be called every time the board layout changes.
	void UpdateTile(int index, CheckersTileType tileType);

	// Set if the current player is player 1. Used to check which pieces
	// are available to select for the current player. This should be called
	// every time the player changes.
	void SetIsPlayer1(bool isPlayer1);

	// Set which index is chosen as the 'selected piece'.
	void SetSourceIndex(int index);

	// Set which index is chosen as the 'destination' from the
	// source index.
	void SetDestinationIndex(int index);

	// Retrive the current source index.
	int GetSourceIndex() const;

	// Retrieve the current destination index.
	int GetDestinationIndex() const;

	// Calcualtes all the possible locations a player can move from the selected source index.
	void MakeMove();

	// Set the callback function which will be invoked once the player has selected their
	// destination.
	void SetMakeMoveCallback(std::function<void()> callback);

	// Checks if the current source is part of a mult-jump (where a player
	// can perform more than 1 capture).
	bool IsMultiJump();

	// Set the state of multi-jump.
	void SetMultiJump(bool multijump);

	// Clear all selections of the checkers board.
	void ClearSelections();

	// Reset the selection handler.
	void Reset();

private:
	// Calculates all the indexes for a simple move
	// which are available based on the specified index
	// for a regular piece and marks the checkers board
	// tiles as selectable.
	void CalculateSimpleMovesForRegularPiece(int index);
	// Calculates all the indexes for a capture move
	// which are available based on the specified index
	// for a regular piece and marks the checkers board
	// tiles as selectable.
	bool CalculateCaptureMovesForRegularPiece(int index);
	// Calculates all the indexes for a simple move
	// which are available based on the specified index
	// for a king piece and marks the checkers board
	// tiles as selectable.
	void CalculateSimpleMovesForKingPiece(int index);
	// Calculates all the indexes for a capture move
	// which are available based on the specified index
	// for a king piece and marks the checkers board
	// tiles as selectable.
	bool CalculateCaptureMovesForKingPiece(int index);	

	// Store all the checker board tiles which represent
	// their state of selection.
	std::vector<CheckersBoardTile> m_checkersBoardTiles;
	// The application window.
	Quanta::QWindow* m_window;
	// The camera used when drawing the 3D checkers board.
	Quanta::QCamera* m_camera;
	// Tracks state of who is able to select pieces.
	bool m_isPlayer1;
	// The source index (selected peice to move).
	int m_sourceIndex;
	// The destination index (selected location to move to).
	int m_destinationIndex;
	// The callback which is invoked when a player decides a destination.
	std::function<void()> m_makeMoveCallback;
	// Tracks the state of multi-jump so we can decide who is selecting peices
	// after a capture move has been made.
	bool m_inMultiJumpState;
};