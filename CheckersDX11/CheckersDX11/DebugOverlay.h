#pragma once

#include <string>
#include <dwrite.h>
#include <wrl/client.h>

#include "Quanta/QEvent.h"
#include "Quanta/QRenderer.h"
#include "Quanta/QWindow.h"
#include "Quanta/QSprite.h"

#include "Agents/Common/checkersboard.cpp"
#include "Agents/Sequential/CheckersSearch.hpp"

#include "CheckersPlayer.h"
#include "CheckersTile2D.h"
#include "GUICanvas.h"

// The DebugOverlay is a graphical overlay as part of the game scene
// which display useful information for testing.
class DebugOverlay
{
public:
	// Construct a debug overlay.
	// window: Window to which the overlay is drawn to.
	// guiCanvas: The canvas that belongs to the scene.
	DebugOverlay(Quanta::QWindow* window, GUICanvas* guiCanvas);

	// Process windows events.
	void ProcessEvent(Quanta::QEvent& e);
	// Update the overlay with the current board and the boards AI considered. 
	// Note this should be called every turn.
	void Update(int turn, const CheckersBoard& selectedBoard, const NextStates& nextStates);
	// Render the debug overlay.
	void Render(Quanta::QRenderer& renderer);

	// Reset the overlay. This should be called every time the game
	// is reset.
	// p1: The checkers game player 1.
	// p2: The checkers game player 2.
	void Reset(CheckersPlayer* p1, CheckersPlayer* p2);

private:
	// Calculate the positions of the debug overlay elements.
	void RecalculatePositions();

	// Set the text that is displayed in the debug panel.
	void SetDebugText(const std::wstring& text);

	void SetScoreText(const std::wstring& text);

	// Initialise the 2D checkers tiles that are used for
	// representing board layouts.
	void InitCheckersTiles();

	// Map the checkers board to the 2D checkers tiles. Should
	// be called every time there is a new board layout to display.
	void MapFromCheckersBoard(const CheckersBoard& board);

	// Sets the sprite of a 2D tile based on its current tile type.
	void SetSpriteTextureByTileType(CheckersTileType tileType, Quanta::QSprite& sprite) const;

	// Calculate the tile positions of the 2d checkers board. This is
	// offset by the m_boardOffsetY and m_boardOffsetX when drawing 
	// multiple boards.
	void CalculateTilePositions();

	// The application window.
	Quanta::QWindow* m_window;

	// The gui canvas that belongs to the game scene.
	GUICanvas*                                m_guiCanvas;
	// The text layout for the debug text panel.
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_debugTextLayout;
	Microsoft::WRL::ComPtr<IDWriteTextLayout> m_scoreTextLayout;

	// A sprite background used for the debug text panel.
	Quanta::QSprite m_sprTextDebugPanel;
	// A sprite which is drawn behind the "selected" checkers board,
	// to show that it was chosen by the AI.
	Quanta::QSprite m_sprSelected;

	// Players
	CheckersPlayer* m_player1;
	CheckersPlayer* m_player2;

	// String that shows how many times player 1 was slower than player 2.
	std::wstring m_timeDiffTextHi;
	// String that shows how many times player 1 was faster than player 2.
	std::wstring m_timeDiffTextLo;
	// String representation of player 1 type.
	std::wstring m_strPlayer1Type;
	// String representation of player 2 type.
	std::wstring m_strPlayer2Type;

	// Stores the number of times player 1 was slower than player 2.
	int m_player1HiCount;
	// Stores the number of times player 1 was faster than player 2.
	int m_player1LoCount;

	// List of 2D checkers tiles that are drawn to the debug overlay.
	std::vector<CheckersTile2D> m_checkersTiles;
	// A copy of the current selected board so it can be 
	// draw to the overlay.
	CheckersBoard m_selectedBoard;
	// A copy of the considered boards so they can be drawn to 
	// the debug overlay.
	NextStates m_nextStates;

	// Controls the y-offset in pixels. Used when drawing 
	// multiple boards.
	int m_boardOffsetY;
	// Controls the x-offset in pixels. Used when drawing 
	// multiple boards.
	int m_boardOffsetX;
};
