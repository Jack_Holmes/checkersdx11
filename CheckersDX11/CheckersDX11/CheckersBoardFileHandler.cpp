#include <vector>
#include <fstream>
#include <sstream>
#include <Shlwapi.h>
#include <locale>
#include <codecvt>
#include <direct.h>

#include "CheckersBoardFileHandler.h"

#include "Quanta/QWindow.h"

CheckersBoardFileHandler::CheckersBoardFileHandler()
{
}

bool CheckersBoardFileHandler::Load(CheckersBoard& checkersboard, std::wstring& outFilename, Quanta::QWindow& window)
{
	

	OPENFILENAMEA openFileName;
	char szFileName[MAX_PATH] = "";
	ZeroMemory(&openFileName, sizeof(openFileName));

	// Filter the open file request to .checkersboard
	openFileName.lStructSize = sizeof(openFileName);
	openFileName.lpstrFilter = "Checkers Board File (*.checkersboard)\0*.*\0";
	openFileName.lpstrFile = szFileName;
	openFileName.hwndOwner = window.GetHandle();
	openFileName.nMaxFile = MAX_PATH;
	openFileName.Flags = OFN_EXPLORER;
	openFileName.lpstrDefExt = "checkersboard";

	if (GetOpenFileName(&openFileName))
	{
		char* ext = PathFindExtension(szFileName);
		if (strcmp(ext, ".checkersboard") == 0)
		{
			std::ifstream ifs(szFileName);

			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
			outFilename = converter.from_bytes(szFileName);

			// Remove extention
			size_t lastdot = outFilename.find_last_of(L".");
			if (lastdot != std::string::npos)
			{
				outFilename = outFilename.substr(0, lastdot);
			}

			// Remove directory
			size_t lastbackslash = outFilename.find_last_of(L"\\");
			if (lastbackslash != std::string::npos
				&& (lastbackslash + 1) != std::string::npos)
			{
				outFilename = outFilename.substr(lastbackslash+1, outFilename.back());
			}

			if (!ifs.good())
			{
				return false;
			}

			// Load the file
			std::vector<std::string> fileContents;
			std::string line;
			while (std::getline(ifs, line))
			{
				fileContents.push_back(line);
			}

			// Construct checkersboard from the file
			checkersboard.clear();
			for(auto& item : fileContents)
			{
				int index, typeId;
				std::istringstream iss(item);
				iss >> index >> typeId;

				switch (typeId)
				{
				case 1: checkersboard.addP1Man(index);
					break;
				case 2: checkersboard.addP2Man(index);
					break;
				case 3: checkersboard.addP1King(index);
					break;
				case 4: checkersboard.addP2King(index);
					break;
				default:
					break;
				}
			}

			return true;
		}
	}

	return false;
}

bool CheckersBoardFileHandler::Save(const CheckersBoard& checkersboard, Quanta::QWindow& window)
{
	OPENFILENAMEA openFileName;
	char szFileName[MAX_PATH] = "";
	ZeroMemory(&openFileName, sizeof(openFileName));

	// Filter the open file request to .checkersboard
	openFileName.lStructSize = sizeof(openFileName);
	openFileName.lpstrFilter = "Checkers Board File (*.checkersboard)\0*.*\0";
	openFileName.lpstrFile = szFileName;
	openFileName.hwndOwner = window.GetHandle();
	openFileName.nMaxFile = MAX_PATH;
	openFileName.Flags = OFN_EXPLORER;
	openFileName.lpstrDefExt = "checkersboard";

	if (GetSaveFileName(&openFileName))
	{
		// Ensure we have the correct extension
		char* ext = PathFindExtension(szFileName);
		std::string fileToSave = szFileName;
		if (strcmp(ext, ".checkersboard") != 0)
		{
			size_t lastdot = fileToSave.find_last_of(".");
			if (lastdot != std::string::npos)
			{
				fileToSave = fileToSave.substr(0, lastdot);
				fileToSave += ".checkersboard";
			}
		}

		// Save the checkers board to the file
		std::ofstream ofs(fileToSave);
		for (int index = 0; index < 64; ++index)
		{
			bool p1man, p1king, p2man, p2king;
			p1man = checkersboard.p1Man(index);
			p1king = checkersboard.p1King(index);
			p2man = checkersboard.p2Man(index);
			p2king = checkersboard.p2King(index);

			int typeId = 0;

			if (p1man)
			{
				typeId = 1;
			}
			else if (p2man)
			{
				typeId = 2;
			}
			else if (p1king)
			{
				typeId = 3;
			}
			else if (p2king)
			{
				typeId = 4;
			}

			ofs << index << " " << typeId << "\n";
		}

		ofs.close();

		return true;
	}

	return false;
}