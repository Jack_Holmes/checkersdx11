#pragma once

#include <unordered_map>
#include <memory>
#include <string>
#include <d3d11.h>
#include <wrl/client.h>

#include "Quanta/QMesh.h"
#include "MeshIDs.h"

// Forward declarations
class QTexture;

// Map that holds the ids and meshes managed by the mesh pool.
using MeshContainer = std::unordered_map<int, std::unique_ptr<Quanta::QMesh>>;

// The MeshPool singleton resource managers mesh assets.
class MeshPool
{
public:
	// Retrieve the mesh pool static instance.
	static MeshPool& Instance()
	{
		static MeshPool instance;
		return instance;
	}

	// Load a .obj file into a QMesh and store it in the object pool. Optional texture.
	Quanta::QMesh* Load(ID3D11Device* d3dDevice, int id, const std::wstring& objFile, Quanta::QTexture* texture = nullptr) const;

	// Retreive a pointer to a mesh stored in the mesh pool.
	static Quanta::QMesh* GetMesh(int id);

	// Prevent copying the mesh pool.
	MeshPool(const MeshPool&) = delete;
	void operator=(const MeshPool&) = delete;

private:
	MeshPool() {};

	// Add id mesh pair to the meshes container.
	static void Add(int id, std::unique_ptr<Quanta::QMesh> mesh);

	// Container holding all the meshes.
	static MeshContainer s_meshes;
};