#pragma once

#include <DirectXMath.h>

#include "Quanta/QWindow.h"
#include "Quanta/QCamera.h"

namespace Quanta
{
	class QRay
	{
	public:
		QRay();
		QRay(const DirectX::XMVECTOR& origin, const DirectX::XMVECTOR& direction);

		void SetOrigin(const DirectX::XMVECTOR& origin);

		void SetDirection(const DirectX::XMVECTOR& direction);

		const DirectX::XMVECTOR& GetOrigin() const;

		const DirectX::XMVECTOR& GetDirection() const;

	private:
		DirectX::XMVECTOR m_origin;
		DirectX::XMVECTOR m_direction;
	};

	QRay GetScreenPickingRay(int screenX, int screenY, const QWindow& window, QCamera& camera);
	QRay GetMousePickingRay(const QWindow& window, QCamera& camera);
}