#pragma once

#include <DirectXMath.h>

namespace Quanta
{

	enum class Space
	{
		Local, World
	};

	class QTransform
	{
	public:
		QTransform();

		void SetPosition(const DirectX::XMFLOAT3& position);
		void SetRotation(const DirectX::XMFLOAT3& eulerAngles);
		void SetRotation(const DirectX::XMFLOAT4& quaternion);				
		void SetScale(const DirectX::XMFLOAT3& scale);

		void Translate(const DirectX::XMFLOAT3& translation, Space space = Space::Local);
		void Rotate(const DirectX::XMFLOAT3& eulerAngles, Space space = Space::Local);
		void Rotate(const DirectX::XMFLOAT4& quaternion, Space space = Space::Local);
		void LookAt(const DirectX::XMFLOAT3& position);
		void Scale(const DirectX::XMFLOAT3& scale);

		const DirectX::XMFLOAT3& GetPosition() const;
		const DirectX::XMFLOAT4& GetRotation() const;
		const DirectX::XMFLOAT3& GetScale() const;

		const DirectX::XMFLOAT4X4& GetWorldMatrix();

	private:
		void UpdateWorldMatrix();

		DirectX::XMFLOAT3 m_position;
		DirectX::XMFLOAT4 m_quaternion;
		DirectX::XMFLOAT4 m_worldQuaternion;
		DirectX::XMFLOAT3 m_scale;

		DirectX::XMFLOAT4X4 m_worldMatrix;
		bool                m_worldMatrixDirty;

	};
}