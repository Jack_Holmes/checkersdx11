#pragma once

#include <Windows.h>

namespace Quanta
{
	class QWindow;

	class QMouse
	{
	public:
		enum class Button
		{
			Left,
			Right,
			Middle,
			XButton1,
			XButton2
		};

		static bool IsButtonPressed(Button button);

		static POINT GetPosition();

		static POINT GetPosition(HWND relativeTo);

		static void SetPosition(POINT position);

		static void SetPosition(POINT position, HWND relativeTo);
	};
}