#pragma once

#include <DirectXMath.h>
#include <basetsd.h>

#include "Quanta/QTypes.h"

namespace Quanta
{
	enum class QLightType : UINT32
	{
		Directional = 0,
		Point,
		Spot
	};

	struct QLight
	{
		DirectX::XMFLOAT4   colour;	     // All		
		DirectX::XMFLOAT3   position;    // Point/Spot only 
		QLightType          type;        // All
		DirectX::XMFLOAT3   direction;   // Directional/Spot only
		float               range;       // Point/Spot only
		DirectX::XMFLOAT3   attenuation; // Point/Spot only
		float               falloff;     // Spot only
	};
}