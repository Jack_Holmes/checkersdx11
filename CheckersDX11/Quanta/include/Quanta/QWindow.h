#pragma once

#include "Quanta/QEvent.h"
#include <d3d11_1.h>
#include <DirectXColors.h>
#include <Windows.h>
#include <wrl/client.h>
#include <string>
#include <queue>
#include <memory>

#define QUANTA_DEF_WIN_STYLE 0x00CB0000L // (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX)

namespace Quanta
{
	class QRenderer;

	class QWindow
	{
	public:
		QWindow(Microsoft::WRL::ComPtr<ID3D11DeviceContext> deviceContext, const std::string& windowTitle,
			UINT width, UINT height, DWORD style = QUANTA_DEF_WIN_STYLE);
		~QWindow();

		void Create(QRenderer* renderer, bool fullscreen = false);

		void SetSize(UINT width, UINT height);
		void SetTitle(const std::string& title) const;
		void SetVisible(bool visible) const;
		void SetCursorVisible(bool visible);
		bool SetCursorImage(const std::string& file);
		bool SetIcon(const std::string& file);
		void SetKeyRepeat(bool enabled);
		bool SetFullscreen(bool enabled);

		bool PollEvent(QEvent& event);
		bool IsOpen() const;
		bool IsMinimised() const;
		void Close();

		HWND GetHandle() const;

		UINT GetWidth() const;
		UINT GetHeight() const;

		Microsoft::WRL::ComPtr<IDXGISwapChain> GetSwapChain() const;

		std::vector<DXGI_MODE_DESC> GetDisplayModes() const;

	private:
		void ProcessMessage(UINT message, WPARAM wParam, LPARAM lParam);
		static LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

		void InitialiseAdapter(Microsoft::WRL::ComPtr<ID3D11Device>& device);
		void InitialiseDeviceInfo();
		void InitialiseDisplayModes(DXGI_FORMAT format);
		void RequestDisplayMode(UINT width, UINT height);

		void CreateSwapChain();

		static QKeyboard::Key MapVirtualKeyToEventKeyCode(WPARAM key, LPARAM flags);
		void SetMouseTracking(bool enable) const;

		QRenderer* m_renderer;

		HWND    m_handle;
		std::string m_windowTitle;
		HCURSOR m_cursor;
		HCURSOR m_lastCursorImage;
		HICON   m_icon;
		DWORD   m_style;

		bool m_isOpen;
		bool m_keyRepeat;
		bool m_minimised;
		bool m_resizing;
		bool m_mouseInClientArea;

		// Adapter info.
		int          m_adapterMemory;
		std::wstring m_adapterDescription;

		// Display modes.
		std::unique_ptr<DXGI_MODE_DESC[]> m_displayModes;
		DXGI_MODE_DESC                    m_displayMode;
		UINT                              m_numDisplayModes;

		// Back buffer properties.
		UINT        m_backBufferWidth;
		UINT        m_backBufferHeight;
		DXGI_FORMAT m_backBufferFormat;

		UINT m_screenResolutionX;
		UINT m_screenResolutionY;
		UINT m_lastWindowWidth;
		UINT m_lastWindowHeight;
		bool m_fullscreen;

		// Holds queue of unpolled events.
		std::queue<QEvent> m_events;

		// Device resources.
		Microsoft::WRL::ComPtr<ID3D11DeviceContext> m_deviceContext;
		Microsoft::WRL::ComPtr<IDXGISwapChain>      m_swapChain;
		Microsoft::WRL::ComPtr<IDXGISwapChain1>     m_swapChain1;
		Microsoft::WRL::ComPtr<IDXGIAdapter>        m_adapter;
		Microsoft::WRL::ComPtr<IDXGIOutput>         m_output;		
	};
}
