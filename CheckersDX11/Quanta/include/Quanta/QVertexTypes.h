#pragma once

#include <d3d11_1.h>
#include <DirectXMath.h>

namespace Quanta
{
	struct QVertexPosition
	{
		QVertexPosition()
			: pos(0.0f, 0.0f, 0.0f)
		{
		}

		QVertexPosition(const DirectX::XMFLOAT3& position)
			: pos(position)
		{
		}

		DirectX::XMFLOAT3 pos;

		static const int NumElements = 1;
		static const D3D11_INPUT_ELEMENT_DESC InputElements[NumElements];
	};

	struct QVertexPositionColour
	{
		QVertexPositionColour()
			: pos(0.0f, 0.0f, 0.0f)
			, col(0.0f, 0.0f, 0.0f, 1.0f)
		{
		}

		QVertexPositionColour(const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT4& colour)
			: pos(position)
			, col(colour)
		{
		}

		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT4 col;

		static const int NumElements = 2;
		static const D3D11_INPUT_ELEMENT_DESC InputElements[NumElements];
	};

	struct QVertexPositionTexture
	{
		QVertexPositionTexture()
			: pos(0.0f, 0.0f, 0.0f)
			, texCoord(0.0f, 0.0f)
		{
		}

		QVertexPositionTexture(const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT2& texCoordinates)
			: pos(position)
			, texCoord(texCoordinates)
		{
		}

		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT2 texCoord;

		static const int NumElements = 2;
		static const D3D11_INPUT_ELEMENT_DESC InputElements[NumElements];
	};

	struct QVertexPositionTextureNormal
	{
		QVertexPositionTextureNormal()
			: pos(0.0f, 0.0f, 0.0f)
			, texCoord(0.0f, 0.0f)
			, norm(0.0f, 0.0f, 0.0f)
		{
		}

		QVertexPositionTextureNormal(const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT2& texCoordinates, const DirectX::XMFLOAT3& normal)
			: pos(position)
			, texCoord(texCoordinates)
			, norm(normal)
		{
		}

		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT2 texCoord;
		DirectX::XMFLOAT3 norm;

		static const int NumElements = 3;
		static const D3D11_INPUT_ELEMENT_DESC InputElements[NumElements];
	};
}