#pragma once

#include <DirectXMath.h>

namespace Quanta
{
	class QTexture;

	class QSprite
	{
	public:
		QSprite();

		void SetTexture(QTexture* texture);

		// Pixel coordinates. Note the origin of a sprite is at its centre.
		void SetPosition(const DirectX::XMINT2& position);

		void SetScale(const DirectX::XMFLOAT2& scale);

		void SetColour(const DirectX::XMFLOAT4& colour);		

		QTexture& GetTexure() const;

		const DirectX::XMINT2& GetPosition() const;

		const DirectX::XMFLOAT2& GetScale() const;

		const DirectX::XMFLOAT4& GetColour() const;

		DirectX::XMUINT2 GetSize() const;

	private:		
		QTexture* m_texture;

		DirectX::XMINT2 m_position;
		DirectX::XMFLOAT2 m_scale;
		DirectX::XMFLOAT4 m_colour;
	};
}
