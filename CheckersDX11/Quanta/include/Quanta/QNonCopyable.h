#pragma once

namespace Quanta
{
	class QNonCopyable
	{
	public:
		QNonCopyable() {};
		virtual ~QNonCopyable() {};
		QNonCopyable(const QNonCopyable&) = delete;
		QNonCopyable& operator=(const QNonCopyable&) = delete;
	};
}
