#pragma once

#include "Quanta/QKeyboard.h"
#include "Quanta/QMouse.h"
#include <cstdint>

namespace Quanta
{
	class QEvent
	{
	public:

		enum class Type
		{
			Closed,
			Resized,
			KeyPressed,
			KeyReleased,
			MouseButtonPressed,
			MouseButtonReleased,
			MouseButtonDoubleClick,
			MouseWheelMoved,
			MouseMoved,
			MouseEntered,
			MouseLeft,
			TextEntered,
		};

		Type type;

		struct SizeData
		{
			unsigned int width;
			unsigned int height;
		};

		struct KeyboardData
		{
			QKeyboard::Key key;
			bool           alt;
			bool           control;
			bool           shift;
			bool           system;
		};

		struct MouseData
		{
			int16_t        x;
			int16_t        y;
			QMouse::Button button;
			int            wheelDelta;
		};

		struct TextData
		{
			uint32_t unicode;
		};

		union
		{
			SizeData     size;
			KeyboardData keyboard;
			TextData     text;
			MouseData    mouse;
		};
	};
}
