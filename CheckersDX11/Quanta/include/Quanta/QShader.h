#pragma once

#include <d3d11.h>
#include <wrl/client.h>
#include <string>

namespace Quanta
{
	class QShader
	{
	public:
		QShader(const std::string& name, ID3D11VertexShader* vertexShader, ID3D11PixelShader* pixelShader, ID3D11InputLayout* inputLayout, UINT stride);

		const std::string& GetName() const;

		ID3D11VertexShader* GetVertexShader() const;

		ID3D11PixelShader* GetPixelShader() const;

		ID3D11InputLayout* GetInputLayout() const;

		UINT GetStride() const;

	private:
		std::string m_name;
		Microsoft::WRL::ComPtr<ID3D11VertexShader> m_vertexShader;
		Microsoft::WRL::ComPtr<ID3D11PixelShader> m_pixelShader;
		Microsoft::WRL::ComPtr<ID3D11InputLayout> m_inputLayout;
		UINT m_stride;
	};
}
