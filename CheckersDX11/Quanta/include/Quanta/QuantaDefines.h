#pragma once

#include <sstream>
#include <fstream>
#include <comdef.h>
#include <windows.h>

#define QUANTA_LINK_LIBRARY(libname) __pragma(comment(lib, #libname ".lib"))

#define QUANTA_ALIGN_16 __declspec(align(16))
#define QUANTA_ALIGN_16_NEW_DELETE void*operator new(size_t i){return _aligned_malloc(i, 16);} void operator delete(void*p){_aligned_free(p);}

#define QUANTA_DBG_OUT(msg)               \
{                                         \
   std::ostringstream os;                 \
   os << msg;                             \
   OutputDebugString( os.str().c_str() ); \
}

#define QUANTA_WARN(msg) QUANTA_DBG_OUT("---- Quanta Warning: " << msg << " ----\n")

#define QUANTA_MAX_NUM_LIGHTS 8
#define QUANTA_SHADOW_MAP_SIZE 2048