#pragma once

#include <d3d11_1.h>
#include <DirectXMath.h>
#include <wrl/client.h>
#include <functional>

#include "Quanta/QNonCopyable.h"
#include "Quanta/QShaderLibrary.h"
#include "Quanta/QMaterial.h"
#include "Quanta/QWindow.h"
#include "Quanta/QRenderTexture.h"
#include "Quanta/QLight.h"

namespace Quanta
{
	class QMesh;
	class QSprite;
	class QShader;
	class QCamera;

	class QRenderer : public QNonCopyable
	{
	public:
		QRenderer(ID3D11Device* device, QWindow* window);

		void Clear(const FLOAT* colorRGBA = DirectX::Colors::DarkSlateGray) const;
		void Display() const;

		void BeginOpaquePass(QCamera& camera, QShader* shader = nullptr, std::function<void()> updateShader = nullptr);
		void EndOpaquePass();		

		void BeginSpritePass(QCamera& camera);		
		void EndSpritePass();

		void BeginShadowPass();
		void EndShadowPass();

		void Draw(
			D3D11_PRIMITIVE_TOPOLOGY topology,
			QCamera& camera,
			ID3D11Buffer** vertexBuffer,
			UINT vertexCount,
			const DirectX::XMMATRIX& worldMatrix,
			QShader* shader);

		void Draw(
			D3D11_PRIMITIVE_TOPOLOGY topology,
			ID3D11Buffer** vertexBuffer,
			ID3D11Buffer* indexBuffer,
			UINT indexCount,
			ID3D11ShaderResourceView* shaderResourceView,
			const DirectX::XMMATRIX& worldMatrix,
			const QMaterial& material,
			std::function<void()> updateShader = nullptr);

		void DrawSprite(QSprite& sprite);

		void DrawMesh(QMesh& mesh);

		QLight* AddLight(const QLight& light);

		void SetGlobalAmbient(const DirectX::XMFLOAT4& globalAmbient);

		QShaderLibrary& GetShaderLibrary() const;

		void PreCreateSwapChain();
		void PostCreateSwapChain(UINT width, UINT height, UINT sampleDescCount, UINT sampleDescQuality);

		void SetVSyncEnabled(bool enabled);

	private:
		void CreateConstantBuffers();

		void CreateSamplerStates();

		void CreateBlendStates();

		void CreateDepthStencilStates();

		void CreateShaders();

		void CreateShadowMap();

		QWindow* m_window;

		Microsoft::WRL::ComPtr<IDXGISwapChain> m_swapChain;

		Microsoft::WRL::ComPtr<ID3D11Device> m_d3dDevice;
		Microsoft::WRL::ComPtr<ID3D11DeviceContext> m_d3dDeviceContext;
		
		Microsoft::WRL::ComPtr<ID3D11RenderTargetView> m_renderTargetView;
		Microsoft::WRL::ComPtr<ID3D11DepthStencilView> m_depthStencilView;
		Microsoft::WRL::ComPtr<ID3D11DepthStencilState> m_depthStencilStateStandard;
		Microsoft::WRL::ComPtr<ID3D11DepthStencilState> m_depthStencilStateNoDepth;
		Microsoft::WRL::ComPtr<ID3D11RasterizerState>  m_rasterizerState;

		CD3D11_VIEWPORT m_viewport;

		Microsoft::WRL::ComPtr<ID3D11Buffer> m_cbVSPerObject;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_cbPSPerObject;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_cbPSPerFrame;

		Microsoft::WRL::ComPtr<ID3D11Buffer> m_cbVSPerObjectSprite;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_cbPSPerObjectSprite;
		
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_cbVSPerObjectDepth;

		Microsoft::WRL::ComPtr<ID3D11SamplerState> m_wrapSamplerState;
		Microsoft::WRL::ComPtr<ID3D11SamplerState> m_shadowSamplerState;

		std::unique_ptr<QRenderTexture> m_shadowMap;
		DirectX::XMFLOAT4X4 m_mainLightViewMatrix;
		DirectX::XMFLOAT4X4 m_mainLightProjMatrix;

		Microsoft::WRL::ComPtr<ID3D11BlendState> m_defaultBlendState;
		Microsoft::WRL::ComPtr<ID3D11BlendState> m_alphaBlendState;

		std::unique_ptr<QShaderLibrary> m_shaderLibrary;
		QShader* m_activeShader;
		QShader* m_defaultShader;
		QShader* m_spriteShader;
		QShader* m_depthShader;

		QCamera* m_currentCamera;

		DirectX::XMFLOAT4 m_globalAmbient;

		std::vector<QLight> m_lights;

		int m_nextFreeLightSlot;

		bool m_vsync;
	};
}
