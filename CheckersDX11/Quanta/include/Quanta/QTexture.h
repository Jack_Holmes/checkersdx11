#pragma once

#include <vector>
#include <d3d11_1.h>
#include <wrl/client.h>
#include <DirectXMath.h>

namespace Quanta
{
	class QTexture
	{
	public:
		QTexture();
	
		void LoadDDSFromFile(ID3D11Device* device, const std::wstring& file);

		Microsoft::WRL::ComPtr<ID3D11Buffer> GetVertexBuffer() const;

		Microsoft::WRL::ComPtr<ID3D11Buffer> GetIndexBuffer() const;

		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> GetShaderResourceView() const;

		const DirectX::XMUINT2& GetSize() const;

	private:
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_indexBuffer;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_shaderResourceView;

		DirectX::XMUINT2 m_size;
	};
}
