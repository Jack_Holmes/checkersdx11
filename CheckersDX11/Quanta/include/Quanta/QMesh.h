#pragma once

#include <wrl/client.h>
#include <d3d11_1.h>
#include <string>

#include "Quanta/QuantaDefines.h"
#include "Quanta/QRenderer.h"
#include "Quanta/QTransform.h"
#include "Quanta/QTexture.h"
#include "Quanta/QMaterial.h"

namespace Quanta
{
	enum class QMeshFileType
	{
		Obj
	};

	QUANTA_ALIGN_16 class QMesh
	{
	public:
		QMesh();

		QUANTA_ALIGN_16_NEW_DELETE

		// Ensure the .obj file has been exported with its up vector as +Y and its forward vector as +Z
		void Load(ID3D11Device* device, const std::wstring& file, QTexture* texture = nullptr, QMeshFileType type=QMeshFileType::Obj);

		void SetTexture(QTexture* texture);

		Microsoft::WRL::ComPtr<ID3D11Buffer> GetVertexBuffer() const;

		Microsoft::WRL::ComPtr<ID3D11Buffer> GetIndexBuffer() const;

		QTexture* GetTexture() const;

		UINT GetIndexCount() const;

		bool HasTexCoords() const;

		bool HasNormals() const;

		QTransform& GetTransform();

		QMaterial& GetMaterial();
		
	private:
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_indexBuffer;

		QTexture* m_texture;

		UINT m_indexCount;
		bool m_hasTexCoords;
		bool m_hasNormals;

		QTransform m_transform;
		QMaterial  m_material;
	};
}