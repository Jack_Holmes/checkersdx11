#pragma once

#include <Windows.h>
#include <exception>
#include <d3d11_1.h>
#include <DirectXMath.h>
#include <wrl/client.h>
#include <comdef.h>
#include <fstream>

namespace Quanta
{
	inline void ThrowIfFailed(HRESULT hr)
	{
		if (FAILED(hr))
		{
			throw std::exception();
		}
	}

	inline void QuantaHR(
		const char* caller,
		const char* HRFunction,
		HRESULT hr)
	{
		if (FAILED(hr))                                        
		{                                                      
			std::ofstream file("quanta_log.txt", std::ios_base::out | std::ios_base::app);
			if (!file.good())
				return;

			_com_error err(hr);
			LPCTSTR errMsg = err.ErrorMessage();
			file << caller << " FAILURE\nInvocation: " << HRFunction << "\nError Message: " << errMsg << "\n";
			file.close();
		}
	} 
	
	inline void QuantaLogToFile(const char* text)
	{
		std::ofstream file("quanta_log.txt", std::ios_base::out | std::ios_base::app);
		if (!file.good())
			return;

		file << text << "\n";
		file.close();
	}

	template <typename VertexType>
	Microsoft::WRL::ComPtr<ID3D11Buffer> CreateVertexBuffer(ID3D11Device* device, UINT vertexCount, const void* initialData, bool dynamic = false)
	{
		D3D11_BUFFER_DESC bufferDesc;
		ZeroMemory(&bufferDesc, sizeof(bufferDesc));
		bufferDesc.Usage = dynamic ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT;
		bufferDesc.ByteWidth = sizeof(VertexType) * vertexCount;
		bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufferDesc.CPUAccessFlags = dynamic ? D3D11_CPU_ACCESS_WRITE : 0;

		D3D11_SUBRESOURCE_DATA subResourceData;
		ZeroMemory(&subResourceData, sizeof(subResourceData));
		subResourceData.pSysMem = initialData;

		Microsoft::WRL::ComPtr<ID3D11Buffer> vertexBuffer;

		QuantaHR(
			"QUtility::CreateVertexBuffer",
			"ID3D11Device::CreateBuffer",
			device->CreateBuffer(&bufferDesc, &subResourceData, vertexBuffer.GetAddressOf()));

		return vertexBuffer;
	}

	template <typename IndexType = WORD>
	Microsoft::WRL::ComPtr<ID3D11Buffer> CreateIndexBuffer(ID3D11Device* device, UINT indexCount, const void* initialData, bool dynamic = false)
	{
		D3D11_BUFFER_DESC bufferDesc;
		ZeroMemory(&bufferDesc, sizeof(bufferDesc));
		bufferDesc.Usage = dynamic ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT;
		bufferDesc.ByteWidth = sizeof(IndexType) * indexCount;
		bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bufferDesc.CPUAccessFlags = dynamic ? D3D11_CPU_ACCESS_WRITE : 0;

		D3D11_SUBRESOURCE_DATA subResourceData;
		ZeroMemory(&subResourceData, sizeof(subResourceData));
		subResourceData.pSysMem = initialData;

		Microsoft::WRL::ComPtr<ID3D11Buffer> indexBuffer;

		QuantaHR(
			"QUtility::CreateIndexBuffer",
			"ID3D11Device::CreateBuffer",
			device->CreateBuffer(&bufferDesc, &subResourceData, indexBuffer.GetAddressOf()));

		return indexBuffer;
	}

	template <typename ConstantBufferType>
	Microsoft::WRL::ComPtr<ID3D11Buffer> CreateConstantBuffer(ID3D11Device* device, D3D11_USAGE usage, UINT CPUAccessFlags)
	{
		D3D11_BUFFER_DESC bufferDesc;
		ZeroMemory(&bufferDesc, sizeof(bufferDesc));
		bufferDesc.Usage = usage;
		bufferDesc.ByteWidth = sizeof(ConstantBufferType);
		bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bufferDesc.CPUAccessFlags = CPUAccessFlags;

		Microsoft::WRL::ComPtr<ID3D11Buffer> constantBuffer;

		QuantaHR(
			"QUtility::CreateConstantBuffer",
			"ID3D11Device::CreateBuffer",
			device->CreateBuffer(&bufferDesc, nullptr, constantBuffer.GetAddressOf()));

		return constantBuffer;
	}
}
