#pragma once

#include <DirectXMath.h>
#include <Quanta/QuantaDefines.h>

namespace Quanta
{
	QUANTA_ALIGN_16 class QMaterial
	{
	public:
		DirectX::XMFLOAT4 ambient = {0.01f, 0.01f, 0.01f, 0.0f};
		DirectX::XMFLOAT4 diffuse = {1.0f, 1.0f, 1.0f, 0.0f};
		DirectX::XMFLOAT4 specular = {0.1f, 0.1f, 0.1f, 0.0f};
		float specularExponent = 30.0f;
	};
}