#pragma once

#include <DirectXMath.h>

#include "Quanta/QMaterial.h"
#include "Quanta/QuantaDefines.h"
#include "Quanta/QTypes.h"
#include "Quanta/QLight.h"

namespace Quanta
{

	struct QConstantBufferVSPerObject
	{
		DirectX::XMFLOAT4X4 world;
		DirectX::XMFLOAT4X4 worldViewProjection;
	};

	struct QConstantBufferPSPerObject
	{
		QMaterial material;
		float hasTexCoords;
		QBytePadding12 p0;
	};

	struct QConstantBufferPSPerFrame
	{
		DirectX::XMFLOAT4X4 shadowMatrix;
		DirectX::XMFLOAT3 eye;
		QBytePadding4 p0;
		QLight lights[QUANTA_MAX_NUM_LIGHTS];
		UINT32 numActiveLights;
		QBytePadding12 p2;
		DirectX::XMFLOAT4 globalAmbient;
	};

	struct QConstantBufferVSPerObjectSprite
	{
		DirectX::XMFLOAT3 offset;
		QBytePadding4 pad0;
		DirectX::XMFLOAT2 scale;
		QBytePadding8 pad1;
	};

	struct QConstantBufferPSPerObjectSprite
	{
		DirectX::XMFLOAT4 colour;
	};

	struct QConstantBufferVSPerObjectDepth
	{
		DirectX::XMFLOAT4X4 worldViewProjection;
	};
}
