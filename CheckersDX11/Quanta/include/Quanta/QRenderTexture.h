#pragma once

#include <d3d11.h>
#include <DirectXMath.h>
#include <wrl/client.h>

#include "Quanta/QuantaDefines.h"
#include "Quanta/QShader.h"

namespace Quanta
{
	QUANTA_ALIGN_16 class QRenderTexture
	{
	public:
		QRenderTexture(ID3D11Device* device, int textureWidth, int textureHeight);

		QUANTA_ALIGN_16_NEW_DELETE

		void SetRenderTarget(ID3D11DeviceContext* deviceContext);

		void ClearRenderTarget(ID3D11DeviceContext* deviceContext, float r, float g, float b, float a) const;

		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> GetShaderResourceView() const;

		Microsoft::WRL::ComPtr<ID3D11Buffer> GetVertexBuffer() const;

		Microsoft::WRL::ComPtr<ID3D11Buffer> GetIndexBuffer() const;

		void Draw(ID3D11DeviceContext* deviceContext,
			QShader& spriteShader,
			ID3D11Buffer* vsConstantBuffer,
			ID3D11Buffer* psConstantBuffer,
			ID3D11SamplerState* samplerState);

	private:
		Microsoft::WRL::ComPtr<ID3D11Texture2D>          m_renderTargetTexture;
		Microsoft::WRL::ComPtr<ID3D11RenderTargetView>   m_renderTargetView;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_shaderResourceView;
		Microsoft::WRL::ComPtr<ID3D11Texture2D>          m_depthStencilBuffer;
		Microsoft::WRL::ComPtr<ID3D11DepthStencilView>   m_depthStencilView;

		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_indexBuffer;

		D3D11_VIEWPORT m_viewport;	
	};
}