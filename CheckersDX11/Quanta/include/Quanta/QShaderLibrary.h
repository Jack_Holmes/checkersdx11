#pragma once

#include <d3d11_1.h>
#include <wrl/client.h>
#include <string>
#include <vector>
#include <memory>

#include "Quanta/QNonCopyable.h"
#include "Quanta/QShader.h"

namespace Quanta
{
	class QShaderLibrary : public QNonCopyable
	{
	public:
		QShaderLibrary();

		QShader* AddShaderFromFiles(
			const std::string& shaderName,
			const std::wstring& vsFile,
			const std::wstring& psFile,
			const std::string& vsProfile,
			const std::string& psProfile,
			const D3D11_INPUT_ELEMENT_DESC* inputElements,
			UINT numElements,
			UINT stride);

		QShader* AddShaderFromCSO(
			const std::string& shaderName,
			const std::wstring& vsFile,
			const std::wstring& psFile,
			const D3D11_INPUT_ELEMENT_DESC* inputElements,
			UINT numElements,
			UINT stride);

		QShader* GetShader(const std::string& shaderName);

		void SetDevice(ID3D11Device* device);

		static HRESULT CompileShaderFromFile(
			LPCWSTR shaderFile,
			LPCSTR entryPoint,
			LPCSTR profile,
			ID3DBlob** blob);

		static HRESULT CompiledShaderToBlob(
			LPCWSTR shaderFile,
			ID3DBlob** blob);

	private:		
		Microsoft::WRL::ComPtr<ID3D11Device> m_device;

		std::vector<std::unique_ptr<QShader>> m_shaders;		
	};
}
