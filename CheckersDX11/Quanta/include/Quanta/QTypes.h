#pragma once

#include <DirectXMath.h>

namespace Quanta
{
	typedef float             QBytePadding4;
	typedef DirectX::XMFLOAT2 QBytePadding8;
	typedef DirectX::XMFLOAT3 QBytePadding12;
}