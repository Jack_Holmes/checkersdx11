#pragma once

#include "Quanta/QTransform.h"
#include <DirectXMath.h>

namespace Quanta
{
	class QCamera
	{
	public:
		explicit QCamera(const DirectX::XMFLOAT3& position, float aspectRatio = 1.333f, float fovY = 45.f, float viewDistance = 100.f);

		QTransform& GetTransform();

		void SetFoVY(float fovY);
		void SetViewDistance(float distance);
		void SetAspectRatio(float aspectRatio);

		const DirectX::XMFLOAT4X4& GetView();
		const DirectX::XMFLOAT4X4& GetProjection();

	private:
		void UpdateView();
		void UpdateProjection();

		DirectX::XMFLOAT4X4 m_viewMatrix;
		DirectX::XMFLOAT4X4 m_projectionMatrix;

		QTransform m_transform;

		float m_fieldOfViewY;
		float m_viewDistance;
		float m_aspectRatio;
		bool  m_viewMatrixDirty;
		bool  m_projectionMatrixDirty;
	};
}