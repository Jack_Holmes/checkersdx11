#pragma once
#include <Windows.h>

namespace Quanta
{
	class QClock
	{
	public:
		QClock();

		float Restart();
		float GetElapsedTime() const;

	private:
		LARGE_INTEGER m_start;
	};
}