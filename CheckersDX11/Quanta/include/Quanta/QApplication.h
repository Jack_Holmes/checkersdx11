#pragma once

#include "Quanta/QuantaDefines.h"

#include <d3d11_1.h>
#include <wrl/client.h>

QUANTA_LINK_LIBRARY(d3d11)
QUANTA_LINK_LIBRARY(dxguid)
QUANTA_LINK_LIBRARY(d3dcompiler)

#include "Quanta/QWindow.h"
#include "Quanta/QRenderer.h"
#include "Quanta/QNonCopyable.h"

namespace Quanta
{
	class QApplication : public QNonCopyable
	{
	public:
		QApplication();
		explicit QApplication(const std::wstring& mediaDirectory);
		virtual ~QApplication();

		static const std::wstring& GetMediaDirectory();

		static const std::wstring& GetWorkingDirectory();

	protected:
		void DeviceLost();

		Microsoft::WRL::ComPtr<ID3D11Device>        m_d3dDevice;
		Microsoft::WRL::ComPtr<ID3D11DeviceContext> m_d3dDeviceContext;

		std::unique_ptr<QWindow> m_window;

		std::unique_ptr<QRenderer> m_renderer;

	private:
		void Initialise(const std::wstring& mediaDirectory);
		void CreateDevice();
		void SetWorkingDirectory();

		static std::wstring s_workingDirectory;
		static std::wstring s_mediaDirectory;

		D3D_FEATURE_LEVEL                            m_d3dFeatureLevel;
		Microsoft::WRL::ComPtr<ID3D11Device1>        m_d3dDevice1;
		Microsoft::WRL::ComPtr<ID3D11DeviceContext1> m_d3dDeviceContext1;
	};
}
